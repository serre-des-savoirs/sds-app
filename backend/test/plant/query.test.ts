import {
  plantResolver,
  plantResolverById,
} from "../../src/plant/api/resolverPlant";

describe("plant query", () => {
  test("test-1 plantResolver return all list of plant", async () => {
    const repositoryPlantMem = {
      getAll: async () => [
        {
          id: "Artichoke-i",
          name: "Artichoke",
          typeVarietal: "typeVarietal",
          typeFamily: "typeFamily",
          wiki: "wiki",
          narrowers: ["narrowers"],
          narrowersId: ["narrowersId"],
          broaders: ["broaders"],
          broadersId: ["broadersId"],
          relatedCrop: "relatedCrop",
          fao: "fao",
          storage: {
            minStorage: "minStorage",
            maxStorage: "maxStorage",
            unitTimeStorage: "unitTimeStorage",
          },
        },
        {
          id: "Tomate-i",
          name: "Tomate",
          typeVarietal: "typeVarietal",
          typeFamily: "typeFamily",
          wiki: "wiki",
          narrowers: ["narrowers"],
          narrowersId: ["narrowersId"],
          broaders: ["broaders"],
          broadersId: ["broadersId"],
          relatedCrop: "relatedCrop",
          fao: "fao",
          storage: {
            minStorage: "minStorage",
            maxStorage: "maxStorage",
            unitTimeStorage: "unitTimeStorage",
          },
        },
      ],
      getPlantById: async (id: string) => undefined,
    };
    const res = await plantResolver(repositoryPlantMem);

    expect(res).toHaveLength(2);
    expect(res[0]).toEqual({
      id: "Artichoke-i",
      name: "Artichoke",
      typeVarietal: "typeVarietal",
      typeFamily: "typeFamily",
      wiki: "wiki",
      narrowers: ["narrowers"],
      narrowersId: ["narrowersId"],
      broaders: ["broaders"],
      broadersId: ["broadersId"],
      relatedCrop: "relatedCrop",
      fao: "fao",
      storage: {
        minStorage: "minStorage",
        maxStorage: "maxStorage",
        unitTimeStorage: "unitTimeStorage",
      },
    });
    expect(res[1]).toEqual({
      id: "Tomate-i",
      name: "Tomate",
      typeVarietal: "typeVarietal",
      typeFamily: "typeFamily",
      wiki: "wiki",
      narrowers: ["narrowers"],
      narrowersId: ["narrowersId"],
      broaders: ["broaders"],
      broadersId: ["broadersId"],
      relatedCrop: "relatedCrop",
      fao: "fao",
      storage: {
        minStorage: "minStorage",
        maxStorage: "maxStorage",
        unitTimeStorage: "unitTimeStorage",
      },
    });
  });

  test("test-2 plantResolver return list of plant by id", async () => {
    const response = [
      {
        id: "Artichoke-i",
        name: "Artichoke",
        typeVarietal: "typeVarietal",
        typeFamily: "typeFamily",
        wiki: "wiki",
        narrowers: ["narrowers"],
        narrowersId: ["narrowersId"],
        broaders: ["broaders"],
        broadersId: ["broadersId"],
        relatedCrop: "relatedCrop",
        fao: "fao",
        storage: {
          minStorage: "minStorage",
          maxStorage: "maxStorage",
          unitTimeStorage: "unitTimeStorage",
        },
      },
    ];

    const repositoryPlantMem = {
      getAll: async () => [],
      getPlantById: async (id: string) =>
        response.filter((res) => res.id === id)[0],
    };

    const res = await plantResolverById(repositoryPlantMem, "Artichoke-i");

    expect(res).toEqual({
      id: "Artichoke-i",
      name: "Artichoke",
      typeVarietal: "typeVarietal",
      typeFamily: "typeFamily",
      wiki: "wiki",
      narrowers: ["narrowers"],
      narrowersId: ["narrowersId"],
      broaders: ["broaders"],
      broadersId: ["broadersId"],
      relatedCrop: "relatedCrop",
      fao: "fao",
      storage: {
        minStorage: "minStorage",
        maxStorage: "maxStorage",
        unitTimeStorage: "unitTimeStorage",
      },
    });
  });

  test("test-3 plantResolver return list of plant by id invalid", async () => {
    const repositoryPlantMem = {
      getAll: async () => [],
      getPlantById: async (id: string) => undefined,
    };

    const res = await plantResolverById(repositoryPlantMem, "foo");

    expect(res).toEqual({
      id: "",
      name: "",
      description: "",
    });
  });

  test("test-4 plantResolver return list of plant by id undefined", async () => {
    const repositoryPlantMem = {
      getAll: async () => [],
      getPlantById: async (id: string) => undefined,
    };

    const res = await plantResolverById(repositoryPlantMem, undefined);

    expect(res).toEqual({
      id: "",
      name: "",
      description: "",
    });
  });
});
