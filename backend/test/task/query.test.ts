import { taskResolver } from "../../src/task/api/resolverTask";

describe("Task query", () => {
  test("test-1 taskResolver return all list of task", async () => {
    const repositoryTaskMem = {
      getAll: async () => [
        {
          id: "taskIdArtichoke-i",
          type: "Artichoke",
          bedValue: "bedValue",
          bedUnit: "bedUnit",
          footValue: "footValue",
          footUnit: "footUnit",
          plantingDensityValue: "plantingDensityValue",
          plantingDensityUnit: "plantingDensityUnit",
          spacingByLineValue: "spacingByLineValue",
          spacingByLineUnit: "spacingByLineUnit",
          walkingSpaceValue: "walkingSpaceValue",
          walkingSpaceUnit: "walkingSpaceUnit",
          spacingInsideLineValue: "spacingInsideLineValue",
          spacingInsideLineUnit: "spacingInsideLineUnit",
          workLoadValue: "workLoadValue",
          workLoadUnit: "workLoadUnit",
        },
        {
          id: "taskIdTomate-i",
          type: "Tomate",
          bedValue: "bedValue",
          bedUnit: "bedUnit",
          footValue: "footValue",
          footUnit: "footUnit",
          plantingDensityValue: "plantingDensityValue",
          plantingDensityUnit: "plantingDensityUnit",
          spacingByLineValue: "spacingByLineValue",
          spacingByLineUnit: "spacingByLineUnit",
          walkingSpaceValue: "walkingSpaceValue",
          walkingSpaceUnit: "walkingSpaceUnit",
          spacingInsideLineValue: "spacingInsideLineValue",
          spacingInsideLineUnit: "spacingInsideLineUnit",
          workLoadValue: "workLoadValue",
          workLoadUnit: "workLoadUnit",
        },
      ],
      getTaskById: async (id: string) => undefined,
    };
    const res = await taskResolver(repositoryTaskMem);

    expect(res).toHaveLength(2);
    expect(res[0]).toEqual({
      id: "taskIdArtichoke-i",
      type: "Artichoke",

      bedValue: "bedValue",
      bedUnit: "bedUnit",
      footValue: "footValue",
      footUnit: "footUnit",
      plantingDensityValue: "plantingDensityValue",
      plantingDensityUnit: "plantingDensityUnit",
      spacingByLineValue: "spacingByLineValue",
      spacingByLineUnit: "spacingByLineUnit",
      walkingSpaceValue: "walkingSpaceValue",
      walkingSpaceUnit: "walkingSpaceUnit",
      spacingInsideLineValue: "spacingInsideLineValue",
      spacingInsideLineUnit: "spacingInsideLineUnit",
      workLoadValue: "workLoadValue",
      workLoadUnit: "workLoadUnit",
    });
    expect(res[1]).toEqual({
      id: "taskIdTomate-i",
      type: "Tomate",

      bedValue: "bedValue",
      bedUnit: "bedUnit",
      footValue: "footValue",
      footUnit: "footUnit",
      plantingDensityValue: "plantingDensityValue",
      plantingDensityUnit: "plantingDensityUnit",
      spacingByLineValue: "spacingByLineValue",
      spacingByLineUnit: "spacingByLineUnit",
      walkingSpaceValue: "walkingSpaceValue",
      walkingSpaceUnit: "walkingSpaceUnit",
      spacingInsideLineValue: "spacingInsideLineValue",
      spacingInsideLineUnit: "spacingInsideLineUnit",
      workLoadValue: "workLoadValue",
      workLoadUnit: "workLoadUnit",
    });
  });
});
