import {
  itkMutation,
  itkMutationClimate,
  itkMutationCultureMode,
  itkMutationFarmEquipment,
  itkMutationImplantationMode,
  itkMutationIrrigationMode,
  itkMutationPlant,
  itkMutationRegion,
  itkMutationSeason,
  itkMutationTask,
  itkMutationTypeSoil,
} from "../../src/itk/api/resolverItk";
import { isPunctuatorTokenKind } from "graphql/language/lexer";

const templateRepositoryItk = {
  getAll: async () => [],
  getItkById: async (id: string) => undefined,
  getAllPlant: async () => [],
  getAllSeason: async () => [],
  getAllClimate: async () => [],
  updateTitle: async (id: string, newTitle: string) => "",
  updateRegion: (id: string, newRegion: string[]) => "",
  updatePlant: async (id: string, newPlant: string) => "",
  updateSeason: async (id: string, newSeason: string[]) => "",
  updateClimate: async (id: string, newClimate: string) => "",
  updateCultureMode: async (id: string, newCultureMode: string) => "",
  updateImplantationMode: async (id: string, newImplantationMode: string) => "",
  updateTypeSoil: async (
    id: string,
    newTypeSoil: string
  ): Promise<"success" | "error"> => "success",
  updateFarmEquipment: async (
    id: string,
    newFarmEquipment: string
  ): Promise<"success" | "error"> => "success",
  updateIrrigationMode: async (
    id: string,
    newIrrigationMode: string
  ): Promise<"success" | "error"> => "success",
  updateTask: async (
    id: string,
    title: string,
    duration: number,
    workloadValue: number,
    workloadUnit: string
  ): Promise<"success" | "error"> => "success",
};

const templateRepositoryConnection = {
  signIn: async (args: any) => "ImAToKeN",
  signOut: async ({ token: string }): Promise<"success" | "error"> => "success",
  getUser: async ({ token: string }): Promise<"success" | "error"> => "success",
};

describe("itk update", () => {
  test("test-1 itkMutation update itk with just a title", async () => {
    const repositoryItkMem = {
      ...templateRepositoryItk,
      updateTitle: async (id: string, newTitle: string, lang: string) =>
        "Artichoke",
    };

    const res = await itkMutation(
      repositoryItkMem,
      "Artichoke-i",
      "Artichoke",
      "en",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("Artichoke");
  });

  test("test-2 itkMutation update itk an id empty", async () => {
    const res = await itkMutation(
      templateRepositoryItk,
      "",
      "Artichoke",
      "en",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-3 itkMutation update itk with an new title empty", async () => {
    const res = await itkMutation(
      templateRepositoryItk,
      undefined,
      "Artichoke",
      "en",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-4 itkMutation update itk with an new title empty", async () => {
    const res = await itkMutation(
      templateRepositoryItk,
      undefined,
      "Artichoke",
      "en",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-5 itkMutation update itk with an language", async () => {
    const repositoryItkMem = {
      ...templateRepositoryItk,
      updateTitle: async (id: string, newTitle: string, lang: string) =>
        "Artichoke",
    };

    const res = await itkMutation(
      repositoryItkMem,
      "Artichoke-i",
      "Artichoke",
      "en",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("Artichoke");
  });

  test("test-6 itkMutation update itk with an empty lang", async () => {
    const repositoryItkMem = {
      ...templateRepositoryItk,
      updateTitle: async (id: string, newTitle: string, lang: string) =>
        "Artichoke",
    };

    const res = await itkMutation(
      repositoryItkMem,
      "Artichoke-i",
      "Artichoke",
      "",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("Artichoke");
  });

  test("test-7 itkMutationRegion update region", async () => {
    const res = await itkMutationRegion(
      templateRepositoryItk,
      "Artichoke-i",
      ["CentreValDeLoire"],
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-8 itkMutationRegion update region with an empty region", async () => {
    const res = await itkMutationRegion(
      templateRepositoryItk,
      "Artichoke-i",
      [],
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-9 itkMutationRegion update region with an empty id", async () => {
    const res = await itkMutationRegion(
      templateRepositoryItk,
      "",
      ["CentreValDeLoire"],
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual([""]);
  });

  test("test-10 itkMutationPlant update plant", async () => {
    const res = await itkMutationPlant(
      templateRepositoryItk,
      "Artichoke-i",
      "Artichoke",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-11 itkMutationPlant update plant with an empty plant", async () => {
    const res = await itkMutationPlant(
      templateRepositoryItk,
      "Artichoke-i",
      "",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-12 itkMutationPlant update plant with an empty id", async () => {
    const res = await itkMutationPlant(
      templateRepositoryItk,
      "",
      "Artichoke",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("error: id is undefined");
  });

  test("test-13 itkMutationSeason update season", async () => {
    const res = await itkMutationSeason(
      templateRepositoryItk,
      "Artichoke-i",
      ["Spring"],
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-14 itkMutationSeason update season with an empty season", async () => {
    const res = await itkMutationSeason(
      templateRepositoryItk,
      "Artichoke-i",
      [],
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-15 itkMutationSeason update season with an empty id", async () => {
    const res = await itkMutationSeason(
      templateRepositoryItk,
      "",
      ["Spring"],
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual(["error: id is undefined"]);
  });

  test("test-16 itkMutationClimate update climate", async () => {
    const res = await itkMutationClimate(
      templateRepositoryItk,
      "Artichoke-i",
      "Spring",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-17 itkMutationClimate update climate with an empty climate", async () => {
    const res = await itkMutationClimate(
      templateRepositoryItk,
      "Artichoke-i",
      "",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-18 itkMutationClimate update climate with an empty id", async () => {
    const res = await itkMutationClimate(
      templateRepositoryItk,
      "",
      "Spring",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("error: id is undefined");
  });

  test("test-19 itkMutationCultureMode update cultureMode", async () => {
    const res = await itkMutationCultureMode(
      templateRepositoryItk,
      "Artichoke-i",
      "Spring",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-20 itkMutationCultureMode update cultureMode with an empty cultureMode", async () => {
    const res = await itkMutationCultureMode(
      templateRepositoryItk,
      "Artichoke-i",
      "",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-21 itkMutationCultureMode update cultureMode with an empty id", async () => {
    const res = await itkMutationCultureMode(
      templateRepositoryItk,
      "",
      "Spring",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("error: id is undefined");
  });

  test("test-22 itkMutationImplantationMode update implantationMode", async () => {
    const res = await itkMutationImplantationMode(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-23 itkMutationImplantationMode update implantationMode with an empty implantationMode", async () => {
    const res = await itkMutationImplantationMode(
      templateRepositoryItk,
      "Artichoke-i",
      "",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("");
  });

  test("test-24 itkMutationImplantationMode update implantationMode with an empty id", async () => {
    const res = await itkMutationImplantationMode(
      templateRepositoryItk,
      "",
      "FreeArea",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("error: id is undefined");
  });

  test("test-25 itkMutationTypeSoil update typeSoil", async () => {
    const res = await itkMutationTypeSoil(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("success");
  });

  test("test-26 itkMutationTypeSoil update typeSoil with an empty typeSoil", async () => {
    const res = await itkMutationTypeSoil(
      templateRepositoryItk,
      "Artichoke-i",
      "",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("success");
  });

  test("test-27 itkMutationTypeSoil update typeSoil with an empty id", async () => {
    const res = await itkMutationTypeSoil(
      templateRepositoryItk,
      "",
      "FreeArea",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("error: id is undefined");
  });

  test("test-28 itkMutationTypeSoil update typeSoil without node typesoil context", async () => {
    const repositoryItkMem = {
      ...templateRepositoryItk,
      updateTypeSoil: (
        id: string,
        newTypeSoil: string
      ): Promise<"success" | "error"> => {
        return Promise.resolve("error");
      },
    };

    const res = await itkMutationTypeSoil(
      repositoryItkMem,
      "Artichoke-i",
      "FreeArea",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("error");
  });

  test("test-29 itkMutationIrrigationMode update irrigationMode", async () => {
    const res = await itkMutationIrrigationMode(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("success");
  });

  test("test-30 itkMutationIrrigationMode update irrigationMode with an empty irrigationMode", async () => {
    const res = await itkMutationIrrigationMode(
      templateRepositoryItk,
      "Artichoke-i",
      "",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("success");
  });

  test("test-31 itkMutationIrrigationMode update irrigationMode with an empty id", async () => {
    const res = await itkMutationIrrigationMode(
      templateRepositoryItk,
      "",
      "FreeArea",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("error: id is undefined");
  });

  test("test-32 itkMutationIrrigationMode update irrigationMode without node irrigationMode context", async () => {
    const repositoryItkMem = {
      ...templateRepositoryItk,
      updateIrrigationMode: (
        id: string,
        newIrrigationMode: string
      ): Promise<"success" | "error"> => {
        return Promise.resolve("error");
      },
    };

    const res = await itkMutationIrrigationMode(
      repositoryItkMem,
      "Artichoke-i",
      "FreeArea",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("error");
  });

  test("test-33 itkMutationFarmEquipment update farmEquipment", async () => {
    const res = await itkMutationFarmEquipment(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("success");
  });

  test("test-34 itkMutationFarmEquipment update farmEquipment with an empty farmEquipment", async () => {
    const res = await itkMutationFarmEquipment(
      templateRepositoryItk,
      "Artichoke-i",
      "",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("success");
  });

  test("test-35 itkMutationFarmEquipment update farmEquipment with an empty id", async () => {
    const res = await itkMutationFarmEquipment(
      templateRepositoryItk,
      "",
      "FreeArea",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("error: id is undefined");
  });

  test("test-36 itkMutationFarmEquipment update farmEquipment without node farmEquipment context", async () => {
    const repositoryItkMem = {
      ...templateRepositoryItk,
      updateFarmEquipment: (
        id: string,
        newFarmEquipment: string
      ): Promise<"success" | "error"> => {
        return Promise.resolve("error");
      },
    };

    const res = await itkMutationFarmEquipment(
      repositoryItkMem,
      "Artichoke-i",
      "FreeArea",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("error");
  });

  test("test-37 itkMutationTask update task", async () => {
    const res = await itkMutationTask(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      0,
      0,
      "Centimeter",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("success");
  });

  test("test-38 itkMutationTask update task with an empty id", async () => {
    const res = await itkMutationTask(
      templateRepositoryItk,
      "",
      "FreeArea",
      0,
      0,
      "Centimeter",
      "ImAToKeN",
      templateRepositoryConnection
    );

    expect(res).toEqual("error: id is undefined");
  });

  // remade tests on all old tests who check if token exist or not
  test("test-39 itkMutationTask update task with an wrong token", async () => {
    const repositoryAuthMem = {
      ...templateRepositoryConnection,
      getUser: async ({ token }: { token: string }) => {
        return "error";
      },
    };

    const res = await itkMutationTask(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      0,
      0,
      "Centimeter",
      "ImAToKeN",
      repositoryAuthMem
    );

    expect(res).toEqual("error: token is undefined");
  });

  test("test-40 itkMutationFarmEquipment update farmEquipment with an wrong token", async () => {
    const repositoryAuthMem = {
      ...templateRepositoryConnection,
      getUser: async ({ token }: { token: string }) => {
        return "error";
      },
    };

    const res = await itkMutationFarmEquipment(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      "ImAToKeN",
      repositoryAuthMem
    );

    expect(res).toEqual("error: token is undefined");
  });

  test("test-41 itkMutationIrrigationMode update irrigationMode with an wrong token", async () => {
    const repositoryAuthMem = {
      ...templateRepositoryConnection,
      getUser: async ({ token }: { token: string }) => {
        return "error";
      },
    };

    const res = await itkMutationIrrigationMode(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      null,
      repositoryAuthMem
    );

    expect(res).toEqual("error: token is undefined");
  });

  test("test-42 itkMutationTypeSoil update typeSoil with an wrong token", async () => {
    const repositoryAuthMem = {
      ...templateRepositoryConnection,
      getUser: async ({ token }: { token: string }) => {
        return "error";
      },
    };

    const res = await itkMutationTypeSoil(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      null,
      repositoryAuthMem
    );

    expect(res).toEqual("error: token is undefined");
  });

  test("test-43 itkMutationImplantationMode update implantationMode with an wrong token", async () => {
    const repositoryAuthMem = {
      ...templateRepositoryConnection,
      getUser: async ({ token }: { token: string }) => {
        return "error";
      },
    };

    const res = await itkMutationImplantationMode(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      null,
      repositoryAuthMem
    );

    expect(res).toEqual("error: token is undefined");
  });

  test("test-44 itkMutationCultureMode update cultureMode with an wrong token", async () => {
    const repositoryAuthMem = {
      ...templateRepositoryConnection,
      getUser: async ({ token }: { token: string }) => {
        return "error";
      },
    };

    const res = await itkMutationCultureMode(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      null,
      repositoryAuthMem
    );

    expect(res).toEqual("error: token is undefined");
  });

  test("test-45 itkMutationClimate update climate with an wrong token", async () => {
    const repositoryAuthMem = {
      ...templateRepositoryConnection,
      getUser: async ({ token }: { token: string }) => {
        return "error";
      },
    };

    const res = await itkMutationClimate(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      "ImAToKeN",
      repositoryAuthMem
    );

    expect(res).toEqual("error: token is undefined");
  });

  test("test-46 itkMutationSeason update season with an wrong token", async () => {
    const repositoryAuthMem = {
      ...templateRepositoryConnection,
      getUser: async ({ token }: { token: string }) => {
        return "error";
      },
    };

    const res = await itkMutationSeason(
      templateRepositoryItk,
      "Artichoke-i",
      ["Summer", "Winter"],
      "ImAToKeN",
      repositoryAuthMem
    );

    expect(res).toEqual("error: token is undefined");
  });

  test("test-47 itkMutationPlant update plant with an wrong token", async () => {
    const repositoryAuthMem = {
      ...templateRepositoryConnection,
      getUser: async ({ token }: { token: string }) => {
        return "error";
      },
    };

    const res = await itkMutationPlant(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      "ImAToKeN",
      repositoryAuthMem
    );

    expect(res).toEqual("error: token is undefined");
  });

  test("test-48 itkMutationRegion update region with an wrong token", async () => {
    const repositoryAuthMem = {
      ...templateRepositoryConnection,
      getUser: async ({ token }: { token: string }) => {
        return "error";
      },
    };

    const res = await itkMutationRegion(
      templateRepositoryItk,
      "Artichoke-i",
      ["NouvelleAquitaine"],
      "ImAToKeN",
      repositoryAuthMem
    );

    expect(res).toEqual("error: token is undefined");
  });

  test("test-49 itkMutationTitle update title with an wrong token", async () => {
    const repositoryAuthMem = {
      ...templateRepositoryConnection,
      getUser: async ({ token }: { token: string }) => {
        return "error";
      },
    };

    const res = await itkMutation(
      templateRepositoryItk,
      "Artichoke-i",
      "FreeArea",
      "fr",
      "ImAToKeN",
      repositoryAuthMem
    );

    expect(res).toEqual("error: token is undefined");
  });
});
