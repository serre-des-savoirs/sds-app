import {
  itkResolver,
  itkResolverById,
  itkResolverClimate,
  itkResolverPlant,
  itkResolverSeason,
} from "../../src/itk/api/resolverItk";

const templateRepositoryItk = {
  getAll: async () => [],
  getItkById: async (id: string) => undefined,
  getAllPlant: async () => [],
  getAllSeason: async () => [],
  getAllClimate: async () => [],
  updateTitle: async (id: string, newTitle: string) => "",
  updateRegion: (id: string, newRegion: string[]) => "",
  updatePlant: async (id: string, newPlant: string) => "",
  updateSeason: async (id: string, newSeason: string[]) => "",
  updateClimate: async (id: string, newClimate: string) => "",
  updateCultureMode: async (id: string, newCultureMode: string) => "",
  updateImplantationMode: async (id: string, newImplantationMode: string) => "",
  updateTypeSoil: async (
    id: string,
    newTypeSoil: string
  ): Promise<"success" | "error"> => "success",
  updateFarmEquipment: async (
    id: string,
    newFarmEquipment: string
  ): Promise<"success" | "error"> => "success",
  updateIrrigationMode: async (
    id: string,
    newIrrigationMode: string
  ): Promise<"success" | "error"> => "success",
  updateTask: async (
    id: string,
    title: string,
    duration: number,
    workloadValue: number,
    workloadUnit: string
  ): Promise<"success" | "error"> => "success",
};

describe("itk query", () => {
  test("test-1 itkResolver return all list of itk", async () => {
    const repositoryItkMem = {
      ...templateRepositoryItk,
      getAll: async () => [
        {
          id: "Artichoke-i",
          name: "Artichoke",
          cultureMode: "cultureMode",
          plantId: "plant",
          task: [
            {
              taskId: "taskId",
              taskName: "taskName",
              typeTask: "typeTask",
              season: "season",
              typeCultivation: "typeCultivation",
              timeBegin: "timeBegin",
              timeEnd: "timeEnd",
            },
          ],
        },
        {
          id: "Tomate-i",
          name: "Tomate",
          cultureMode: "cultureMode1",
          plantId: "plant1",
          task: [],
        },
      ],
    };
    const res = await itkResolver(repositoryItkMem);

    expect(res).toHaveLength(2);
    expect(res[0]).toEqual({
      id: "Artichoke-i",
      name: "Artichoke",
      cultureMode: "cultureMode",
      plantId: "plant",
      task: [
        {
          taskId: "taskId",
          taskName: "taskName",
          typeTask: "typeTask",
          season: "season",
          typeCultivation: "typeCultivation",
          timeBegin: "timeBegin",
          timeEnd: "timeEnd",
        },
      ],
    });
    expect(res[1]).toEqual({
      id: "Tomate-i",
      name: "Tomate",
      cultureMode: "cultureMode1",
      plantId: "plant1",
      task: [],
    });
  });

  test("test-2 itkResolver return list of itk by id", async () => {
    const response = [
      {
        id: "Artichoke-i",
        name: "Artichoke",
        cultureMode: "cultureMode",
        plantId: "plant",
        season: ["season"],
        hasRelativePeriod: {
          hasBeginning: "hasBeginning",
          hasEnding: "hasEnding",
        },
        typeSoil: "typeSoil",
        hasFarmEquipment: "farmEquipment",
        hasIrrigationMode: "irrigationMode",
        task: [
          {
            hasRelativePeriod: {
              hasBeginning: "hasBeginning",
              hasEnding: "hasEnding",
            },
            taskId: "taskId",
            taskName: "taskName",
            taskType: "typeTask",
            implements: {
              hasBedWidth: {
                parameterValue: 0,
                hasParameterUnit: "hasParameterUnit",
              },
              hasFootPassWidth: {
                parameterValue: 0,
                hasParameterUnit: "hasParameterUnit",
              },
              hasPlantingDensity: {
                parameterValue: 0,
                hasParameterUnit: "hasParameterUnit",
              },
              hasSpacingByLine: {
                parameterValue: 0,
                hasParameterUnit: "hasParameterUnit",
              },
              hasWalkingSpaceWidth: {
                parameterValue: 0,
                hasParameterUnit: "hasParameterUnit",
              },
              hasSpacingInsideLine: {
                parameterValue: 0,
                hasParameterUnit: "hasParameterUnit",
              },
              hasWorkload: {
                parameterValue: 0,
                hasWorkloadUnit: "hasWorkloadUnit",
              },
            },
            hasOperationDistance: {
              distanceDuration: {
                numericDuration: 0,
                unitType: "unitType",
              },
              refersToOperation: "refersToOperation",
            },
          },
        ],
        hasYield: {
          parameterValue: "parameterValue",
          hasParameterUnit: "hasParameterUnit",
          maxYield: "maxYield",
          minYield: "minYield",
          averageYield: "averageYield",
        },
        dependOnSoilClimateContext: {
          belongsToRegion: "belongsToRegion",
          hasClimateContext: "hasClimateContext",
        },
        hasGlobalWorload: {
          parameterValue: 0,
          hasParameterUnit: "hasParameterUnit",
        },
        hasImplementationMode: "hasImplementationMode",
        lossMargin: "lossMargin",
      },
    ];

    const repositoryItkMem = {
      ...templateRepositoryItk,
      getAll: async () => response,
      getItkById: async (id: string) =>
        response.filter((itk) => itk.id === id)[0],
    };

    const res = await itkResolverById(repositoryItkMem, "Artichoke-i");

    expect(res).toEqual({
      id: "Artichoke-i",
      name: "Artichoke",
      cultureMode: "cultureMode",
      plantId: "plant",
      season: ["season"],
      hasRelativePeriod: {
        hasBeginning: "hasBeginning",
        hasEnding: "hasEnding",
      },
      typeSoil: "typeSoil",
      hasFarmEquipment: "farmEquipment",
      hasIrrigationMode: "irrigationMode",
      task: [
        {
          taskId: "taskId",
          taskName: "taskName",
          taskType: "typeTask",
          hasRelativePeriod: {
            hasBeginning: "hasBeginning",
            hasEnding: "hasEnding",
          },
          implements: {
            hasBedWidth: {
              parameterValue: 0,
              hasParameterUnit: "hasParameterUnit",
            },
            hasFootPassWidth: {
              parameterValue: 0,
              hasParameterUnit: "hasParameterUnit",
            },
            hasPlantingDensity: {
              parameterValue: 0,
              hasParameterUnit: "hasParameterUnit",
            },
            hasSpacingByLine: {
              parameterValue: 0,
              hasParameterUnit: "hasParameterUnit",
            },
            hasWalkingSpaceWidth: {
              parameterValue: 0,
              hasParameterUnit: "hasParameterUnit",
            },
            hasSpacingInsideLine: {
              parameterValue: 0,
              hasParameterUnit: "hasParameterUnit",
            },
            hasWorkload: {
              parameterValue: 0,
              hasWorkloadUnit: "hasWorkloadUnit",
            },
          },
          hasOperationDistance: {
            distanceDuration: {
              numericDuration: 0,
              unitType: "unitType",
            },
            refersToOperation: "refersToOperation",
          },
        },
      ],
      hasYield: {
        parameterValue: "parameterValue",
        hasParameterUnit: "hasParameterUnit",
        maxYield: "maxYield",
        minYield: "minYield",
        averageYield: "averageYield",
      },
      dependOnSoilClimateContext: {
        belongsToRegion: "belongsToRegion",
        hasClimateContext: "hasClimateContext",
      },
      hasGlobalWorload: {
        parameterValue: 0,
        hasParameterUnit: "hasParameterUnit",
      },
      hasImplementationMode: "hasImplementationMode",
      lossMargin: "lossMargin",
    });
  });

  test("test-3 itkResolver return list of itk by id invalid", async () => {
    const repositoryItkMem = templateRepositoryItk;
    const res = await itkResolverById(repositoryItkMem, "foo");

    expect(res).toEqual({
      id: "",
      name: "",
      description: "",
      cultureMode: "",
      plant: "",
      task: [],
    });
  });

  test("test-4 itkResolver return list of itk by id undifined", async () => {
    const repositoryItkMem = templateRepositoryItk;
    const res = await itkResolverById(repositoryItkMem, undefined);

    expect(res).toEqual({
      id: "",
      name: "",
      description: "",
      cultureMode: "",
      plant: "",
      task: [],
    });
  });

  test("test-5 itkResolverPlant get all plant", async () => {
    const repositoryItkMem = {
      ...templateRepositoryItk,
      getAllPlant: async () => [
        {
          id: "tomato-i",
          plantName: "tomato",
        },
        {
          id: "bean-i",
          plantName: "bean",
        },
      ],
    };
    const res = await itkResolverPlant(repositoryItkMem);
    expect(res).toEqual([
      {
        id: "tomato-i",
        plantName: "tomato",
      },
      {
        id: "bean-i",
        plantName: "bean",
      },
    ]);
  });

  test("test-6 itkResolverPlant get all plant with response empty", async () => {
    const repositoryItkMem = templateRepositoryItk;

    const res = await itkResolverPlant(repositoryItkMem);

    expect(res).toEqual([]);
  });

  test("test-7 itkResolverSeason get all Season", async () => {
    const repositoryItkMem = {
      ...templateRepositoryItk,
      getAllSeason: async () => [
        {
          id: "spring-i",
          seasonName: "spring",
        },
        {
          id: "summer-i",
          seasonName: "summer",
        },
      ],
    };

    const res = await itkResolverSeason(repositoryItkMem);

    expect(res).toEqual([
      {
        id: "spring-i",
        seasonName: "spring",
      },
      {
        id: "summer-i",
        seasonName: "summer",
      },
    ]);
  });

  test("test-8 itkResolverSeason get all Season with response empty", async () => {
    const repositoryItkMem = templateRepositoryItk;

    const res = await itkResolverSeason(repositoryItkMem);

    expect(res).toEqual([]);
  });

  test("test-9 itkResolverClimate get all Climate", async () => {
    const repositoryItkMem = {
      ...templateRepositoryItk,
      getAllClimate: async () => [
        {
          id: "Summer",
          climateName: "Summer",
        },
        {
          id: "Autumn",
          climateName: "Autumn",
        },
      ],
    };

    const res = await itkResolverClimate(repositoryItkMem);

    expect(res).toEqual([
      {
        id: "Summer",
        climateName: "Summer",
      },
      {
        id: "Autumn",
        climateName: "Autumn",
      },
    ]);
  });
});
