export const typeDefinitonsTask = /* GraphQL */ `
  type Query {
    taskTypes: [Task!]!
    getTask(id: ID!): Task!
  }

  type Task {
    id: ID!
    type: String!
    bedValue: String!
    bedUnit: String!
    footValue: String!
    footUnit: String!
    plantingDensityValue: String!
    plantingDensityUnit: String!
    spacingByLineValue: String!
    spacingByLineUnit: String!
    walkingSpaceValue: String!
    walkingSpaceUnit: String!
    spacingInsideLineValue: String!
    spacingInsideLineUnit: String!
    workLoadValue: String!
    workLoadUnit: String!
  }
`;
