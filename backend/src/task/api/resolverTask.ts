import { RepositoryTask } from "./domain";
import { RepositoryTaskGraphDB } from "../repository/repositoryTask";

export async function taskResolver(repositoryTask: RepositoryTask) {
  return await repositoryTask.getAll();
}

const emptyTask = {
  id: "",
  type: "",
};

async function taskTypeResolverById(
  repositoryTask: RepositoryTask,
  id: string | undefined
) {
  if (id === undefined) return emptyTask;
  const res = await repositoryTask.getTaskById(id);
  return res || emptyTask;
}

export const resolverTask = {
  Query: {
    taskTypes: async () => taskResolver(new RepositoryTaskGraphDB()),
    getTask: async (parent: unknown, args: { id: string }) =>
      taskTypeResolverById(new RepositoryTaskGraphDB(), args.id),
  },
};
