export interface task {
  id: string;
  type: string;
  bedValue: string;
  bedUnit: string;
  footValue: string;
  footUnit: string;
  plantingDensityValue: string;
  plantingDensityUnit: string;
  spacingByLineValue: string;
  spacingByLineUnit: string;
  walkingSpaceValue: string;
  walkingSpaceUnit: string;
  spacingInsideLineValue: string;
  spacingInsideLineUnit: string;
  workLoadValue: string;
  workLoadUnit: string;
}

export interface RepositoryTask {
  getAll: () => Promise<task[]>;
  getTaskById: (id: string) => Promise<task | undefined>;
}
