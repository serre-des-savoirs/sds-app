import {
  GraphDBClient,
  GraphLabelProperty,
  GraphProperty,
} from "../../graphDBClient";
import { task } from "../api/domain";

interface TaskGraph {
  taskId: GraphProperty;
  taskType: GraphProperty;
  bedValue?: GraphProperty;
  bedUnit?: GraphProperty;
  footValue?: GraphProperty;
  footUnit?: GraphLabelProperty;
  plantingDensityValue?: GraphProperty;
  plantingDensityUnit?: GraphLabelProperty;
  spacingByLineValue?: GraphProperty;
  spacingByLineUnit?: GraphLabelProperty;
  walkingSpaceValue?: GraphProperty;
  walkingSpaceUnit?: GraphLabelProperty;
  spacingInsideLineValue?: GraphProperty;
  spacingInsideLineUnit?: GraphLabelProperty;
  workloadValue?: GraphProperty;
  workloadUnit?: GraphProperty;
}

export class RepositoryTaskGraphDB {
  private dbClient: GraphDBClient;

  constructor() {
    this.dbClient = new GraphDBClient({
      config: {
        endpoint: process.env.GRAPHDB_ENDPOINT as string,
        username: process.env.GRAPHDB_USER as string,
        password: process.env.GRAPHDB_PASS as string,
        repository: process.env.GRAPHDB_REPOSITORY as string,
      },
    });
  }

  async getAll(): Promise<task[]> {
    const res = await this.dbClient.query<TaskGraph>({
      query: `
            PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
            PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            select ?taskId ?taskType where { 
                ?taskId a c3pocm:TechnicalOperation .
                ?taskId c3pocm:implements /rdf:type ?taskType
                FILTER NOT EXISTS{?childType rdfs:subClassOf ?taskType}
            }
        `,
    });

    return res.results.bindings.map((task) => {
      return {
        id: task.taskId.value,
        type: task.taskType.value,
        bedValue: "",
        bedUnit: "",
        footValue: "",
        footUnit: "",
        plantingDensityValue: "",
        plantingDensityUnit: "",
        spacingByLineValue: "",
        spacingByLineUnit: "",
        walkingSpaceValue: "",
        walkingSpaceUnit: "",
        spacingInsideLineValue: "",
        spacingInsideLineUnit: "",
        workLoadValue: "",
        workLoadUnit: "",
      };
    });
  }

  async getTaskById(id: string): Promise<task | undefined> {
    const res = await this.dbClient.query<TaskGraph>({
      query: `
            PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
            PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX c3poparam: <http://www.elzeard.co/ontologies/c3po/parameter#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            select * where {
                VALUES (?taskId) {
                	(c3pokb:${id})
            	}
                ?taskId a c3pocm:TechnicalOperation .
                ?taskId c3pocm:implements /rdf:type ?taskType
                OPTIONAL {
                    ?taskId c3pocm:implements/c3pocm:hasBedWidth/c3poparam:parameterValue ?bedValue .
                    ?taskId c3pocm:implements/c3pocm:hasBedWidth/c3poparam:hasParameterUnit/skos:prefLabel ?bedUnit .
                    FILTER(LANGMATCHES(LANG(?bedUnit), 'fr'))
                }
                OPTIONAL {
                    ?taskId c3pocm:implements/c3pocm:hasFootPassWidth/c3poparam:parameterValue ?footValue .
                    ?taskId c3pocm:implements/c3pocm:hasFootPassWidth/c3poparam:hasParameterUnit/skos:prefLabel ?footUnit .
                    FILTER(LANGMATCHES(LANG(?footUnit), 'fr'))
                }
                OPTIONAL {
                    ?taskId c3pocm:implements/c3pocm:hasPlantingDensity/c3poparam:parameterValue ?plantingDensityValue .
                    ?taskId c3pocm:implements/c3pocm:hasPlantingDensity/c3poparam:hasParameterUnit/skos:prefLabel ?plantingDensityUnit .
                    FILTER(LANGMATCHES(LANG(?plantingDensityUnit), 'fr'))
                }
                OPTIONAL {
                    ?taskId c3pocm:implements/c3pocm:hasSpacingByLine/c3poparam:parameterValue ?spacingByLineValue .
                    ?taskId c3pocm:implements/c3pocm:hasSpacingByLine/c3poparam:hasParameterUnit/skos:prefLabel ?spacingByLineUnit .
                    FILTER(LANGMATCHES(LANG(?spacingByLineUnit), 'fr'))
                }
                OPTIONAL {
                    ?taskId c3pocm:implements/c3pocm:hasSpacingInsideLine/c3poparam:parameterValue ?spacingInsideLineValue .
                    ?taskId c3pocm:implements/c3pocm:hasSpacingInsideLine/c3poparam:hasParameterUnit/skos:prefLabel ?spacingInsideLineUnit .
                    FILTER(LANGMATCHES(LANG(?spacingInsideLineUnit), 'fr'))
                }
                OPTIONAL {
                    ?taskId c3pocm:implements/c3pocm:hasWalkingSpaceWidth/c3poparam:parameterValue ?walkingSpaceValue .
                    ?taskId c3pocm:implements/c3pocm:hasWalkingSpaceWidth/c3poparam:hasParameterUnit/skos:prefLabel ?walkingSpaceUnit .
                    FILTER(LANGMATCHES(LANG(?walkingSpaceUnit), 'fr'))
                }
                OPTIONAL {
                    ?taskId c3pocm:implements/c3pocm:hasWorkload/c3poparam:parameterValue ?workloadValue .
                    ?taskId c3pocm:implements/c3pocm:hasWorkload/c3poparam:hasWorkloadUnit ?workloadUnit .
                }
                FILTER NOT EXISTS{?childType rdfs:subClassOf ?taskType}
            }`,
    });
    if (res.results.bindings.length === 0) return undefined;

    return res.results.bindings.map((task) => {
      return {
        id: task.taskId.value,
        type: task.taskType.value,
        bedValue: task.bedValue?.value || "",
        bedUnit: task.bedUnit?.value || "",
        footValue: task.footValue?.value || "",
        footUnit: task.footUnit?.value || "",
        plantingDensityValue: task.plantingDensityValue?.value || "",
        plantingDensityUnit: task.plantingDensityUnit?.value || "",
        spacingByLineValue: task.spacingByLineValue?.value || "",
        spacingByLineUnit: task.spacingByLineUnit?.value || "",
        walkingSpaceValue: task.walkingSpaceValue?.value || "",
        walkingSpaceUnit: task.walkingSpaceUnit?.value || "",
        spacingInsideLineValue: task.spacingInsideLineValue?.value || "",
        spacingInsideLineUnit: task.spacingInsideLineUnit?.value || "",
        workLoadValue: task.workloadValue?.value || "",
        workLoadUnit: task.workloadUnit?.value.split("#")[1] || "",
      };
    })[0];
  }
}
