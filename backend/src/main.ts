import { createYoga } from "graphql-yoga";
import { createServer } from "http";
import { gatewaySchema } from "./schema";
import { RepositoryItkGraphDB } from "./itk/repository/repositoryItk";
import { Connection } from "./auth/function/connection";
require("dotenv").config();

async function main() {
  const yoga = createYoga({
    schema: gatewaySchema,
    context: async (req) => {
      const token = req.request.headers.get("Authorization");
      if (!token)
        return { repositoryItk: new RepositoryItkGraphDB(), token: null };
      return {
        repositoryItk: new RepositoryItkGraphDB(),
        repositoryConnection: new Connection(),
        token: token.split(" ")[1],
      };
    },
  });
  const server = createServer(yoga);
  server.on("error", (error) => {
    console.error(error);
  });

  server.listen(3038, () => {
    console.info("Server is running on http://localhost:3038/graphql");
  });
}

main();
