import prefix from "loglevel-plugin-prefix";
import log from "loglevel";
import { format } from "date-fns";

export function configureLog() {
  prefix.reg(log);

  prefix.apply(log, {
    // https://github.com/kutuluk/loglevel-plugin-prefix
    template: "[%t %l]",
    levelFormatter: function (level) {
      return level.toUpperCase().padStart(5, " ");
    },
    nameFormatter: function (name) {
      return name || "root";
    },
    timestampFormatter: function (date) {
      return format(date, "yyyy-MM-dd HH:mm:ss.SSSXXX");
    },
  });

  log.setLevel(log.levels.TRACE);
}
