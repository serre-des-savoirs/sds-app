import { result } from "lodash";
import {
  GraphDBClient,
  GraphLabelProperty,
  GraphProperty,
} from "../../graphDBClient";
import { faoLinkFormatter } from "../../utils/formatter";
import { plant } from "../api/domain";

interface PlantGraph {
  plantId: GraphProperty;
  plantName: GraphLabelProperty;
  typeVarietal?: GraphProperty;
  typeFamily?: GraphLabelProperty;
  wiki?: GraphProperty;
  narrowerId?: GraphProperty;
  narrower?: GraphLabelProperty;
  broaderId?: GraphProperty;
  broaderName?: GraphLabelProperty;
  relatedCrop?: GraphProperty;
  fao?: GraphProperty;
  taxRef?: GraphProperty;
  minStorage?: GraphProperty;
  maxStorage?: GraphProperty;
  unitTimeStorage?: GraphProperty;
  botanicalFamilyId?: GraphLabelProperty;
  botanicalFamilyName?: GraphProperty;
  usageFamilyId?: GraphProperty;
  usageFamilyName?: GraphLabelProperty;
}

export class RepositoryPlantGraphDB {
  private dbClient: GraphDBClient;
  constructor() {
    this.dbClient = new GraphDBClient({
      config: {
        endpoint: process.env.GRAPHDB_ENDPOINT as string,
        username: process.env.GRAPHDB_USER as string,
        password: process.env.GRAPHDB_PASS as string,
        repository: process.env.GRAPHDB_REPOSITORY as string,
      },
    });
  }

  async getAll(): Promise<plant[]> {
    const res = await this.dbClient.query<PlantGraph>({
      query: `
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX c3poplant: <http://www.elzeard.co/ontologies/c3po/plant#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        select ?plantId ?plantName ?botanicalFamilyId ?botanicalFamilyName ?usageFamilyId ?usageFamilyName where { 
          ?plantId a c3poplant:CultivatedPlant .
          ?plantId skos:prefLabel ?plantName .
          FILTER(LANGMATCHES(LANG(?plantName), 'fr'))
          OPTIONAL {
            ?botanicalFamilyId a c3poplant:BotanicalFamily .
            ?plantId skos:broader+ ?botanicalFamilyId .
            ?botanicalFamilyId rdfs:label ?botanicalFamilyName .
            FILTER(LANGMATCHES(LANG(?botanicalFamilyName), 'fr'))
          }
          OPTIONAL {
            ?usageFamilyId a c3poplant:UsageFamily .
            ?plantId skos:broader+ ?usageFamilyId .
            ?usageFamilyId rdfs:label ?usageFamilyName .
            FILTER(LANGMATCHES(LANG(?usageFamilyName), 'fr'))
          }
        }`,
    });

    const plantsById: Record<string, plant> = res.results.bindings.reduce(
      (plantsById, binding) => {
        const plantId = binding.plantId.value;
        if (!plantsById[plantId]) {
          plantsById[plantId] = {
            id: plantId,
            name: binding.plantName.value,
            botanicalFamilies: [],
            usageFamilies: [],
          };
        }

        if (
          binding.botanicalFamilyId?.value &&
          !plantsById[plantId].botanicalFamilies.some(
            (family) => family.id === binding.botanicalFamilyId.value
          )
        ) {
          plantsById[plantId].botanicalFamilies = plantsById[
            plantId
          ].botanicalFamilies.concat({
            id: binding.botanicalFamilyId.value,
            name: binding.botanicalFamilyId.value,
          });
        }
        if (
          binding.usageFamilyId?.value &&
          !plantsById[plantId].usageFamilies.some(
            (family) => family.id === binding.usageFamilyId.value
          )
        ) {
          plantsById[plantId].usageFamilies = plantsById[
            plantId
          ].usageFamilies.concat({
            id: binding.usageFamilyId.value,
            name: binding.usageFamilyName.value,
          });
        }
        return plantsById;
      },
      {} as Record<string, plant>
    );

    return Object.values(plantsById);
  }

  async getPlantById(id: string): Promise<plant | undefined> {
    const res = await this.dbClient.query<PlantGraph>({
      query: `
        PREFIX c3poplant: <http://www.elzeard.co/ontologies/c3po/plant#>
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX c3poparam: <http://www.elzeard.co/ontologies/c3po/parameter#>
        select * where { 
          VALUES (?plantId) {(<http://www.elzeard.co/ontologies/c3pokb#${id}>)}
          ?plantId a c3poplant:CultivatedPlant .
            ?plantId skos:prefLabel ?plantName .
            ?plantId c3poplant:isVarietalType ?typeVarietal .
            ?plantId skos:broader/rdfs:label ?typeFamily .
            OPTIONAL {
              ?botanicalFamilyId a c3poplant:BotanicalFamily .
              ?plantId skos:broader+ ?botanicalFamilyId .
              ?botanicalFamilyId rdfs:label ?botanicalFamilyName .
              FILTER(LANGMATCHES(LANG(?botanicalFamilyName), 'fr'))
            }
            OPTIONAL {
              ?usageFamilyId a c3poplant:UsageFamily .
              ?plantId skos:broader+ ?usageFamilyId .
              ?usageFamilyId rdfs:label ?usageFamilyName .
              FILTER(LANGMATCHES(LANG(?usageFamilyName), 'fr'))
            }
            OPTIONAL {
                ?plantId c3poplant:wikipediaLink ?wiki
                FILTER(LANGMATCHES(LANG(?wiki), 'fr'))
            }
            OPTIONAL {?plantId <http://ontology.inrae.fr/frenchcropusage/relatedCrop> ?relatedCrop}
            OPTIONAL {?plantId c3poplant:hasTaxonName ?taxRef}
            OPTIONAL {?plantId skos:closeMatch ?fao}
            OPTIONAL {
                ?plantId skos:narrower ?narrowerId .
                ?plantId skos:narrower/rdfs:label ?narrower
                FILTER(LANGMATCHES(LANG(?narrower), 'fr'))
            }
            OPTIONAL {
                ?plantId skos:broader ?broaderId .
                ?broaderId rdfs:label ?broaderName .
                ?broaderId c3poplant:hasTaxonName ?taxRef .
                ?broaderId skos:closeMatch ?fao .
                ?broaderId c3poplant:wikipediaLink ?wiki .
                ?broaderId <http://ontology.inrae.fr/frenchcropusage/relatedCrop> ?relatedCrop .
                FILTER(LANGMATCHES(LANG(?broaderName), 'fr'))
            }
            OPTIONAL {
                ?plantId c3poplant:stores/c3poplant:hasDuration/c3poparam:minValue ?minStorage .
                ?plantId c3poplant:stores/c3poplant:hasDuration/c3poparam:maxValue ?maxStorage .
                ?plantId c3poplant:stores/c3poplant:hasDuration/c3poparam:hasParameterUnit ?unitTimeStorage .
            }
            FILTER(LANGMATCHES(LANG(?typeFamily), 'fr'))
            FILTER(LANGMATCHES(LANG(?plantName), 'fr'))
        }`,
    });

    if (res.results.bindings.length === 0) return undefined;

    const broader:
      | {
          [key: string]: {
            taxRef: string;
            fao: string;
            wiki: string;
            relatedCrop: string;
          };
        }
      | undefined = res.results.bindings.reduce((acc, plant) => {
      if (plant.broaderId && !acc[plant.broaderId.value]) {
        acc[plant.broaderId.value] = {
          taxRef: plant.taxRef?.value,
          fao: plant.fao?.value,
          wiki: plant.wiki?.value,
          relatedCrop: plant.relatedCrop?.value,
        };
      }
      return acc;
    }, {} as { [key: string]: { taxRef: string; fao: string; wiki: string; relatedCrop: string } } | undefined);

    const broaderKey = Object.keys(broader)[0]?.split("#")[1];
    const broaderValues = Object.values(broader)[0];

    const botanicalFamilies = res.results.bindings.reduce(
      (botanicalFamilies, binding) => {
        if (
          !botanicalFamilies.some(
            (f) => f.id === binding.botanicalFamilyId.value
          )
        ) {
          botanicalFamilies = botanicalFamilies.concat({
            id: binding.botanicalFamilyId.value,
            name: binding.botanicalFamilyName.value,
          });
        }
        return botanicalFamilies;
      },
      [] as { id: string; name: string }[]
    );
    const usageFamilies = res.results.bindings.reduce(
      (usageFamilies, binding) => {
        if (!usageFamilies.some((f) => f.id === binding.usageFamilyId.value)) {
          usageFamilies = usageFamilies.concat({
            id: binding.usageFamilyId.value,
            name: binding.usageFamilyName.value,
          });
        }
        return usageFamilies;
      },
      [] as { id: string; name: string }[]
    );

    return {
      id: id,
      broaderKey,
      name: res.results.bindings[0].plantName.value,
      typeVarietal: res.results.bindings[0].typeVarietal?.value || "unknown",
      typeFamily: res.results.bindings[0].typeFamily?.value,
      wiki: res.results.bindings[0].wiki?.value || broaderValues?.wiki,
      relatedCrop:
        res.results.bindings[0].relatedCrop?.value ||
        broaderValues?.relatedCrop,
      fao: res.results.bindings[0].fao?.value
        ? faoLinkFormatter(res.results.bindings[0].fao?.value) ||
          faoLinkFormatter(broaderValues?.fao)
        : "",
      taxRef: res.results.bindings[0].taxRef?.value || broaderValues?.taxRef,
      botanicalFamilies,
      usageFamilies,
      storage: {
        minStorage: res.results.bindings[0].minStorage?.value,
        maxStorage: res.results.bindings[0].maxStorage?.value,
        unitTimeStorage:
          res.results.bindings[0].unitTimeStorage?.value.split("#")[1],
      },
    };
  }
}
