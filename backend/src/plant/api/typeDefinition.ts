export const typeDefinitonsPlant = /* GraphQL */ `
  type Query {
    info: String!
    plants: [Plant!]!
    getPlant(id: ID!): Plant!
  }

  type Plant {
    id: ID!
    broaderKey: ID
    name: String!
    typeVarietal: String!
    typeFamily: String!
    wiki: String
    botanicalFamilies: [PlantFamily!]
    usageFamilies: [PlantFamily!]
    relatedCrop: String
    fao: String
    taxRef: String
    storage: Storage
  }

  type PlantFamily {
    id: String!
    name: String
  }

  type Storage {
    minStorage: String
    maxStorage: String
    unitTimeStorage: String
  }
`;
