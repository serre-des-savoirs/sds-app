import { RepositoryPlant } from "./domain";
import { RepositoryPlantGraphDB } from "../repository/repositoryPlant";

export async function plantResolver(repositoryPlant: RepositoryPlant) {
  return await repositoryPlant.getAll();
}

const emptyPlant = {
  id: "",
  name: "",
  description: "",
};

export async function plantResolverById(
  repositoryPlant: RepositoryPlant,
  id: string | undefined
) {
  if (id === undefined) return emptyPlant;
  const res = await repositoryPlant.getPlantById(id);
  return res || emptyPlant;
}

export const resolverPlant = {
  Query: {
    plants: async () => plantResolver(new RepositoryPlantGraphDB()),
    getPlant: async (parent: unknown, args: { id: string }) =>
      plantResolverById(new RepositoryPlantGraphDB(), args.id),
  },
};
