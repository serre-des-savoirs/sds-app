export type plant = {
  id: string;
  broaderKey?: string;
  name: string;
  typeVarietal?: string;
  typeFamily?: string;
  wiki?: string;
  botanicalFamilies?:
    | {
        id: string;
        name: string;
      }[]
    | undefined;
  usageFamilies?:
    | {
        id: string;
        name: string;
      }[]
    | undefined;
  relatedCrop?: string;
  fao?: string;
  taxRef?: string;
  storage?: storage;
};

export interface storage {
  minStorage?: string;
  maxStorage?: string;
  unitTimeStorage?: string;
}

export interface RepositoryPlant {
  getAll: () => Promise<plant[]>;
  getPlantById: (id: string) => Promise<plant | undefined>;
}
