export function faoLinkFormatter(value: string) {
  if (!value || value === "") return "";
  return "http://".concat(value.split("http%3A//")[1]);
}
