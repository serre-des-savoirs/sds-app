import { makeExecutableSchema } from "@graphql-tools/schema";
import { stitchSchemas } from "@graphql-tools/stitch";
import { resolverItk } from "./itk/api/resolverItk";
import { resolverPlant } from "./plant/api/resolverPlant";
import { resolverTask } from "./task/api/resolverTask";
import { typeDefinitonsItk } from "./itk/api/typeDefinition";
import { typeDefinitonsPlant } from "./plant/api/typeDefinition";
import { typeDefinitonsTask } from "./task/api/typeDefinition";
import { typeDefinitionsAuth } from "./auth/api/typeDefinition";
import { resolverAuth } from "./auth/api/resolverAuth";

export const schemaItk = makeExecutableSchema({
  typeDefs: [typeDefinitonsItk],
  resolvers: [resolverItk],
});

export const schemaPlant = makeExecutableSchema({
  typeDefs: [typeDefinitonsPlant],
  resolvers: [resolverPlant],
});

export const schemaTask = makeExecutableSchema({
  typeDefs: [typeDefinitonsTask],
  resolvers: [resolverTask],
});

export const schemaAuth = makeExecutableSchema({
  typeDefs: [typeDefinitionsAuth],
  resolvers: [resolverAuth],
});

export const gatewaySchema = stitchSchemas({
  subschemas: [schemaItk, schemaPlant, schemaTask, schemaAuth],
});
