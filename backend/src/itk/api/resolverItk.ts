import { RepositoryConnection } from "../../auth/api/domain";
import { Connection } from "../../auth/function/connection";
import { RepositoryItkGraphDB } from "../repository/repositoryItk";
import { RepositoryItk } from "./domain";

export async function itkResolver(repositoryItk: RepositoryItk) {
  return await repositoryItk.getAll();
}

const emptyItk = {
  id: "",
  name: "",
  description: "",
  cultureMode: "",
  plant: "",
  task: [],
};

export async function itkResolverById(
  repositoryItk: RepositoryItk,
  id: string | undefined
) {
  if (!id) {
    return emptyItk;
  }

  const itkId = id.indexOf("-i") > 1 ? id : `cropitinerary/${id}`;
  const res = await repositoryItk.getItkById(itkId);
  return res || emptyItk;
}

const emptyItkMutation = "";

export async function itkMutation(
  repositoryItk: RepositoryItk,
  id: string | undefined,
  newTitle: string,
  lang: string,
  token: string,
  connection: RepositoryConnection
) {
  if (!id) {
    return emptyItkMutation;
  }
  if ((await connection.getUser({ token })) === "error") {
    return "error: token is undefined";
  }
  return await repositoryItk.updateTitle(id, newTitle, lang);
}

export async function itkMutationRegion(
  repositoryItk: RepositoryItk,
  id: string | undefined,
  newRegion: string[],
  token: string,
  connection: RepositoryConnection
) {
  if (!id) {
    return [emptyItkMutation];
  }
  if ((await connection.getUser({ token })) === "error") {
    return "error: token is undefined";
  }
  repositoryItk.updateRegion(id, newRegion);
  return emptyItkMutation;
}

export async function itkResolverPlant(repositoryItk: RepositoryItk) {
  return await repositoryItk.getAllPlant();
}

export async function itkMutationPlant(
  repositoryItk: RepositoryItk,
  id: string | undefined,
  newPlant: string,
  token: string,
  connection: RepositoryConnection
) {
  if (!id) {
    return "error: id is undefined";
  }
  if ((await connection.getUser({ token })) === "error") {
    return "error: token is undefined";
  }
  await repositoryItk.updatePlant(id, newPlant);
  return emptyItkMutation;
}

export async function itkMutationSeason(
  repositoryItk: RepositoryItk,
  id: string | undefined,
  newSeason: string[],
  token: string,
  connection: RepositoryConnection
) {
  if (!id) {
    return ["error: id is undefined"];
  }
  if ((await connection.getUser({ token })) === "error") {
    return "error: token is undefined";
  }
  repositoryItk.updateSeason(id, newSeason);
  return emptyItkMutation;
}

export async function itkMutationClimate(
  repositoryItk: RepositoryItk,
  id: string | undefined,
  newClimat: string,
  token: string,
  connection: RepositoryConnection
) {
  if (!id) {
    return "error: id is undefined";
  }
  if ((await connection.getUser({ token })) === "error") {
    return "error: token is undefined";
  }
  repositoryItk.updateClimate(id, newClimat);
  return emptyItkMutation;
}

export async function itkResolverSeason(repositoryItk: RepositoryItk) {
  return await repositoryItk.getAllSeason();
}

export async function itkResolverClimate(repositoryItk: RepositoryItk) {
  return await repositoryItk.getAllClimate();
}

export async function itkMutationCultureMode(
  repositoryItk: RepositoryItk,
  id: string | undefined,
  newCultureMode: string,
  token: string,
  connection: RepositoryConnection
) {
  if (!id) {
    return "error: id is undefined";
  }
  if ((await connection.getUser({ token })) === "error") {
    return "error: token is undefined";
  }
  repositoryItk.updateCultureMode(id, newCultureMode);
  return emptyItkMutation;
}

export async function itkMutationImplantationMode(
  repositoryItk: RepositoryItk,
  id: string | undefined,
  newImplantationMode: string,
  token: string,
  connection: RepositoryConnection
) {
  if (!id) {
    return "error: id is undefined";
  }
  if ((await connection.getUser({ token })) === "error") {
    return "error: token is undefined";
  }
  repositoryItk.updateImplantationMode(id, newImplantationMode);
  return emptyItkMutation;
}

export async function itkMutationTypeSoil(
  repositoryItk: RepositoryItk,
  id: string | undefined,
  newTypeSoil: string,
  token: string,
  connection: RepositoryConnection
) {
  if (!id) {
    return "error: id is undefined";
  }
  if ((await connection.getUser({ token })) === "error") {
    return "error: token is undefined";
  }
  return await repositoryItk.updateTypeSoil(id, newTypeSoil);
}

export async function itkMutationIrrigationMode(
  repositoryItk: RepositoryItk,
  id: string | undefined,
  newIrrigationMode: string,
  token: string,
  connection: RepositoryConnection
) {
  if (!id) {
    return "error: id is undefined";
  }
  if ((await connection.getUser({ token })) === "error") {
    return "error: token is undefined";
  }
  return repositoryItk.updateIrrigationMode(id, newIrrigationMode);
}

export async function itkMutationFarmEquipment(
  repositoryItk: RepositoryItk,
  id: string | undefined,
  newFarmEquipment: string,
  token: string,
  connection: RepositoryConnection
) {
  if (!id) {
    return "error: id is undefined";
  }
  if ((await connection.getUser({ token })) === "error") {
    return "error: token is undefined";
  }
  return repositoryItk.updateFarmEquipment(id, newFarmEquipment);
}

export async function itkMutationTask(
  repositoryItk: RepositoryItk,
  id: string | undefined,
  title: string,
  duration: number,
  workloadValue: number,
  workloadUnit: string,
  token: string,
  connection: RepositoryConnection
) {
  if (!id) {
    return "error: id is undefined";
  }
  if ((await connection.getUser({ token })) === "error") {
    return "error: token is undefined";
  }
  return repositoryItk.updateTask(
    id,
    title,
    duration,
    workloadValue,
    workloadUnit
  );
}

export const resolverItk = {
  Query: {
    itks: async (
      _parent: unknown,
      _args: {},
      context: { repositoryItk: RepositoryItkGraphDB }
    ) => itkResolver(context.repositoryItk),
    getItk: async (
      parent: unknown,
      args: { id: string | undefined },
      context: { repositoryItk: RepositoryItkGraphDB }
    ) => itkResolverById(context.repositoryItk, args.id),
    getAllPlant: async (
      _parent: unknown,
      _args: {},
      context: { repositoryItk: RepositoryItkGraphDB }
    ) => itkResolverPlant(context.repositoryItk),
    getAllSeason: async (
      _parent: unknown,
      _args: {},
      context: { repositoryItk: RepositoryItkGraphDB }
    ) => itkResolverSeason(context.repositoryItk),
    getAllClimate: async (
      _parent: unknown,
      _args: {},
      context: { repositoryItk: RepositoryItkGraphDB }
    ) => itkResolverClimate(context.repositoryItk),
  },
  Mutation: {
    updateTitle: async (
      parent: unknown,
      args: { id: string; newTitle: string; lang: string },
      context: {
        repositoryItk: RepositoryItkGraphDB;
        repositoryConnection: Connection;
        token: string;
      }
    ) =>
      itkMutation(
        context.repositoryItk,
        args.id,
        args.newTitle,
        args.lang,
        context.token,
        context.repositoryConnection
      ),
    updateRegion: async (
      parent: unknown,
      args: { id: string; newRegion: string[] },
      context: {
        repositoryItk: RepositoryItkGraphDB;
        repositoryConnection: Connection;
        token: string;
      }
    ) =>
      itkMutationRegion(
        context.repositoryItk,
        args.id,
        args.newRegion,
        context.token,
        context.repositoryConnection
      ),
    updatePlant: async (
      parent: unknown,
      args: { id: string; newPlant: string },
      context: {
        repositoryItk: RepositoryItkGraphDB;
        repositoryConnection: Connection;
        token: string;
      }
    ) =>
      itkMutationPlant(
        context.repositoryItk,
        args.id,
        args.newPlant,
        context.token,
        context.repositoryConnection
      ),
    updateSeason: async (
      parent: unknown,
      args: { id: string; newSeason: string[] },
      context: {
        repositoryItk: RepositoryItkGraphDB;
        repositoryConnection: Connection;
        token: string;
      }
    ) =>
      itkMutationSeason(
        context.repositoryItk,
        args.id,
        args.newSeason,
        context.token,
        context.repositoryConnection
      ),
    updateClimate: async (
      parent: unknown,
      args: { id: string; newClimate: string },
      context: {
        repositoryItk: RepositoryItkGraphDB;
        repositoryConnection: Connection;
        token: string;
      }
    ) =>
      itkMutationClimate(
        context.repositoryItk,
        args.id,
        args.newClimate,
        context.token,
        context.repositoryConnection
      ),
    updateCultureMode: async (
      parent: unknown,
      args: { id: string; newCultureMode: string },
      context: {
        repositoryItk: RepositoryItkGraphDB;
        repositoryConnection: Connection;
        token: string;
      }
    ) =>
      itkMutationCultureMode(
        context.repositoryItk,
        args.id,
        args.newCultureMode,
        context.token,
        context.repositoryConnection
      ),
    updateImplantationMode: async (
      parent: unknown,
      args: { id: string; newImplantationMode: string },
      context: {
        repositoryItk: RepositoryItkGraphDB;
        repositoryConnection: Connection;
        token: string;
      }
    ) =>
      itkMutationImplantationMode(
        context.repositoryItk,
        args.id,
        args.newImplantationMode,
        context.token,
        context.repositoryConnection
      ),
    updateTypeSoil: async (
      parent: unknown,
      args: { id: string; newTypeSoil: string },
      context: {
        repositoryItk: RepositoryItkGraphDB;
        repositoryConnection: Connection;
        token: string;
      }
    ) =>
      itkMutationTypeSoil(
        context.repositoryItk,
        args.id,
        args.newTypeSoil,
        context.token,
        context.repositoryConnection
      ),
    updateIrrigationMode: async (
      parent: unknown,
      args: { id: string; newIrrigationMode: string },
      context: {
        repositoryItk: RepositoryItkGraphDB;
        repositoryConnection: Connection;
        token: string;
      }
    ) =>
      itkMutationIrrigationMode(
        context.repositoryItk,
        args.id,
        args.newIrrigationMode,
        context.token,
        context.repositoryConnection
      ),
    updateFarmEquipment: async (
      parent: unknown,
      args: { id: string; newFarmEquipment: string },
      context: {
        repositoryItk: RepositoryItkGraphDB;
        repositoryConnection: Connection;
        token: string;
      }
    ) =>
      itkMutationFarmEquipment(
        context.repositoryItk,
        args.id,
        args.newFarmEquipment,
        context.token,
        context.repositoryConnection
      ),
    updateTask: async (
      parent: unknown,
      args: {
        id: string;
        newTitle: string;
        duration: number;
        workloadValue: number;
        workloadUnit: string;
      },
      context: {
        repositoryItk: RepositoryItkGraphDB;
        repositoryConnection: Connection;
        token: string;
      }
    ) =>
      itkMutationTask(
        context.repositoryItk,
        args.id,
        args.newTitle,
        args.duration,
        args.workloadValue,
        args.workloadUnit,
        context.token,
        context.repositoryConnection
      ),
  },
};
