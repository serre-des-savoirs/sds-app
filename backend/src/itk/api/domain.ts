export type itk = {
  id: string;
  name: string;
  cultureMode: string;
  implantationMode?: string;
  plantId: string;
  source?: string;
  season: string[];
  task: taskReference[];
  hasRelativePeriod?: hasRelativePeriod;
  dependsOnCulturalContext?: culturalContext;
  dependsOnSoilClimateContext?: soilClimateContext;
  hasYield?: Yield;
  lossMargin?: string;
  hasGlobalWorload?: hasGlobalWorload;
  typeSoil: string;
  hasIrrigationMode: string;
  hasFarmEquipment: string;
};

export type itkAll = {
  id: string;
  name: string;
  cultureMode: string;
  plantId: string;
  dependsOnSoilClimateContext?: soilClimateContext;
  implantationMode?: string;
  parentItkId?: string | null;
};

interface hasGlobalWorload {
  parameterValue: number;
  hasParameterUnit: string;
}

interface Yield {
  maxYield: string;
  minYield: string;
  parameterValue: string;
  hasParameterUnit: string;
}

interface hasRelativePeriod {
  hasBeginning: string;
  hasEnding: string;
}

interface soilClimateContext {
  belongsToRegion: string[];
  hasClimateContext?: string;
}

interface culturalContext {
  cultivatesIn: string;
  hasCultivation: string;
}

export interface taskReference {
  taskId: string;
  taskName: string;
  taskType: string;
  implements: task;
  hasOperationDistance: hasOperationDistance;
  hasRelativePeriod: hasRelativePeriod;
}

interface hasOperationDistance {
  distanceDuration: {
    numericDuration: number;
    unitType: string;
  };
  refersToOperation: {
    id: string;
    type: string;
  };
}

interface task {
  hasBedWidth: hasBedWidth;
  hasFootPassWidth: hasFootPassWidth;
  hasPlantingDensity: hasPlantingDensity;
  hasSpacingByLine: hasSpacingByLine;
  hasSpacingInsideLine: hasSpacingInsideLine;
  hasWalkingSpaceWidth: hasWalkingSpaceWidth;
  hasWorkload: hasWorkload;
}

interface hasBedWidth {
  parameterValue: number;
  hasParameterUnit: string;
}

interface hasFootPassWidth {
  parameterValue: number;
  hasParameterUnit: string;
}

interface hasPlantingDensity {
  parameterValue: number;
  hasParameterUnit: string;
}

interface hasSpacingByLine {
  parameterValue: number;
  hasParameterUnit: string;
}

interface hasSpacingInsideLine {
  parameterValue: number;
  hasParameterUnit: string;
}

interface hasWalkingSpaceWidth {
  parameterValue: number;
  hasParameterUnit: string;
}

interface hasWorkload {
  parameterValue: number;
  hasWorkloadUnit: string;
}

export interface RepositoryItk {
  getAll: () => Promise<itkAll[]>;
  getItkById: (id: string) => Promise<itk | undefined>;
  getAllPlant: () => Promise<{ id: string; plantName: string }[]>;
  getAllSeason: () => Promise<{ id: string; seasonName: string }[]>;
  getAllClimate: () => Promise<{ id: string; climateName: string }[]>;
  updateTitle: (id: string, newTitle: string, lang: string) => Promise<String>;
  updateRegion: (id: string, newRegion: string[]) => void;
  updatePlant: (id: string, newPlant: string) => void;
  updateSeason: (id: string, newSeason: string[]) => void;
  updateClimate: (id: string, newClimate: string) => void;
  updateCultureMode: (id: string, newCultureMode: string) => void;
  updateImplantationMode: (id: string, newImplementationMode: string) => void;
  updateTypeSoil: (
    id: string,
    newTypeSoil: string
  ) => Promise<"success" | "error">;
  updateIrrigationMode: (
    id: string,
    newIrrigationMode: string
  ) => Promise<"success" | "error">;
  updateFarmEquipment: (
    id: string,
    newFarmEquipment: string
  ) => Promise<"success" | "error">;
  updateTask: (
    id: string,
    title: string,
    duration: number,
    workloadValue: number,
    workloadUnit: string
  ) => void;
}
