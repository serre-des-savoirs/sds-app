export const typeDefinitonsItk = /* GraphQL */ `
  type Query {
    itks: [Itk!]!
    getItk(id: ID!): Itk!
    getAllPlant: [GetPlant!]!
    getAllClimate: [getClimate!]!
    getAllSeason: [getSeason!]!
  }

  type Mutation {
    updateTitle(id: ID!, newTitle: String!, lang: String!): String!
    updateRegion(id: ID!, newRegion: [String!]!): String!
    updatePlant(id: ID!, newPlant: String!): String!
    updateSeason(id: ID!, newSeason: [String!]!): String!
    updateClimate(id: ID!, newClimate: String!): String!
    updateCultureMode(id: ID!, newCultureMode: String!): String!
    updateImplantationMode(id: ID!, newImplantationMode: String!): String!
    updateTypeSoil(id: ID!, newTypeSoil: String!): String!
    updateIrrigationMode(id: ID!, newIrrigationMode: String!): String!
    updateFarmEquipment(id: ID!, newFarmEquipment: String!): String!
    updateTask(
      id: ID!
      newTitle: String!
      duration: Int!
      workloadValue: Int!
      workloadUnit: String!
    ): String!
  }

  type getSeason {
    id: ID
    seasonName: String
  }

  type getClimate {
    id: ID
    climateName: String
  }

  type GetPlant {
    id: ID
    plantName: String
  }

  type hasBedWidth {
    parameterValue: Float
    hasParameterUnit: String
  }

  type hasFootPassWidth {
    parameterValue: Float
    hasParameterUnit: String
  }

  type hasPlantingDensity {
    parameterValue: Float
    hasParameterUnit: String
  }

  type hasSpacingByLine {
    parameterValue: Float
    hasParameterUnit: String
  }

  type hasSpacingInsideLine {
    parameterValue: Float
    hasParameterUnit: String
  }

  type hasWalkingSpaceWidth {
    parameterValue: Float
    hasParameterUnit: String
  }

  type hasWorkload {
    parameterValue: Float
    hasWorkloadUnit: String
  }

  type distanceDuration {
    numericDuration: Float
    unitType: String
  }

  type hasOperationDistance {
    distanceDuration: distanceDuration
    refersToOperation: OperationDistanceReference
  }

  type OperationDistanceReference {
    id: String!
    type: String!
  }

  type Task {
    hasBedWidth: hasBedWidth
    hasFootPassWidth: hasFootPassWidth
    hasPlantingDensity: hasPlantingDensity
    hasSpacingByLine: hasSpacingByLine
    hasSpacingInsideLine: hasSpacingInsideLine
    hasWalkingSpaceWidth: hasWalkingSpaceWidth
    hasWorkload: hasWorkload
  }

  type taskReference {
    taskId: String
    taskType: String
    taskName: String
    implements: Task
    hasOperationDistance: hasOperationDistance
    hasRelativePeriod: hasRelativePeriod
  }

  type hasRelativePeriod {
    hasBeginning: String
    hasEnding: String
  }

  type culturalContext {
    cultivatesIn: String
    hasCultivation: String
  }

  type soilClimateContext {
    belongsToRegion: [String]
    hasClimateContext: String
  }

  type Yield {
    averageYield: String
    maxYield: String
    minYield: String
    parameterValue: String
    hasParameterUnit: String
  }

  type hasGlobalWorload {
    parameterValue: Int
    hasParameterUnit: String
  }

  type Itk {
    id: ID
    name: String
    cultureMode: String
    implantationMode: String
    plantId: String
    source: String
    season: [String]
    task: [taskReference]
    hasRelativePeriod: hasRelativePeriod
    dependsOnCulturalContext: culturalContext
    dependsOnSoilClimateContext: soilClimateContext
    hasYield: Yield
    lossMargin: String
    hasGlobalWorload: hasGlobalWorload
    hasFarmEquipment: String
    typeSoil: String
    hasIrrigationMode: String
  }
`;
