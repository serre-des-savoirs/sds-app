import { id } from "date-fns/locale";
import {
  GraphDBClient,
  GraphLabelProperty,
  GraphProperty,
} from "../../graphDBClient";
import { RepositoryItk, itk, itkAll, taskReference } from "../api/domain";
import {
  DELETE_REGION,
  DELETE_SEASON,
  INSERT_REGION,
  INSERT_SEASON,
  MUTATION_CLIMATE,
  MUTATION_CULTURE_MODE,
  MUTATION_FARM_EQUIPMENT,
  MUTATION_IMPLANTATION_MODE,
  MUTATION_IRRIGATION_MODE,
  MUTATION_PLANT,
  MUTATION_TASKS,
  MUTATION_TYPE_SOIL,
  QUERY_GET_ALL_PLANT,
  QUERY_GET_CLIMATE,
  QUERY_GET_FARM_EQUIPMENT_EXIST,
  QUERY_GET_IRRIGATION_MODE_EXIST,
  QUERY_GET_ITK_BY_ID,
  QUERY_GET_ITK_TASK_BY_ID,
  QUERY_GET_SEASON,
  QUERY_GET_TYPE_SOIL_EXIST,
  QUERY_UPDATE_ITK_TITLE,
} from "./query";
import { groupBy, identity } from "lodash";

const C3POVOC_PREFIX = "http://www.elzeard.co/ontologies/c3po/vocabulary";

const OPEN_FIELD_URI = `${C3POVOC_PREFIX}#UnderCover`;
const UNDER_COVER_URI = `${C3POVOC_PREFIX}#OpenField`;

interface ItkGraph {
  itk: GraphProperty;
  itkName: GraphLabelProperty;
  plantId: GraphProperty;
  source: GraphProperty;
  parentItk?: GraphProperty;
  cultivatesIn: GraphLabelProperty;
  hasCultivation: GraphLabelProperty;
  region: GraphLabelProperty;
  climate: GraphLabelProperty;
  cultureMode: GraphProperty;
  globalWorloadValue: GraphProperty;
  globalWorloadUnit: GraphLabelProperty;
  implantationMode: GraphLabelProperty;
  season: GraphLabelProperty;
  typeSol: GraphLabelProperty;
  hasIrrigationMode: GraphProperty;
  hasFarmEquipment: GraphLabelProperty;
  relativePeriodBeg: GraphProperty;
  relativePeriodEnd: GraphProperty;
  averageValueYield: GraphProperty;
  minValueYield: GraphProperty;
  maxValueYield: GraphProperty;
  parameterValueYield: GraphProperty;
  parameterUnitYield: GraphProperty;
  yieldUnitYield: GraphProperty;
  lossMargin: GraphProperty;
}

interface TaskGraph {
  taskId: GraphProperty;
  taskName: GraphLabelProperty;
  taskTypeName: GraphLabelProperty;
  taskType: GraphProperty;
  bedValue: GraphProperty;
  bedUnit: GraphProperty;
  footValue: GraphProperty;
  footUnit: GraphProperty;
  plantingDensityValue: GraphProperty;
  plantingDensityUnit: GraphProperty;
  spacingByLineValue: GraphProperty;
  spacingByLineUnit: GraphProperty;
  walkingSpaceValue: GraphProperty;
  walkingSpaceUnit: GraphProperty;
  spacingInsideLineValue: GraphProperty;
  spacingInsideLineUnit: GraphProperty;
  workloadValue: GraphProperty;
  workloadUnit: GraphProperty;
  distanceValueDuration: GraphProperty;
  distanceUnitDuration: GraphProperty;
  relativePeriodBeg: GraphProperty;
  relativePeriodEnd: GraphProperty;
  refTaskId: GraphProperty;
}

export class RepositoryItkGraphDB implements RepositoryItk {
  private dbClient: GraphDBClient;

  constructor() {
    this.dbClient = new GraphDBClient({
      config: {
        endpoint: process.env.GRAPHDB_ENDPOINT as string,
        username: process.env.GRAPHDB_USER as string,
        password: process.env.GRAPHDB_PASS as string,
        repository: process.env.GRAPHDB_REPOSITORY as string,
      },
    });
  }

  private convertRelativePeriod(baseDate: string, weeksToAdd: string) {
    let baseDateStr: String = "";
    if (baseDate.indexOf("Y") === -1) {
      baseDateStr = `0Y${baseDate}`;
    } else {
      baseDateStr = baseDate;
    }
    const [baseYearsStr, baseWeeksStr] = baseDateStr.split("Y");
    const baseYears = parseInt(baseYearsStr);
    const baseWeeks = parseInt(baseWeeksStr);
    const weeksToAddNum = parseInt(weeksToAdd);
    const totalWeeks = baseYears * 52 + baseWeeks + weeksToAddNum;
    const years = Math.floor(totalWeeks / 52);
    const remainingWeeks = totalWeeks % 52;

    return `${years}Y${remainingWeeks}W`;
  }

  async getAll(): Promise<itkAll[]> {
    const res = await this.dbClient.query<ItkGraph>({
      query: `
        PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        select * where { 
            ?itk a c3pocm:CropItinerary .
            ?itk rdfs:label ?itkName .
            ?itk c3pocm:concernsPlant ?plantId .
            ?plantId skos:prefLabel ?plantName .
            OPTIONAL {
              ?itk c3pocm:hasCultureMode ?cultureMode .
            }
            OPTIONAL {
              ?itk c3pocm:hasImplantationMode ?implantationMode .
            }
            OPTIONAL {
              ?itk c3pocm:dependsOnSoilClimateContext/c3pocm:belongsToRegion ?region .
            }
            OPTIONAL {
              ?itk c3pocm:isSubItineraryOf ?parentItk
            }
            FILTER(LANGMATCHES(LANG(?itkName), 'fr'))
            FILTER(LANGMATCHES(LANG(?plantName), 'fr'))
        }`,
    });

    const itksById: Record<string, itkAll> = res.results.bindings.reduce(
      (itksById, binding) => {
        const itkId = binding.itk.value;
        if (!itksById[itkId]) {
          itksById[itkId] = {
            id: itkId,
            name: binding.itkName.value,
            plantId: binding.plantId.value,
            cultureMode: binding.cultureMode?.value,
            implantationMode: binding.implantationMode?.value,
            dependsOnSoilClimateContext: {
              belongsToRegion: [],
            },
            parentItkId: binding.parentItk?.value || null,
          };
        }

        if (
          binding.region?.value &&
          itksById[itkId].dependsOnSoilClimateContext.belongsToRegion.indexOf(
            binding.region.value
          ) === -1
        ) {
          itksById[itkId].dependsOnSoilClimateContext.belongsToRegion.push(
            binding.region.value
          );
        }
        return itksById;
      },
      {} as Record<string, itkAll>
    );

    const itksGroupedByParent = groupBy(
      Object.values(itksById),
      (itk) => itk.parentItkId
    );

    const parentIds = Object.keys(itksGroupedByParent);
    const itksWithoutParentAndWithoutChildren = itksGroupedByParent[
      "null"
    ].filter((itk) => parentIds.indexOf(itk.id) === -1);

    // Pour les itks avec parent, on prend un plein champ et un sous-abri
    const someChildrenItks = Object.entries(itksGroupedByParent)
      .flatMap(([parentId, group]) => {
        if (parentId === "null") {
          return [];
        }
        return [
          group.find((itk) => itk.cultureMode === OPEN_FIELD_URI),
          group.find((itk) => itk.cultureMode === UNDER_COVER_URI),
        ];
      })
      .filter(identity);

    return itksWithoutParentAndWithoutChildren
      .concat(someChildrenItks)
      .map((itk) => ({
        ...itk,
        cultureMode: itk.cultureMode?.split("#")[1],
        implantationMode: itk.implantationMode?.split("#")[1],
        dependsOnSoilClimateContext: {
          belongsToRegion: itk.dependsOnSoilClimateContext.belongsToRegion.map(
            (region) => region.split("#")[1]
          ),
        },
      }));
  }

  async getItkById(id: string): Promise<itk | undefined> {
    const res = await this.dbClient.query<ItkGraph>({
      query: QUERY_GET_ITK_BY_ID(id),
    });

    if (res.results.bindings.length === 0) return undefined;

    const resTask = await this.dbClient.query<TaskGraph>({
      query: QUERY_GET_ITK_TASK_BY_ID(id),
    });

    const taskPracticeTypesById = resTask.results.bindings.reduce(
      (acc, binding) => ({
        ...acc,
        [binding.taskId.value]: binding.taskType.value,
      }),
      {} as Record<string, string>
    );

    const taskList: taskReference[] = resTask.results.bindings
      .filter((task) => {
        return (
          task.taskId !== undefined &&
          (!task.taskName ||
            !task.taskName["xml:lang"] ||
            task.taskName["xml:lang"] === "fr") &&
          (!task.taskTypeName ||
            !task.taskTypeName["xml:lang"] ||
            task.taskTypeName["xml:lang"] === "fr")
        );
      })
      .map((task) => ({
        taskId: task.taskId.value,
        taskName: task.taskName?.value || task.taskTypeName?.value,
        taskType: task.taskType?.value,
        implements: {
          hasBedWidth: {
            parameterValue: Number(task.bedValue?.value || "0"),
            hasParameterUnit: task.bedUnit?.value,
          },
          hasFootPassWidth: {
            parameterValue: Number(task.footValue?.value || "0"),
            hasParameterUnit: task.footUnit?.value,
          },
          hasPlantingDensity: {
            parameterValue: Number(task.plantingDensityValue?.value || "0"),
            hasParameterUnit: task.plantingDensityUnit?.value,
          },
          hasSpacingByLine: {
            parameterValue: Number(task.spacingByLineValue?.value || "0"),
            hasParameterUnit: task.spacingByLineUnit?.value,
          },
          hasWalkingSpaceWidth: {
            parameterValue: Number(task.walkingSpaceValue?.value || "0"),
            hasParameterUnit: task.walkingSpaceUnit?.value,
          },
          hasSpacingInsideLine: {
            parameterValue: Number(task.spacingInsideLineValue?.value || "0"),
            hasParameterUnit: task.spacingInsideLineUnit?.value,
          },
          hasWorkload: {
            parameterValue: Number(task.workloadValue?.value || "0"),
            hasWorkloadUnit: task.workloadUnit?.value,
          },
        },
        hasOperationDistance: {
          distanceDuration: {
            numericDuration: Number(task.distanceValueDuration?.value || 0),
            unitType: task.distanceUnitDuration?.value,
          },
          refersToOperation: task.refTaskId
            ? {
                id: task.refTaskId.value,
                type: taskPracticeTypesById[task.refTaskId.value],
              }
            : null,
        },
        hasRelativePeriod: {
          hasBeginning: task.relativePeriodBeg?.value,
          hasEnding: task.relativePeriodEnd?.value,
        },
      }))
      .sort((a, b) => a.taskId.localeCompare(b.taskId));

    const season = res.results.bindings.reduce((acc, curr) => {
      if (curr.season && !acc.includes(curr.season.value)) {
        acc.push(curr.season.value);
      }
      return acc;
    }, [] as string[]);

    const regions = [
      ...new Set(
        res.results.bindings
          .map((binding) => binding.region?.value?.split("#")[1])
          .filter((region) => !!region)
      ),
    ];

    return {
      id: id,
      name: res.results.bindings[0].itkName.value,
      cultureMode: res.results.bindings[0].cultureMode?.value.split("#")[1],
      plantId: res.results.bindings[0].plantId.value,
      source: res.results.bindings[0].source?.value,
      season: season,
      task: taskList,
      typeSoil: res.results.bindings[0].typeSol?.value,
      hasIrrigationMode: res.results.bindings[0].hasIrrigationMode?.value,
      hasFarmEquipment: res.results.bindings[0].hasFarmEquipment?.value,
      hasRelativePeriod: {
        hasBeginning: res.results.bindings[0].relativePeriodBeg?.value,
        hasEnding: res.results.bindings[0].relativePeriodEnd?.value,
      },
      hasYield: {
        maxYield: res.results.bindings[0].maxValueYield?.value,
        minYield: res.results.bindings[0].minValueYield?.value,
        parameterValue:
          res.results.bindings[0].parameterValueYield?.value ||
          res.results.bindings[0].averageValueYield?.value,
        hasParameterUnit:
          res.results.bindings[0].parameterUnitYield?.value ||
          res.results.bindings[0].yieldUnitYield?.value,
      },
      dependsOnSoilClimateContext: {
        belongsToRegion: regions,
        hasClimateContext: res.results.bindings[0].climate?.value,
      },
      dependsOnCulturalContext: {
        cultivatesIn: res.results.bindings[0].cultivatesIn?.value,
        hasCultivation: res.results.bindings[0].hasCultivation?.value,
      },
      hasGlobalWorload: {
        parameterValue: Number(
          res.results.bindings[0].globalWorloadValue?.value || null
        ),
        hasParameterUnit: res.results.bindings[0].globalWorloadUnit?.value,
      },
      implantationMode: res.results.bindings[0].implantationMode?.value,
      lossMargin: res.results.bindings[0].lossMargin?.value,
    };
  }

  async updateTitle(
    id: string,
    newTitle: string,
    lang: string
  ): Promise<String> {
    await this.dbClient.update({
      update: QUERY_UPDATE_ITK_TITLE(id, newTitle, lang),
    });

    return newTitle;
  }

  updateRegion(id: string, region: string[]): void {
    this.dbClient.update({
      update: DELETE_REGION(id),
    });
    region.forEach((el) => {
      this.dbClient.update({
        update: INSERT_REGION(id, el),
      });
    });
  }

  async getAllPlant(): Promise<{ id: string; plantName: string }[]> {
    const res = await this.dbClient.query<{
      plantName: GraphLabelProperty;
      plant: GraphProperty;
    }>({
      query: QUERY_GET_ALL_PLANT(),
    });
    if (res.results.bindings.length === 0) return [];

    return res.results.bindings.map((el) => {
      return {
        id: el.plant.value.split("#")[1],
        plantName: el.plantName.value,
      };
    });
  }

  async updatePlant(id: string, plantId: string): Promise<void> {
    await this.dbClient.update({
      update: MUTATION_PLANT(id, plantId),
    });
  }

  async updateClimate(id: string, climate: string): Promise<void> {
    await this.dbClient.update({
      update: MUTATION_CLIMATE(id, climate),
    });
  }

  async updateSeason(id: string, season: string[]): Promise<void> {
    await this.dbClient.update({
      update: DELETE_SEASON(id),
    });

    season.forEach((el) => {
      this.dbClient.update({
        update: INSERT_SEASON(id, el),
      });
    });
  }

  async getAllClimate(): Promise<{ id: string; climateName: string }[]> {
    const res = await this.dbClient.query<{
      climateName: GraphLabelProperty;
      climate: GraphProperty;
    }>({
      query: QUERY_GET_CLIMATE(),
    });

    if (res.results.bindings.length === 0) return [];

    return res.results.bindings.map((el) => {
      return {
        id: el.climate.value.split("#")[1],
        climateName: el.climateName.value,
      };
    });
  }

  async getAllSeason(): Promise<{ id: string; seasonName: string }[]> {
    const res = await this.dbClient.query<{
      seasonName: GraphLabelProperty;
      season: GraphProperty;
    }>({
      query: QUERY_GET_SEASON(),
    });

    if (res.results.bindings.length === 0) return [];

    return res.results.bindings.map((el) => {
      return {
        id: el.season.value.split("#")[1],
        seasonName: el.seasonName.value,
      };
    });
  }

  async updateCultureMode(id: string, cultureMode: string): Promise<void> {
    await this.dbClient.update({
      update: MUTATION_CULTURE_MODE(id, cultureMode),
    });
  }

  async updateImplantationMode(
    id: string,
    implantationMode: string
  ): Promise<void> {
    await this.dbClient.update({
      update: MUTATION_IMPLANTATION_MODE(id, implantationMode),
    });
  }

  async updateTypeSoil(
    id: string,
    typeSoil: string
  ): Promise<"success" | "error"> {
    const res = await this.dbClient.query<{ typeSol: GraphProperty }>({
      query: QUERY_GET_TYPE_SOIL_EXIST(id),
    });
    if (res.results.bindings[0].typeSol) {
      await this.dbClient.update({
        update: MUTATION_TYPE_SOIL(id, typeSoil),
      });
      return "success";
    }
    return "error";
  }

  async updateIrrigationMode(
    id: string,
    irrigationMode: string
  ): Promise<"success" | "error"> {
    const res = await this.dbClient.query<{ hasIrrigationMode: GraphProperty }>(
      {
        query: QUERY_GET_IRRIGATION_MODE_EXIST(id),
      }
    );
    if (res.results.bindings[0].hasIrrigationMode) {
      await this.dbClient.update({
        update: MUTATION_IRRIGATION_MODE(id, irrigationMode),
      });
      return "success";
    }
    return "error";
  }

  async updateFarmEquipment(
    id: string,
    farmEquipment: string
  ): Promise<"success" | "error"> {
    const res = await this.dbClient.query<{ hasFarmEquipment: GraphProperty }>({
      query: QUERY_GET_FARM_EQUIPMENT_EXIST(id),
    });
    if (res.results.bindings[0].hasFarmEquipment) {
      await this.dbClient.update({
        update: MUTATION_FARM_EQUIPMENT(id, farmEquipment),
      });
      return "success";
    }
    return "error";
  }

  async updateTask(
    id: string,
    title: string,
    duration: number,
    workloadValue: number,
    workloadUnit: string
  ): Promise<string> {
    await this.dbClient.update({
      update: MUTATION_TASKS(id, title, duration, workloadValue, workloadUnit),
    });
    return "success";
  }
}
