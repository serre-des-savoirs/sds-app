export const QUERY_GET_ITK_BY_ID = (id: string) => `
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX c3potime: <http://www.elzeard.co/ontologies/c3po/time#>
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3poparam: <http://www.elzeard.co/ontologies/c3po/parameter#>
PREFIX time: <http://www.w3.org/2006/time#>
select * where {
    VALUES (?itk) {
		(<http://www.elzeard.co/ontologies/c3pokb#${id}>)
	}
	?itk a c3pocm:CropItinerary .
    ?itk rdfs:label ?itkName .
    ?itk c3pocm:concernsPlant ?plantId .
    OPTIONAL {
        ?itk dc:source ?source
    }
    OPTIONAL {
        ?itk c3pocm:dependsOnSoilClimateContext/c3pocm:hasSoilContext/c3pocm:hasSoilNature/skos:prefLabel ?typeSol .
        FILTER(LANGMATCHES(LANG(?typeSol), 'fr'))
    }
    OPTIONAL {
        ?itk c3pocm:hasIrrigationMode/skos:prefLabel ?hasIrrigationMode .
        FILTER(LANGMATCHES(LANG(?hasIrrigationMode), 'fr'))
    }
    OPTIONAL {
        ?itk c3pocm:dependsOnCulturalContext/c3pocm:hasFarmEquipmentLevel/skos:prefLabel ?hasFarmEquipment
        FILTER(LANGMATCHES(LANG(?hasFarmEquipment), 'fr'))
    }
    OPTIONAL {
        ?itk c3pocm:dependsOnSoilClimateContext/c3pocm:belongsToRegion ?region .
    }
    OPTIONAL {
      OPTIONAL {
         ?itk c3pocm:dependsOnCulturalContext/c3pocm:cultivatesIn/skos:prefLabel ?cultivatesIn .
         FILTER(LANGMATCHES(LANG(?cultivatesIn), 'fr'))
      }
      OPTIONAL {
         ?itk c3pocm:dependsOnCulturalContext/c3pocm:hasCultivation/skos:prefLabel ?hasCultivation .
         FILTER(LANGMATCHES(LANG(?hasCultivation), 'fr'))
      }
    }
    OPTIONAL {
      OPTIONAL {
        ?itk c3pocm:dependsOnSoilClimateContext/c3pocm:belongsToRegion/skos:prefLabel ?region .
        FILTER(LANGMATCHES(LANG(?region), 'fr'))
      }
      OPTIONAL {
        ?itk c3pocm:dependsOnSoilClimateContext/c3pocm:hasClimateContext/c3pocm:hasClimate/skos:prefLabel ?climate .
        FILTER(LANGMATCHES(LANG(?climate), 'fr'))
      }
    }
    OPTIONAL {
        ?itk c3pocm:hasCultureMode ?cultureMode .
    }
    OPTIONAL {
        ?itk c3pocm:hasGlobalWorload/c3poparam:parameterValue ?globalWorloadValue .
        ?itk c3pocm:hasGlobalWorload/c3poparam:hasParameterUnit ?globalWorloadUnit .
    }
    OPTIONAL {
        ?itk c3pocm:hasImplantationMode/skos:prefLabel ?implantationMode .
        FILTER(LANGMATCHES(LANG(?implantationMode), 'fr'))
    }
    OPTIONAL {
        ?itk c3pocm:hasRelativePeriod/c3potime:hasBeginning/c3potime:inRDate ?relativePeriodBeg .
        ?itk c3pocm:hasRelativePeriod/c3potime:hasEnding/c3potime:inRDate ?relativePeriodEnd .
    }
    OPTIONAL {
        ?itk c3pocm:hasSeasonality/skos:prefLabel ?season .
        FILTER(LANGMATCHES(LANG(?season), 'fr'))
    }
    OPTIONAL {
        ?itk c3pocm:hasYield/c3poparam:parameterValue ?parameterValueYield .
        ?itk c3pocm:hasYield/c3poparam:hasParameterUnit ?parameterUnitYield .
    }
    OPTIONAL {
        ?itk c3pocm:hasYield/c3poparam:averageYield ?averageValueYield .
        ?itk c3pocm:hasYield/c3poparam:minYield ?minYield .
        ?itk c3pocm:hasYield/c3poparam:maxYield ?maxYield .
        ?itk c3pocm:hasYield/c3poparam:hasYieldUnit ?yieldUnitYield .
    }
    OPTIONAL {
        ?itk c3pocm:lossMargin ?lossMargin .
    }
    FILTER(LANGMATCHES(LANG(?itkName), 'fr'))
}`;

export const QUERY_GET_ITK_TASK_BY_ID = (id: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX c3potime: <http://www.elzeard.co/ontologies/c3po/time#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX c3poparam: <http://www.elzeard.co/ontologies/c3po/parameter#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX time: <http://www.w3.org/2006/time#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
select * where { 
	VALUES (?itk) {
		(<http://www.elzeard.co/ontologies/c3pokb#${id}>)
	}
    ?itk a c3pocm:CropItinerary .
    ?itk c3pocm:hasOperationMember ?taskId .
    OPTIONAL {
        ?taskId c3pocm:implements/rdf:type ?taskType .
        FILTER NOT EXISTS { ?parentType rdfs:subClassOf ?taskType }
        OPTIONAL {
            ?taskType rdfs:label ?taskTypeName .
            FILTER(LANGMATCHES(LANG(?taskTypeName), 'fr'))
        }
        OPTIONAL {
            ?taskId rdfs:label ?taskName .
        }
        OPTIONAL {
            ?taskId c3pocm:implements/c3pocm:hasBedWidth/c3poparam:parameterValue ?bedValue .
            ?taskId c3pocm:implements/c3pocm:hasBedWidth/c3poparam:parameterUnit ?bedUnit .
        }
        OPTIONAL {
            ?taskId c3pocm:implements/c3pocm:hasFootPassWidth/c3poparam:parameterValue ?footValue .
            ?taskId c3pocm:implements/c3pocm:hasFootPassWidth/c3poparam:parameterUnit ?footUnit .
        }
        OPTIONAL {
            ?taskId c3pocm:implements/c3pocm:hasBedPlantingDensity/c3poparam:parameterValue ?plantingDensityValue .
            ?taskId c3pocm:implements/c3pocm:hasBedPlantingDensity/c3poparam:parameterUnit ?plantingDensityUnit .
        }
        OPTIONAL {
            ?taskId c3pocm:implements/c3pocm:spacingBetweenRows/c3poparam:parameterValue ?spacingByLineValue .
            ?taskId c3pocm:implements/c3pocm:spacingBetweenRows/c3poparam:parameterUnit ?spacingByLineUnit .
        }
        OPTIONAL {
            ?taskId c3pocm:implements/c3pocm:spacingInRow/c3poparam:parameterValue ?spacingInsideLineValue .
            ?taskId c3pocm:implements/c3pocm:spacingInRow/c3poparam:parameterUnit ?spacingInsideLineUnit .
        }
        OPTIONAL {
            ?taskId c3pocm:implements/c3pocm:hasWalkingSpaceWidth/c3poparam:parameterValue ?walkingSpaceValue .
            ?taskId c3pocm:implements/c3pocm:hasWalkingSpaceWidth/c3poparam:parameterUnit ?walkingSpaceUnit .
        }
        OPTIONAL {
            ?taskId c3pocm:implements/c3pocm:hasWorkload/c3poparam:parameterValue ?workloadValue .
            ?taskId c3pocm:implements/c3pocm:hasWorkload/c3poparam:parameterUnit ?workloadUnit .
        }
        OPTIONAL {
            ?taskId c3pocm:hasRelativePeriod/c3potime:hasBeginning/c3potime:inRDate ?relativePeriodBeg .
            ?taskId c3pocm:hasRelativePeriod/c3potime:hasEnding/c3potime:inRDate ?relativePeriodEnd .
        }
        OPTIONAL {
            ?taskId c3pocm:hasOperationDistance/c3pocm:distanceDuration/time:numericDuration ?distanceValueDuration .
            ?taskId c3pocm:hasOperationDistance/c3pocm:distanceDuration/time:unitType ?distanceUnitDuration .
            ?taskId c3pocm:hasOperationDistance/c3pocm:refersToOperation ?refTaskId .
        }
	}
}`;

export const QUERY_UPDATE_ITK_TITLE = (
  id: string,
  newTitle: string,
  lang: string
) => `
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>

DELETE {
   c3pokb:${id} rdfs:label ?itk.
}
INSERT {
   c3pokb:${id} rdfs:label "${newTitle}"@${lang}.
}
WHERE {
   c3pokb:${id} rdfs:label ?itk .
   FILTER(LANGMATCHES(LANG(?itk), "${lang}"))
}`;

export const DELETE_REGION = (id: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>

DELETE {
    ?context c3pocm:belongsToRegion ?region
}
WHERE {
    c3pokb:${id} c3pocm:dependsOnSoilClimateContext ?context .
    ?context c3pocm:belongsToRegion ?region
}`;

export const INSERT_REGION = (id: string, region: string) => `
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
INSERT {
   ?context c3pocm:belongsToRegion c3povoc:${region} .
}
WHERE {
   c3pokb:${id} c3pocm:dependsOnSoilClimateContext ?context .
}`;

export const QUERY_GET_ALL_PLANT = () => `
    PREFIX c3poplant: <http://www.elzeard.co/ontologies/c3po/plant#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
select * where { 
	?plant a c3poplant:CultivatedPlant .
    ?plant skos:prefLabel ?plantName .
    FILTER(LANGMATCHES(LANG(?plantName), "fr"))
}`;

export const MUTATION_PLANT = (id: string, name: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
delete {
    ?itk c3pocm:concernsPlant ?plant .
}
insert {
    ?itk c3pocm:concernsPlant c3pokb:${name} .
}
where {
    VALUES (?itk) {
		(c3pokb:${id})
	}
    ?itk c3pocm:concernsPlant ?plant .
}
`;

export const QUERY_GET_SEASON = () => `
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
select * where { 
    ?season a skos:Concept .
    ?season skos:broader c3povoc:Seasonality .
    ?season skos:prefLabel ?seasonName .
    FILTER(LANGMATCHES(LANG(?seasonName),  'fr'))
}`;

export const QUERY_GET_CLIMATE = () => `
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
select * where { 
    ?climate a skos:Concept .
    ?climate skos:broader c3povoc:Climate .
    ?climate skos:prefLabel ?climateName .
    FILTER(LANGMATCHES(LANG(?climateName), 'fr'))
}`;

export const MUTATION_CLIMATE = (id: string, name: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
delete {
    ?context c3pocm:hasClimate ?oldClimate .
}
insert {
    ?context c3pocm:hasClimate c3povoc:${name} .
}
where {
    VALUES (?itk) {
		(c3pokb:${id})
	}
    ?itk c3pocm:dependsOnSoilClimateContext/c3pocm:hasClimateContext ?context .
   	?context c3pocm:hasClimate ?oldClimate .
}
`;

export const DELETE_SEASON = (id: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
DELETE {
    c3pokb:${id} c3pocm:hasSeasonality ?season .
}
WHERE {
    c3pokb:${id} c3pocm:hasSeasonality ?season .
}`;

export const INSERT_SEASON = (id: string, season: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
INSERT {
    c3pokb:${id} c3pocm:hasSeasonality c3povoc:${season} .
}
WHERE {}
`;

export const MUTATION_CULTURE_MODE = (id: string, name: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
delete {
    c3pokb:${id} c3pocm:hasCultureMode ?cultureMode .
}
insert {
    c3pokb:${id} c3pocm:hasCultureMode c3povoc:${name} .
}
where {
 	c3pokb:${id} c3pocm:hasCultureMode ?cultureMode .  
}
`;

export const MUTATION_IMPLANTATION_MODE = (id: string, name: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
delete {
    c3pokb:${id} c3pocm:hasImplantationMode ?hasImplantation .
}
insert {
    c3pokb:${id} c3pocm:hasImplantationMode c3povoc:${name} .
}
where {
 	c3pokb:${id} c3pocm:hasImplantationMode ?hasImplantation .  
}
`;

export const QUERY_GET_TYPE_SOIL_EXIST = (id: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
select ?typeSoil where {
    VALUES (?itk)
    {
        (c3pokb:${id})
    }
    OPTIONAL {
    	?itk c3pocm:dependsOnSoilClimateContext/c3pocm:hasSoilContext ?typeSoil .        
    }
}
`;

export const MUTATION_TYPE_SOIL = (id: string, name: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
delete {
    ?context c3pocm:hasSoilNature ?typeSol .
}
insert {
    ?context c3pocm:hasSoilNature c3povoc:${name} .
}
where {
     c3pokb:${id} c3pocm:dependsOnSoilClimateContext/c3pocm:hasSoilContext ?context .
    ?context c3pocm:hasSoilNature ?typeSol .
}
`;

export const MUTATION_IRRIGATION_MODE = (id: string, name: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
delete {
    ?context c3pocm:hasIrrigationMode ?irrigationMode
}
insert {
    ?context c3pocm:hasIrrigationMode c3povoc:${name} .
}
where {
 	c3pokb:${id} c3pocm:hasIrrigationMode ?irrigationMode
}
`;

export const MUTATION_FARM_EQUIPMENT = (id: string, name: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
delete {
    ?context c3pocm:hasFarmEquipmentLevel ?farmEquipmentLevel .
}
insert {
    ?context c3pocm:hasFarmEquipmentLevel c3povoc:${name} .
}
where {
     c3pokb:${id} c3pocm:dependsOnCulturalContext ?context .
    ?context c3pocm:hasFarmEquipmentLevel ?farmEquipmentLevel .
}
`;

export const QUERY_GET_IRRIGATION_MODE_EXIST = (id: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
select ?irrigationMode where {
    VALUES (?itk)
    {
        (c3pokb:${id})
    }
    OPTIONAL {
        	?itk c3pocm:hasIrrigationMode ?irrigationMode .
    }
}
`;

export const QUERY_GET_FARM_EQUIPMENT_EXIST = (id: string) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
select ?farmEquipmentLevel where {
    VALUES (?itk)
    {
        (c3pokb:${id})
    }
    OPTIONAL {
        ?itk c3pocm:dependsOnCulturalContext/c3pocm:hasFarmEquipmentLevel ?farmEquipmentLevel .
    }
}
`;

export const MUTATION_TASKS = (
  id: string,
  title: string,
  duration: number,
  workloadValue: number,
  workloadUnit: string
) => `
PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX time: <http://www.w3.org/2006/time#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX c3poparam: <http://www.elzeard.co/ontologies/c3po/parameter#>
PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
delete {
    c3pokb:${id} rdfs:label ?name .
    ?context time:numericDuration ?distance .
    ?contextWorkload c3poparam:parameterValue ?workloadValue .
    ?contextWorkload c3poparam:parameterUnit ?workloadUnit .
} insert {
    c3pokb:${id} rdfs:label "${title}"@fr .
    ?context time:numericDuration "${duration}"^^xsd:decimal .
    ?contextWorkload c3poparam:parameterValue "${workloadValue}"^^xsd:float .
    ?contextWorkload c3poparam:parameterUnit c3povoc:${workloadUnit} .
} where { 
    c3pokb:${id} rdfs:label ?name .
    c3pokb:${id} c3pocm:hasOperationDistance/c3pocm:distanceDuration ?context .
    c3pokb:${id} c3pocm:implements/c3pocm:hasWorkload ?contextWorkload .
    ?contextWorkload c3poparam:parameterValue ?workloadValue .
    ?contextWorkload c3poparam:parameterUnit ?workloadUnit .
    ?context time:numericDuration ?distance
} 
`;
