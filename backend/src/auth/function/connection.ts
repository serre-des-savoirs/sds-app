import SSOApiClient from "../SSOApiClient";
import SSOUser from "../SSOUser";
import { RepositoryConnection } from "../api/domain";

export class Connection implements RepositoryConnection {
  private ssoApiClient: SSOApiClient;
  constructor() {
    this.ssoApiClient = new SSOApiClient({
      apiTokenEndpointUrl: `${process.env.OAUTH_URL}/realms/master/protocol/openid-connect/token`,
      apiEndpointUrl: `${process.env.OAUTH_URL}/admin/realms/master`,
      apiLogin: `${process.env.OAUTH_ADMIN_USERNAME}`,
      apiPassword: `${process.env.OAUTH_ADMIN_PASSWORD}`,
    });
  }

  public async signIn({
    username,
    password,
  }: {
    username: string;
    password: string;
  }) {
    try {
      if (!username || !password) {
        throw new Error("No username or password provided");
      }
      const auth = await this.ssoApiClient.loginUser({
        tokenEndpointUrl: `${process.env.OAUTH_URL}/realms/${process.env.KEYCLOAK_REALM}/protocol/openid-connect/token`,
        username: username,
        password: password,
        clientId: `${process.env.OAUTH_CLIENT_ID}`,
        clientSecret: `${process.env.OAUTH_CLIENT_SECRET}`,
      });
      return auth.getAuthHeader();
    } catch (error) {
      return "error";
    }
  }

  public async signOut({ token }: { token: string }) {
    try {
      if (!token) {
        throw new Error("No token provided");
      }
      const decodedToken = SSOUser.fromJWT({ jwt: token });
      await this.ssoApiClient.logoutUser(decodedToken.getId());
      return "success";
    } catch (error) {
      return "error";
    }
  }

  public async getUser({ token }: { token: string }) {
    // check if token is valid and logged in
    try {
      if (!token) {
        throw new Error("No token provided");
      }
      const decodedToken = SSOUser.fromJWT({ jwt: token });
      await this.ssoApiClient.getUserById(decodedToken.getId());
      return "success";
    } catch (error) {
      return "error";
    }
  }
}
