import got, { Method } from "got";
import { I18nError } from "./I18nError";
import SSOUser from "./SSOUser";

export default class SSOApiClient {
  private readonly _apiTokenEndpointUrl: string;
  private readonly _apiEndpointUrl: string;
  private readonly _apiClientId: string;
  private readonly _apiLogin: string;
  private readonly _apiPassword: string;

  /**
   * @param {string} apiTokenEndpointUrl
   * @param {string} apiEndpointUrl
   * @param {string} [apiClientId=admin-cli]
   * @param {string} [apiLogin]
   * @param {string} [apiPassword]
   */
  constructor({
    apiTokenEndpointUrl,
    apiEndpointUrl,
    apiClientId,
    apiLogin,
    apiPassword,
  }: {
    apiTokenEndpointUrl: string;
    apiEndpointUrl: string;
    apiClientId?: string | undefined;
    apiLogin: string;
    apiPassword: string;
  }) {
    this._apiTokenEndpointUrl = apiTokenEndpointUrl;
    this._apiEndpointUrl = apiEndpointUrl;
    this._apiClientId = apiClientId || "admin-cli";
    this._apiLogin = apiLogin;
    this._apiPassword = apiPassword;
  }

  /**
   * Login a user
   *
   * @param {string} tokenEndpointUrl
   * @param {string} username
   * @param {string} password
   * @param {string} clientId
   * @param {string} [clientSecret]
   *
   * @return {SSOUser}
   */
  async loginUser({
    tokenEndpointUrl,
    username,
    password,
    clientId,
    clientSecret,
  }: {
    tokenEndpointUrl: string;
    username: string;
    password: string;
    clientId: string;
    clientSecret?: string;
  }) {
    try {
      // @ts-ignore
      let response = await got.post(tokenEndpointUrl, {
        form: {
          username: username.toLowerCase(),
          password,
          client_id: clientId,
          client_secret: clientSecret,
          grant_type: "password",
        },
        responseType: "json",
      });

      // When login succeeds, the returned stringify object in the response body looks like :
      // {
      //   access_token: '...',
      //   expires_in: 900,
      //   refresh_expires_in: 1800,
      //   refresh_token: '...',
      //   token_type: 'bearer',
      //   'not-before-policy': 0,
      //   session_state: '...',
      //   scope: '...'
      // }
      let ticket = response.body;
      return new SSOUser({
        jwtSession: {
          ticket: response.body,
        },
      });
    } catch (e) {
      if ([401, 404].includes(e.response?.statusCode)) {
        throw new I18nError("Invalid credentials", "INVALID_CREDENTIALS", 400);
      } else if (e.response?.statusCode === 400) {
        if (e.response.body.includes("Account disabled")) {
          throw new I18nError(
            "This account has been disabled",
            "ACCOUNT_DISABLED",
            400
          );
        }

        if (e.response.body.includes("Account is not fully set up")) {
          throw new I18nError(
            "Account is not fully set up. This may be a problem of email not verified of password reset request.",
            "ACCOUNT_NOT_VALIDATED",
            400
          );
        }
      }
      throw new I18nError(e.response.body);
    }
  }

  /**
   * @param {string} action
   * @param {object} [body]
   * @param {object} [method=GET]
   * @return {object}
   */
  async requestApi({
    uri,
    body,
    method,
  }: {
    uri: string;
    body?: any;
    method?: Method;
  }) {
    let adminUser = await this.loginUser({
      tokenEndpointUrl: this._apiTokenEndpointUrl,
      username: this._apiLogin,
      password: this._apiPassword,
      clientId: this._apiClientId,
    });

    // console.log(`Admin user success: ${adminUser}`);

    // @ts-ignore
    //   json: true,
    const cancelableRequest = got(uri, {
      method: method || "GET",
      body: JSON.stringify(body),
      headers: {
        Authorization: `Bearer ${adminUser.getAccessToken()}`,
        "Content-Type": "application/json",
      },
      responseType: "json",
    });
    // console.log(cancelableRequest)
    let toto = await cancelableRequest;
    return { body: toto.body };
  }

  /**
   * @param first
   * @param max
   * @return {SSOUser[]}
   */
  async getUsers({ first = 0, max = 100 } = {}) {
    let { body: users } = await this.requestApi({
      uri: `${this._apiEndpointUrl}/users?first=${first}&max=${max}`,
    });

    if (users) {
      // @ts-ignore
      return users.map((user) => new SSOUser({ user }));
    }
  }

  /**
   * @param userId
   * @return {SSOUser}
   */
  async getUserById(userId) {
    let { body: user } = await this.requestApi({
      uri: `${process.env.OAUTH_URL}/auth/admin/realms/sds/users/${userId}`,
    });

    if (user) {
      // @ts-ignore
      return new SSOUser({ user });
    }

    throw new I18nError(
      `User ${userId} is not registered on Keycloak.`,
      "USER_NOT_IN_SSO"
    );
  }

  /**
   * @param username
   * @return {SSOUser}
   * @throws Error if user is not registered for this username.
   */
  async getUserByUsername(username) {
    username = username.toLowerCase();
    let { body: users } = await this.requestApi({
      uri: `${this._apiEndpointUrl}/users?username=${username}`,
    });

    if (users && Array.isArray(users)) {
      let user = users.find((user) => user.username === username);

      if (user) {
        return new SSOUser({ user });
      }
    }

    throw new I18nError(
      `User ${username} is not registered on Keycloak.`,
      "USER_NOT_IN_SSO"
    );
  }

  /**
   * @param username
   * @return {boolean}
   */
  async isUsernameExists(username) {
    username = username.toLowerCase();
    let { body: users } = await this.requestApi({
      uri: `${this._apiEndpointUrl}/users?username=${username}`,
    });

    // @ts-ignore
    return !!(users || []).find((user) => user.username === username);
  }

  async refreshToken({
    tokenEndpoint,
    refreshToken,
    clientId,
    clientSecret,
  }: {
    tokenEndpoint: string;
    refreshToken: string;
    clientId: string;
    clientSecret: string;
  }): Promise<any> {
    let adminUser = await this.loginUser({
      tokenEndpointUrl: this._apiTokenEndpointUrl,
      username: this._apiLogin,
      password: this._apiPassword,
      clientId: this._apiClientId,
    });

    let response = await got.post(tokenEndpoint, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: `Bearer ${adminUser.getAccessToken()}`,
      },
      form: {
        refresh_token: refreshToken,
        client_id: clientId,
        client_secret: clientSecret,
        grant_type: "refresh_token",
      },
      responseType: "json",
    });

    return response.body;
  }

  /**
   * Create a user.
   *
   * @param username
   * @param password
   * @param isTemporaryPassword
   * @param properties - A list of user properties
   * @return {SSOUser}
   */
  async createUser({ username, password, isTemporaryPassword, ...properties }) {
    username = username.toLowerCase();
    try {
      await this.requestApi({
        uri: `${process.env.OAUTH_URL}/auth/admin/realms/sds/users`,
        method: "POST",
        body: {
          username,
          ...properties,
          enabled: true,
        },
      });
    } catch (error) {
      // @ts-ignore
      const b =
        error.response?.body?.errorMessage === "User exists with same username";

      if (error.response?.statusCode === 409 || b) {
        throw new I18nError(
          `User ${username} already exists in SSO database and can't be recreated`,
          "USER_ALREADY_EXISTS",
          400
        );
      } else {
        throw new I18nError(error);
      }
    }
    let user = await this.getUserByUsername(username);
    await this.resetUserPassword({
      userId: user.getId(),
      password,
      isTemporaryPassword,
    });
    console.log(`User ${username} password reset.`);
    return user;
  }

  /**
   * @param userId
   * @param attributes
   */
  async setUserAttributes({ userId, attributes }) {
    await this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}`,
      method: "PUT",
      body: {
        attributes,
      },
    });
  }

  /**
   *
   * @param username
   * @param props
   * @param identityProvider
   * @param userId
   * @param userName
   * @param password
   * @return {boolean}
   */
  async createUserWithFederatedIdentity({
    username,
    props,
    identityProvider,
    userId,
    userName,
    password,
  }) {
    let user = await this.createUser({
      username,
      props,
      password,
      isTemporaryPassword: false,
    });

    await this.requestApi({
      uri: `${
        this._apiEndpointUrl
      }/users/${user.getId()}/federated-identity/${identityProvider}`,
      body: {
        identityProvider,
        userId,
        userName,
      },
    });

    return user;
  }

  /**
   * @param userId
   * @param password
   * @param isTemporaryPassword
   * @return {boolean}
   */
  async resetUserPassword({ userId, password, isTemporaryPassword }) {
    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}/reset-password`,
      method: "PUT",
      body: {
        temporary: !!isTemporaryPassword,
        type: "password",
        value: password,
      },
    });
  }

  /**
   * Reset password by sending an email.
   *
   * @param userId
   * @param redirectUri
   * @param clientId
   * @return {boolean}
   */
  async resetUserPasswordByMail({ userId, redirectUri, clientId }) {
    let extraParams = "";

    if (redirectUri && clientId) {
      extraParams = `?redirect_uri=${redirectUri}&client_id=${clientId}`;
    }

    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}/execute-actions-email${extraParams}`,
      method: "PUT",
      body: ["UPDATE_PASSWORD"],
    });
  }

  /**
   * Send an email verification to validate email address.
   * @param userId
   * @param redirectUri
   * @param clientId
   * @return {boolean}
   */
  async sendEmailValidation({ userId, redirectUri, clientId }) {
    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}/send-verify-email?redirect_uri=${redirectUri}&client_id=${clientId}`,
      method: "PUT",
    });
  }

  /**
   * Remove a user
   *
   * @param userId
   * @return {boolean}
   */
  async removeUser(userId) {
    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}`,
      method: "DELETE",
    });
  }

  /**
   * Disable user
   *
   * @param userId
   * @return {boolean}
   */
  async disableUser(userId) {
    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}`,
      method: "PUT",
      body: {
        enabled: false,
      },
    });
  }

  /**
   * Enable user
   *
   * @param userId
   * @return {boolean}
   */
  async enableUser(userId) {
    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}`,
      method: "PUT",
      body: {
        enabled: true,
      },
    });
  }

  /**
   * Logout user
   *
   * @param userId
   * @return {boolean}
   */
  async logoutUser(userId) {
    return this.requestApi({
      uri: `${process.env.OAUTH_URL}/auth/admin/realms/sds/users/${userId}/logout`,
      method: "POST",
      body: {
        user: userId,
      },
    });
  }
}
