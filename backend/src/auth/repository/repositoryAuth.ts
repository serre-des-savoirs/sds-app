import { GraphDBClient } from "../../graphDBClient";
import { RepositoryAuth } from "../api/domain";
import { Connection } from "../function/connection";

export class RepositoryAuthGraphDB implements RepositoryAuth {
  private readonly dbClient: GraphDBClient;
  private readonly conn: Connection;

  constructor() {
    this.dbClient = new GraphDBClient({
      config: {
        endpoint: `${process.env.GRAPHDB_ENDPOINT}`,
        username: `${process.env.GRAPHDB_USER}`,
        password: `${process.env.GRAPHDB_PASS}`,
        repository: `${process.env.GRAPHDB_REPO}`,
      },
    });
    this.conn = new Connection();
  }

  public async signIn(args: any): Promise<any> {
    return this.conn.signIn({
      username: args.email,
      password: args.password,
    });
  }

  public async signOut(token: string): Promise<any> {
    await this.conn.signOut({
      token: token,
    });
    return "success";
  }

  public async getUser(token: string): Promise<any> {
    return this.conn.getUser({
      token: token,
    });
  }
}
