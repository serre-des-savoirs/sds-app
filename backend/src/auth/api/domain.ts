export interface RepositoryAuth {
  signIn(args: any): Promise<any>;
  signOut(args: any): Promise<any>;
  getUser(token: string): Promise<any>;
}

export interface RepositoryConnection {
  signIn({
    username,
    password,
  }: {
    username: string;
    password: string;
  }): Promise<any>;
  signOut({ token }: { token: string }): Promise<any>;
  getUser({ token }: { token: string }): Promise<any>;
}
