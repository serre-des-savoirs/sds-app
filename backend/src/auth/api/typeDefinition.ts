export const typeDefinitionsAuth = `
type Query {
    signOut: String!
    getUser: String!
}

type Mutation {
    signIn(email: String!, password: String!): String!
}
`;
