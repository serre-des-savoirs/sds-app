import { RepositoryAuthGraphDB } from "../repository/repositoryAuth";
import { RepositoryAuth } from "./domain";

export async function QuerySignIn(repositoryAuth: RepositoryAuth, args: any) {
  return repositoryAuth.signIn(args);
}

export async function QueryGetUser(
  repositoryAuth: RepositoryAuth,
  token: string
) {
  return repositoryAuth.getUser(token);
}

export async function MutationSignOut(
  repositoryAuth: RepositoryAuth,
  token: string
) {
  return repositoryAuth.signOut(token);
}

export const resolverAuth = {
  Query: {
    signOut: async (parent: unknown, args: any, context: any, info: any) =>
      MutationSignOut(new RepositoryAuthGraphDB(), context.token),
    getUser: async (parent: unknown, args: any, context: any, info: any) =>
      QueryGetUser(new RepositoryAuthGraphDB(), context.token),
  },
  Mutation: {
    signIn: async (parent: unknown, args: any, context: any, info: any) =>
      QuerySignIn(new RepositoryAuthGraphDB(), args),
  },
};
