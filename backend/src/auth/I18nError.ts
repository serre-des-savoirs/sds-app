/**
 * This is an error extended with a i18n key
 */
export class I18nError extends Error {
  private i18nKey: string;
  private statusCode: number;
  private captureInSentry: boolean;

  /**
   * @param {string} message
   * @param {string} [i18nKey=SERVER_ERROR]
   * @param {number} [statusCode=500]
   */
  constructor(
    message: any,
    i18nKey = "SERVER_ERROR",
    statusCode = 500,
    captureInSentry = false
  ) {
    super();
    this.message = message;
    this.i18nKey = i18nKey || "SERVER_ERROR";
    this.statusCode = statusCode;
    this.name = this.constructor.name;
    this.captureInSentry = captureInSentry || false;
  }
}
