export function deduplicateObjects(data: any[]) {
  const ids = data.map((el) => el.taskId);
  const uniqueIds = new Set(ids);
  return [...uniqueIds].map((id) => data.find((el) => el.taskId === id));
}

export function eliminateObjects(data: any[], key: string, values: string[]) {
  return data.filter((el) => !values.includes(el[key]));
}

export function sortObjects<T>(
  data: T[],
  sortFunction: (a: T, b: T) => number
) {
  return data.sort(sortFunction);
}
