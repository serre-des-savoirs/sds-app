import { Itk, ItkFilter, Plant, PlantFilter } from "./types";

export function myFilter<T>(
  // TODO : voir Fatou pour un objet T qui contient forcément une propriéte title de type string
  list: (T & { title: string })[],
  search: string,
  filter: Partial<T>
): T[] {
  return (
    list
      // search
      .filter((item) => item.title.toLowerCase().includes(search.toLowerCase()))
      // filter
      .filter((item) =>
        Object.keys(filter).every(
          (key) =>
            !filter[key as keyof T] ||
            item[key as keyof T] === filter[key as keyof T]
        )
      )
  );
}

export function myFilterItk(
  list: Itk[],
  search: string,
  filter: ItkFilter
): Itk[] {
  return (
    list
      // search
      .filter((item) => item.title.toLowerCase().includes(search.toLowerCase()))
      // filters
      .filter((item) => {
        if (!filter.plantId) return true;
        return item.plantId === filter.plantId;
      })
      .filter((item) => {
        if (!filter.technicalCulture) return true;
        return item.technicalCulture === filter.technicalCulture;
      })
      .filter((item) => {
        if (!filter.hasImplantationMode) return true;
        return item.hasImplantationMode === filter.hasImplantationMode;
      })
      .filter((item) => {
        if (
          !filter.regions ||
          !item.dependsOnSoilClimateContext?.belongsToRegion
        )
          return true;
        return item.dependsOnSoilClimateContext?.belongsToRegion
          .map((region) => region.toLowerCase())
          .includes(filter.regions.toLowerCase());
      })
  );
}

export function myFilterPlant(
  list: Plant[],
  search: string,
  filter: PlantFilter
): Plant[] {
  return (
    list
      // search
      .filter((item) => item.title.toLowerCase().includes(search.toLowerCase()))
      // filter
      .filter((item) => {
        if (!filter.botaniqueId) return true;
        return item.botanicalFamilies
          .map(({ id }) => id.toLowerCase().split("#")[1])
          .includes(filter.botaniqueId.toLowerCase());
      })
      .filter((item) => {
        if (!filter.usageId) return true;
        return item.usageFamilies
          .map(({ id }) => id.toLowerCase().split("#")[1])
          .includes(filter.usageId.toLowerCase());
      })
  );
}
