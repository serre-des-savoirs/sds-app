import { hasWorkloadUnitOptions } from "./constants";

export function convertTime(time: string): string {
  if (!time) return "";

  const timeUnits: { [key: string]: string } = {
    unitDay: "jour(s)",
    unitHour: "h",
    unitMinute: "min",
    unitSecond: "s",
    unitWeek: "semaine(s)",
    unitMonth: "mois",
  };

  const newTime = time.split("#")[1];

  return timeUnits[newTime] || "";
}

export function convertWeekTimeToDate(data: string | undefined): Date | null {
  if (!data) return null;

  const yearsMatch = data.match(/(\d+)Y/);
  const weeksMatch = data.match(/(\d+)W/);

  const currentYear = new Date().getFullYear();
  const years = yearsMatch ? Number(yearsMatch[1]) : 0;
  const weeks = weeksMatch ? Number(weeksMatch[1]) : 0;

  const startDate = new Date(currentYear, 0, 1);
  const endDate = new Date(startDate.getTime());
  endDate.setFullYear(startDate.getFullYear() + years);
  endDate.setDate(startDate.getDate() + weeks * 7);

  return endDate;
}

export function convertWeekTimeToFormattedString(
  data: string | undefined
): string {
  if (!data) return "";

  const yearsMatch = data.match(/(\d+)Y/);
  const weeksMatch = data.match(/(\d+)W/);

  const currentYear = new Date().getFullYear();
  const years = yearsMatch ? Number(yearsMatch[1]) : 0;
  const weeks = weeksMatch ? Number(weeksMatch[1]) : 0;

  const startDate = new Date(currentYear, 0, 1);
  const endDate = new Date(startDate.getTime());
  endDate.setFullYear(startDate.getFullYear() + years);
  endDate.setDate(startDate.getDate() + weeks * 7);

  const formattedEndDate = endDate.toLocaleDateString("fr-FR", {
    month: "2-digit",
    day: "2-digit",
  });

  return `${formattedEndDate}`;
}

export function convertMeasure(data: string | undefined): string {
  if (!data) return "";

  const measureUnits: { [key: string]: string } = {
    KilogramPerSquareMeter: "kg/m²",
    Centimeter: "cm",
    BunchPerSquareMeter: "botte(s)/m²",
    PlantPerSquareMeter: "plant(s)/m²",
    SecondPerPlant: "sec/plant",
    HourPerAre: "h/ha",
    SecondPerSquareMeter: "sec/m²",
  };

  const measure = data.split("#")[1];

  return (
    measureUnits[measure] ||
    hasWorkloadUnitOptions.find((option) => option.value === measure)?.label ||
    ""
  );
}

export function calculateYearDifference(date1: string, date2: string): number {
  if (!date1 || !date2) return 0;
  let year1 = "";
  let year2 = "";
  if (date1.indexOf("Y") === -1) {
    year1 = `0Y${date1}`;
  } else {
    year1 = date1;
  }
  if (date2.indexOf("Y") === -1) {
    year2 = `0Y${date2}`;
  } else {
    year2 = date2;
  }
  return Number(year2.split("Y")[0]) - Number(year1.split("Y")[0]);
}
