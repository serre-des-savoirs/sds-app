// Chakra UI Colors
export const uiDesign = {
  export: {
    backgroundColor: "blue",
    textColor: "white",
    borderColor: "blue.300",
    colorScheme: "blue",
  },
  submit: {
    backgroundColor: "blue",
    textColor: "white",
    borderColor: "blue.300",
    colorScheme: "blue",
  },
  buttonAdd: {
    backgroundColor: "#C2E189",
    textColor: "white",
    borderColor: "teal.300",
    colorScheme: "teal",
  },
  buttonUpdate: {
    backgroundColor: "#C2E189",
    textColor: "white",
    borderColor: "green.300",
    colorScheme: "green",
  },
  buttonDeleteCancel: {
    backgroundColor: "red",
    textColor: "white",
    borderColor: "red.300",
    colorScheme: "red",
  },
  card: {
    backgroundColor: "whiteAlpha",
    textColor: "#00313C",
    borderColor: "gray.200",
    colorScheme: "gray",
  },
  basicCard: {
    backgroundColor: "gray.50",
    textColor: "#00313C",
    borderColor: "gray.200",
    colorScheme: "gray",
  },
  boxCardContent: {
    backgroundColor: "gray.100",
    textColor: "#00313C",
    borderColor: "gray.200",
    colorScheme: "gray",
  },
  icon: {
    backgroundColor: "gray.100",
    textColor: "#00313C",
    borderColor: "gray.200",
    colorScheme: "whiteAlpha",
  },
  header: {
    backgroundColor: "whiteAlpha.900",
    textColor: "#00313C",
    borderColor: "gray.200",
    colorScheme: "gray",
  },
  footer: {
    backgroundColor: "gray.100",
    textColor: "#00313C",
    borderColor: "gray.200",
    colorScheme: "gray",
  },
  body: {
    backgroundColor: "gray.50",
    textColor: "#00313C",
    borderColor: "gray.200",
    colorScheme: "gray",
  },
  title: {
    backgroundColor: "gray.50",
    textColor: "#00313C",
    borderColor: "gray.200",
    colorScheme: "gray",
  },
  register: {
    backgroundColor: "#EDE939",
    textColor: "#00313C",
    borderColor: "gray.200",
    colorScheme: "gray",
  },
};
