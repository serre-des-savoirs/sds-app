export function ellipse<T>(max: number, object: T[]): T[] {
  if (object.length <= max) {
    return object;
  }
  return [...object.slice(0, max), `+${object.length - max}` as T];
}
