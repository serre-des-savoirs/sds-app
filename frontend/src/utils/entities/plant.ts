import { Option, Plant, PlantInput } from "../types";

export function getPlantOptions(selectPlant: Plant[]): Option[] {
  return selectPlant.map((plant: Plant) => {
    return { value: plant.id, label: plant.title };
  });
}

export function generatePlant(plant: PlantInput): Plant {
  return {
    id: plant.title.replaceAll(" ", "_").concat("-i"),
    ...plant,
  };
}
