import { v4 as uuidv4 } from "uuid";
import { Task, TaskInput } from "../types";

export function generateTask(task: TaskInput): Task {
  return {
    id: uuidv4(),
    ...task,
  };
}

export function getTaskOptions(selectTask: Task[]) {
  const categoryObject = selectTask.reduce((acc: any, task) => {
    acc[task.type] = true;
    return acc;
  }, {});

  return Object.keys(categoryObject).map((key) => ({
    label: key,
    options: selectTask
      .filter((task) => task.type === key)
      .map((task) => ({ label: task.title, value: task.id }))
      .sort((a, b) => a.label.localeCompare(b.label)),
  }));
}

export function getTaskCategories(tasks: Task[]) {
  // const categoryObject = tasks.reduce(
  // 	(acc: { [key: string]: { [key: string]: boolean } }, task) => {
  // 		if (!acc[task.category]) {
  // 			acc[task.category] = {};
  // 		}
  // 		// Idempotent
  // 		acc[task.category][task.subCategory] = true;
  // 		return acc;
  // 	},
  // 	{},
  // );

  // return Object.entries(categoryObject).map(([key, value]) => ({
  // 	label: key,
  // 	options: Object.keys(value as {}).map((subKey) => ({
  // 		label: subKey,
  // 		value: subKey,
  // 	})),
  // }));
  return ["test", [{ label: "test", value: "test" }]];
}
