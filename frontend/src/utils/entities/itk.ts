import { v4 as uuidv4 } from "uuid";
import { Itk, ItkInput } from "../types";

export function generateItk(itk: ItkInput): Itk {
  return {
    id: uuidv4(),
    ...itk,
  };
}
