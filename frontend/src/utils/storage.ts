// TODO : create hook useLocalStorage

export function setStorage(key: string, value: any) {
  localStorage.setItem(key, JSON.stringify(value));
}

export function getStorage(key: string) {
  return JSON.parse(localStorage.getItem(key) || "[]");
}

export async function updateStorage(key: string, value: any) {
  const storage = await getStorage(key);
  const newStorage = [...storage, value];
  setStorage(key, newStorage);
}

export function removeStorage(key: string) {
  localStorage.removeItem(key);
}

export function clearStorage() {
  localStorage.clear();
}
