import { TDocumentDefinitions } from "pdfmake/interfaces";
import { Itk, Plant } from "../../types";
import { useMyContext } from "../../../context/MyContext";

export function plantPdfExport(currentPlant: Plant): TDocumentDefinitions {
  const { itkList } = useMyContext();
  return {
    content: [
      {
        text: currentPlant.title,
        style: "header",
      },
      {
        text: "Informations:",
        style: "subheader",
      },
      {
        text: `Type Variétale: ${
          currentPlant.typeVarietal === "false" ? "Non" : "Oui"
        }`,
        style: "content",
      },
      {
        text: `Famille: ${currentPlant.typeFamily}`,
        style: "content",
      },
      {
        text: `Wikipedia: ${currentPlant.wiki}`,
        style: "content",
      },
      {
        text: `Culture associée: ${currentPlant.relatedCrop}`,
        style: "content",
      },
      {
        text: `FAO: ${currentPlant.fao}`,
        style: "content",
      },
      {
        text: currentPlant.storages.min
          ? `Stockage: ${currentPlant.storages.min} - ${currentPlant.storages.max} ${currentPlant.storages.unit}`
          : "Stockage: Aucun",
        style: "content",
      },
      {
        text: "Famille associées:",
        style: "subheader",
      },
      {
        text: currentPlant.botanicalFamilies.map((f) => f.name).join(", "),
        style: "content",
      },
      {
        text: "Type associées:",
        style: "subheader",
      },
      {
        text: currentPlant.usageFamilies.map((f) => f.name).join(", "),
        style: "content",
      },
      {
        text: "ITKs associés:",
        style: "subheader",
      },
      {
        table: {
          headerRows: 1,
          widths: ["*", "*"],
          body: [["Titre", "Url"]].concat(
            itkList
              .filter((itk) => itk.plantId === currentPlant.id)
              .map((itk) => [itk.title, `http://localhost:5173/itk/${itk.id}`])
          ),
        },
      },
    ],
    styles: {
      header: {
        fontSize: 22,
        bold: true,
        //[left, top, right, bottom]
        margin: [0, 0, 10, 0],
      },
      subheader: {
        fontSize: 14,
        bold: true,
        margin: [0, 10, 5, 0],
      },
      content: {
        fontSize: 12,
        margin: [10, 10, 5, 0],
      },
    },
  };
}
