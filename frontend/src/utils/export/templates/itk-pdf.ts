import { TDocumentDefinitions } from "pdfmake/interfaces";
import { useMyContext } from "../../../context/MyContext";
import { deduplicateObjects, sortObjects } from "../../cleanGraphData";
import {
  cultureModeOptions,
  farmEquipmentOptions,
  irrigationModeOptions,
  regionOptions,
} from "../../constants";
import {
  convertMeasure,
  convertTime,
  convertWeekTimeToFormattedString,
} from "../../convert";
import { Itk, TaskReference } from "../../types";

export function itkPdfExport(currentItk: Itk): TDocumentDefinitions {
  const { plantList } = useMyContext();
  return {
    content: [
      {
        text: currentItk.title,
        style: "header",
      },
      {
        text: "Générale:",
        style: "subheader",
      },
      {
        text: `Culture technique: ${
          cultureModeOptions.find(
            (el) => el.value === currentItk.technicalCulture
          )?.label
        }`,
        style: "content",
      },
      {
        text: `Rendement estimé: ${
          currentItk.hasYield?.averageYield
        } ${convertMeasure(currentItk.hasYield?.hasParameterUnit)}`,
        style: "content",
      },
      {
        text: `Période: ${convertWeekTimeToFormattedString(
          currentItk.hasRelativePeriod?.hasBeginning
        )} - ${convertWeekTimeToFormattedString(
          currentItk.hasRelativePeriod?.hasEnding
        )}`,
        style: "content",
      },
      {
        text: "Compatibilité:",
        style: "subheader",
      },
      {
        text: `Plante associée: ${
          plantList.find((plant) => plant.id === currentItk.plantId)?.title
        }`,
        style: "content",
      },
      {
        text: "Implantation:",
        style: "subheader",
      },
      {
        text: `mode d'implantation: ${currentItk.hasImplantationMode}`,
        style: "content",
      },
      {
        text: `mode de culture: ${currentItk.technicalCulture}`,
        style: "content",
      },
      {
        text: "Contexte culturale",
        style: "subheader",
      },
      {
        text: `Type de sol: ${currentItk.dependsOnCulturalContext?.hasCultivation}`,
        style: "content",
      },
      {
        text: `Type de climat: ${currentItk.dependsOnSoilClimateContext?.hasClimateContext}`,
        style: "content",
      },
      {
        text: `Région: ${
          [
            ...regionOptions
              .filter((el) =>
                currentItk.dependsOnSoilClimateContext?.belongsToRegion?.includes(
                  el.value
                )
              )
              .map((el) => el.label),
          ].join(", ") || "non spécifié"
        }`,
        style: "content",
      },
      {
        text: `Saison: ${currentItk.season?.join(", ")}`,
        style: "content",
      },
      {
        text: `Irrigation: ${
          irrigationModeOptions.find(
            (el) => el.value === currentItk.hasIrrigationMode
          )?.label || "non spécifié"
        }`,
        style: "content",
      },
      {
        text: `Ferme equipement: ${
          farmEquipmentOptions.find(
            (el) => el.value === currentItk.hasFarmEquipment
          )?.label || "non spécifié"
        }`,
        style: "content",
      },
      {
        text: "Tâches:",
        style: "subheader",
      },
      {
        table: {
          widths: ["*", "*", "*"],
          body: [
            [
              {
                text: "Tâche",
                style: "tableHeader",
              },
              {
                text: "Durée",
                style: "tableHeader",
              },
              {
                text: "Distance",
                style: "tableHeader",
              },
            ],
            ...sortObjects(
              deduplicateObjects(currentItk.tasks),
              (a, b) =>
                a.hasOperationDistance.distanceDuration.numericDuration -
                b.hasOperationDistance.distanceDuration.numericDuration
            ).map((task: TaskReference) => {
              return [
                {
                  text: task.taskName,
                  style: "tableContent",
                },
                {
                  text: `${
                    task.hasOperationDistance.distanceDuration
                      .numericDuration || 0
                  } ${convertTime(
                    task.hasOperationDistance.distanceDuration.unitType
                  )}`,
                  style: "tableContent",
                },
                {
                  text: `${
                    task.implements.hasWorkload.parameterValue || 0
                  } ${convertMeasure(
                    task.implements.hasWorkload.hasWorkloadUnit
                  )}`,
                  style: "tableContent",
                },
              ];
            }),
          ],
        },
      },
    ],
    styles: {
      header: {
        fontSize: 22,
        bold: true,
        //[left, top, right, bottom]
        margin: [0, 0, 10, 0],
      },
      subheader: {
        fontSize: 14,
        bold: true,
        margin: [0, 10, 5, 0],
      },
      content: {
        fontSize: 12,
        margin: [10, 10, 5, 0],
      },
    },
  };
}
