import { TDocumentDefinitions } from "pdfmake/interfaces";
import { Task } from "../../types";

export function taskPdfExport(currentTask: Task): TDocumentDefinitions {
  return {
    content: [
      {
        text: currentTask.title,
        style: "header",
      },

      {
        text: `Type: ${currentTask.type || "Aucun type"}`,
        style: "subheader",
      },
    ],
    styles: {
      header: {
        fontSize: 22,
        bold: true,
      },
      subheader: {
        fontSize: 16,
        bold: true,
      },
    },
  };
}
