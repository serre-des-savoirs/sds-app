import { itkPdfExport } from "./templates/itk-pdf";
import { plantPdfExport } from "./templates/plant-pdf";
import { taskPdfExport } from "./templates/task-pdf";

export function exportPdf(content: any, type: "itk" | "plant" | "task"): any {
  switch (type) {
    case "itk":
      return itkPdfExport(content);
    case "plant":
      return plantPdfExport(content);
    case "task":
      return taskPdfExport(content);
    default:
      break;
  }
}
