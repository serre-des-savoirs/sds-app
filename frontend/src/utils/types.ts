// ITK

export type Itk = {
  id: string;
  title: string;
  technicalCulture: string;
  plantId: string;
  source: string;
  // Tasks as references or tasks as values
  tasks: TaskReference[];
  season: string[];
  hasRelativePeriod?: HasRelativePeriod;
  dependsOnCulturalContext?: CulturalContext;
  dependsOnSoilClimateContext?: SoilClimateContext;
  hasImplantationMode?: string;
  hasYield?: Yield;
  lossMargin?: string;
  hasGlobalWorload?: HasGlobalWorload;
  typeSoil?: string;
  hasIrrigationMode?: string;
  hasFarmEquipment?: string;
};

type HasGlobalWorload = {
  parameterValue: number;
  hasParameterUnit: string;
};

type Yield = {
  maxYield: string;
  minYield: string;
  averageYield: string;
  parameterValue: string;
  hasParameterUnit: string;
};

type HasRelativePeriod = {
  hasBeginning: string;
  hasEnding: string;
};

type SoilClimateContext = {
  belongsToRegion?: string[];
  hasClimateContext?: string;
};

type CulturalContext = {
  cultivatesIn: string;
  hasCultivation: string;
};

export type task = {
  hasBedWidth: {
    parameterValue: number;
    hasParameterUnit: string;
  };
  hasFootPassWidth: {
    parameterValue: number;
    hasParameterUnit: string;
  };
  hasPlantingDensity: {
    parameterValue: number;
    hasParameterUnit: string;
  };
  hasSpacingByLine: {
    parameterValue: number;
    hasParameterUnit: string;
  };
  hasSpacingInsideLine: {
    parameterValue: number;
    hasParameterUnit: string;
  };
  hasWalkingSpaceWidth: {
    parameterValue: number;
    hasParameterUnit: string;
  };
  hasWorkload: {
    parameterValue: number;
    hasWorkloadUnit: string;
  };
};

type TaskOperationDistance = {
  distanceDuration: {
    numericDuration: number;
    unitType: string;
  };
  refersToOperation: {
    id: string;
    type: string;
  };
};

export type TaskReference = {
  taskId: string;
  taskName: string;
  taskType: string;
  implements: task;
  hasOperationDistance: TaskOperationDistance;
  hasRelativePeriod?: HasRelativePeriod;
};

export type ItkInput = Omit<Itk, "id">;

export type ItkFilter = {
  technicalCulture: string;
  plantId: string;
  hasImplantationMode: string;
  regions: string;
};

// Plant

export type Plant = {
  id: string;
  broaderKey?: string;
  title: string;
  typeVarietal: string;
  typeFamily: string;
  wiki: string;
  botanicalFamilies: {
    id: string;
    name: string;
  }[];
  usageFamilies: {
    id: string;
    name: string;
  }[];
  relatedCrop: string;
  fao: string;
  taxRef: string;
  storages: PlantStorage;
};

export type PlantStorage = {
  min: string;
  max: string;
  unit: string;
};

export type PlantInput = Omit<Plant, "id">;

export type PlantFilter = {
  title: string;
  botaniqueId: string;
  usageId: string;
};

// Task

export type Task = {
  id: string;
  title: string;
  type: string;
};

export type TaskInput = Omit<Task, "id">;

export type TaskFilter = {};

// Option

export type Option = {
  value: string;
  label?: string;
};

// Testing

export interface FormValues {
  number: number;
  firstName: string;
  email: string;
  password: string;
  select: string;
  date: string;
  pin: number[];
  radio: string;
  switch: boolean;
  textArea: string;
  checkBox: string[];
}

export interface taskDetailsProps {
  title: string;
  modalTitle: string;
  filter: (tasks: TaskReference[]) => TaskReference[];
  defaultValue?: number;
}

export interface updateComponentProps {
  title: string;
  modify:
    | "region"
    | "plantId"
    | "climate"
    | "season"
    | "technicalCulture"
    | "implantationMode"
    | "typeSoil"
    | "irrigationMode"
    | "farmEquipment";
  content: string | string[];
  options: Option[];
  isMultiple: boolean;
}
