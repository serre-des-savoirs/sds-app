import {
  Itk,
  Option,
  TaskReference,
  taskDetailsProps,
  updateComponentProps,
} from "./types";

export const cultureModeOptions: Option[] = [
  {
    value: "OpenField",
    label: "Plein champ",
  },
  {
    value: "UnderCover",
    label: "Sous abri",
  },
  // {
  //   value: "HeatedGreenHouse",
  //   label: "serre chauffé",
  // },
];

export const botaniqueFamilyOptions: Option[] = [
  {
    value: "Asteraceae-i",
    label: "Asteracées",
  },
  {
    value: "Asparagaceae-i",
    label: "Asparagacées",
  },
  {
    value: "Fabaceae-i",
    label: "Fabacées",
  },
  {
    value: "Amaranthaceae-i",
    label: "Amaranthacées",
  },
  {
    value: "Brassicaceae-i",
    label: "Brassicacées",
  },
  {
    value: "Cucurbitaceae-i",
    label: "Cucurbitacées",
  },
  {
    value: "Amaryllidaceae-i",
    label: "Amaryllidacées",
  },
  {
    value: "Apiaceae-i",
    label: "Apiacées",
  },
  {
    value: "Solanaceae-i",
    label: "Solanacées",
  },
  {
    value: "Poaceae-i",
    label: "Poacées",
  },
  {
    value: "Caprifoliaceae-i",
    label: "Caprifoliacées",
  },
  {
    value: "Portulacaceae-i",
    label: "Portulacacées",
  },
  {
    value: "Lamiaceae-i",
    label: "Lamiacées",
  },
  {
    value: "Polygonaceae-i",
    label: "Polygonacées",
  },
  {
    value: "Aizoaceae-i",
    label: "Aizoacées",
  },
  {
    value: "Rosaceae-i",
    label: "Rosacées",
  },
  {
    value: "Convolvulaceae-i",
    label: "Convolvulacées",
  },
  {
    value: "Dioscoreaceae-i",
    label: "Dioscoréacées",
  },
  {
    value: "Laureaceae-i",
    label: "Lauracées",
  },
  {
    value: "Boraginaceae-i",
    label: "Boraginacées",
  },
  {
    value: "Cactaceae-i",
    label: "Cactacées",
  },
  {
    value: "Asphodelaceae-i",
    label: "Asphodélacées",
  },
  {
    value: "Cannabeaceae-i",
    label: "Cannabacées",
  },
  {
    value: "Caryophyllaceae-i",
    label: "Caryophyllacées",
  },
  {
    value: "Cistaceae-i",
    label: "Cistacées",
  },
  {
    value: "Hydrophyllaceae-i",
    label: "Hydrophyllacées",
  },
  {
    value: "Iridaceae-i",
    label: "Iridacées",
  },
  {
    value: "Liliaceae-i",
    label: "Liliacées",
  },
  {
    value: "Malvaceae-i",
    label: "Malvacées",
  },
  {
    value: "Plumbaginaceae-i",
    label: "Plumbaginacées",
  },
  {
    value: "Ranunculaceae-i",
    label: "Ranunculacées",
  },
  {
    value: "Scrophulariaceae-i",
    label: "Scrophulariacées",
  },
  {
    value: "Tropaeolaceae-i",
    label: "Tropaeolacées",
  },
];

export const usageFamilyOptions: Option[] = [
  {
    value: "BulbVegetables-i",
    label: "Légumes Bulbe",
  },
  {
    value: "LeafVegetables-i",
    label: "Légumes Feuille",
  },
  {
    value: "FruitVegetables-i",
    label: "Légumes Fruit",
  },
  {
    value: "RootVegetables-i",
    label: "Légumes Racine",
  },
  {
    value: "TuberousVegetables-i",
    label: "Légumes Tubercule",
  },
  {
    value: "FlowerVegetables-i",
    label: "Légumes Fleur",
  },
  {
    value: "BeanVegetables-i",
    label: "Légumes Graine",
  },
  {
    value: "MedicinalPlants-i",
    label: "Plantes médicinales",
  },
  {
    value: "AromaticPlants-i",
    label: "Plantes aromatiques",
  },
  {
    value: "SmallFruits-i",
    label: "Petits fruits",
  },
  {
    value: "ServicePlants-i",
    label: "Plantes de service",
  },
  {
    value: "Cereals-i",
    label: "Céréales",
  },
  {
    value: "Vegetables-i",
    label: "Légumes",
  },
  {
    value: "Fruits-i",
    label: "Fruits",
  },
  {
    value: "Flowers-i",
    label: "Fleurs",
  },
];

export const implantationModeOptions: Option[] = [
  {
    value: "PermanentBed",
    label: "Planche permanente",
  },
  {
    value: "FreeArea",
    label: "Surface libre",
  },
  // TODO : not implemented yet
  // {
  // 	value: "FreeAreaWithSimpleInterrow",
  // 	label: "Surface libre (inter-rang simple)",
  // },
  // {
  // 	value: "FreeAreaWithTemporaryBed",
  // 	label: "Surface libre (planches temporaires)",
  // },
];

export const regionOptions: Option[] = [
  {
    value: "AuvergneRhoneAlpes",
    label: "Auvergne-Rhône-Alpes",
  },
  {
    value: "BourgogneFrancheCompté",
    label: "Bourgogne-Franche-Comté",
  },
  {
    value: "Bretagne",
    label: "Bretagne",
  },
  {
    value: "CentreValDeLoire",
    label: "Centre-Val de Loire",
  },
  {
    value: "Corse",
    label: "Corse",
  },
  {
    value: "GrandEst",
    label: "Grand Est",
  },
  {
    value: "HautsDeFrance",
    label: "Hauts-de-France",
  },
  {
    value: "IleDeFrance",
    label: "Île-de-France",
  },
  {
    value: "Normandie",
    label: "Normandie",
  },
  {
    value: "NouvelleAquitaine",
    label: "Nouvelle-Aquitaine",
  },
  {
    value: "Occitanie",
    label: "Occitanie",
  },
  {
    value: "PaysDeLaLoire",
    label: "Pays de la Loire",
  },
  {
    value: "ProvenceAlpesCoteDAzur",
    label: "Provence-Alpes-Côte d'Azur",
  },
  {
    value: "FranceNorthOuest",
    label: "Nord-Ouest France",
  },
];

export const seasonOptions: Option[] = [
  {
    label: "Hiver",
    value: "Winter",
  },
  {
    label: "Printemps",
    value: "Spring",
  },
  {
    label: "Été",
    value: "Summer",
  },
  {
    label: "Automne",
    value: "Autumn",
  },
];

export const typeSoilOptions: Option[] = [
  {
    label: "Riche en humus",
    value: "HumusRich",
  },
  {
    label: "Sol calcaire",
    value: "LimestoneSoil",
  },
  {
    label: "Meuble",
    value: "Loose",
  },
  {
    label: "Bien drainé",
    value: "WellDrained",
  },
  {
    label: "Riche",
    value: "Rich",
  },
];

export const irrigationModeOptions: Option[] = [
  {
    label: "Goutte à goutte",
    value: "Drip",
  },
  {
    label: "Aspertion",
    value: "Sprinkler",
  },
  {
    label: "Gravité",
    value: "Gravity",
  },
];

export const farmEquipmentOptions: Option[] = [
  {
    label: "Équipement à traction animale",
    value: "AnimalPoweredEquipment",
  },
  {
    label: "Équipement robotique",
    value: "RobotPoweredEquipment",
  },
  {
    label: "Équipement manuel",
    value: "HandPoweredEquipment",
  },
  {
    label: "Équipement lourd motorisé",
    value: "HeavyPoweredEquipment",
  },
  {
    label: "Équipement léger motorisé",
    value: "LightPoweredEquipment",
  },
];

export const unitTimeOption: Option[] = [
  {
    label: "secondes",
    value: "unitSecond",
  },
  {
    label: "minutes",
    value: "unitMinute",
  },
  {
    label: "heures",
    value: "unitHour",
  },
  {
    label: "jours",
    value: "unitDay",
  },
  {
    label: "semaines",
    value: "unitWeek",
  },
  {
    label: "mois",
    value: "unitMonth",
  },
  {
    label: "années",
    value: "unitYear",
  },
];

export const climateOptions: Option[] = [
  {
    label: "Continental",
    value: "Continental",
  },
  {
    label: "Méditerranéen",
    value: "Mediterranean",
  },
  {
    label: "Océanique",
    value: "Oceanic",
  },
  {
    label: "Montagneux",
    value: "Mountain",
  },
  {
    label: "Semi-océanique",
    value: "SemiOceanic",
  },
];

export const hasWorkloadUnitOptions: Option[] = [
  {
    label: "Heure par are",
    value: "HourPerAre",
  },
  {
    label: "Heure par hectare",
    value: "HourPerHectare",
  },
  {
    label: "Seconde par godet",
    value: "SecondPerCup",
  },
  {
    label: "Seconde par unité vendu",
    value: "SecondPerSoldUnit",
  },
  {
    label: "Seconde par mètre linéaire",
    value: "SecondPerLinearMeter",
  },
  {
    label: "Seconde par motte",
    value: "SecondPerBall",
  },
  {
    label: "Seconde par caissette",
    value: "SecondPerTray",
  },
  {
    label: "Seconde par mètre carré",
    value: "SecondPerSquareMeter",
  },
  {
    label: "Seconde par alvéole",
    value: "SecondPerAlveolus",
  },
  {
    label: "Minute par mètre carré",
    value: "MinutePerSquareMeter",
  },
  {
    label: "Minute par hectare",
    value: "MinutePerHectare",
  },
  {
    label: "Minute par are",
    value: "MinutePerAre",
  },
];

export const EMPTY_TASKREF = {
  taskId: "",
  taskName: "",
  taskType: "",
  hasOperationDistance: {
    distanceDuration: {
      numericDuration: 0,
      unitType: "unitWeek",
    },
    refersToOperation: {
      id: "",
      type: "",
    },
  },
  implements: {
    hasBedWidth: {
      parameterValue: 0,
      hasParameterUnit: "",
    },
    hasFootPassWidth: {
      parameterValue: 0,
      hasParameterUnit: "",
    },
    hasPlantingDensity: {
      parameterValue: 0,
      hasParameterUnit: "",
    },
    hasSpacingByLine: {
      parameterValue: 0,
      hasParameterUnit: "",
    },
    hasSpacingInsideLine: {
      parameterValue: 0,
      hasParameterUnit: "",
    },
    hasWalkingSpaceWidth: {
      parameterValue: 0,
      hasParameterUnit: "",
    },
    hasWorkload: {
      parameterValue: 0,
      hasWorkloadUnit: "",
    },
  },
};

export const getRegionOptions = (itkList: Itk[]) => {
  const regions = itkList.reduce((acc, el) => {
    if (el.dependsOnSoilClimateContext?.belongsToRegion) {
      el.dependsOnSoilClimateContext.belongsToRegion.forEach((region) => {
        if (!acc.includes(region)) {
          acc.push(region);
        }
      });
    }
    return acc;
  }, [] as string[]);

  const regionsOptions: Option[] = regions.map((el) => ({
    label: regionOptions.find((region) => region.value === el)?.label,
    value: el,
  }));

  return [{ value: "", label: "Tous" }, ...regionsOptions];
};

export const getCulturalContext = (obj: Itk): updateComponentProps[] => [
  {
    title: "Région:",
    modify: "region",
    content:
      (obj?.dependsOnSoilClimateContext?.belongsToRegion || []).length > 0
        ? (regionOptions
            .filter((el) =>
              obj.dependsOnSoilClimateContext?.belongsToRegion?.includes(
                el.value
              )
            )
            .map((el) => el.label) as string[])
        : [EMPTY_RESULT],
    options: regionOptions,
    isMultiple: true,
  },
  {
    title: "Saison:",
    modify: "season",
    content: obj.season.length > 0 ? obj.season : [EMPTY_RESULT],
    options: seasonOptions,
    isMultiple: true,
  },
  {
    title: "Type de sol:",
    modify: "typeSoil",
    content: obj.typeSoil ? [obj.typeSoil] : [EMPTY_RESULT],
    options: typeSoilOptions,
    isMultiple: false,
  },
  {
    title: "Type de climat:",
    modify: "climate",
    content: obj.dependsOnSoilClimateContext?.hasClimateContext
      ? (climateOptions
          .filter((el) =>
            obj.dependsOnSoilClimateContext?.hasClimateContext?.includes(
              el.value
            )
          )
          .map((el) => el.label) as string[])
      : [EMPTY_RESULT],
    options: climateOptions,
    isMultiple: false,
  },
  {
    title: "Équipement de la ferme:",
    modify: "farmEquipment",
    content: obj.hasFarmEquipment ? [obj.hasFarmEquipment] : [EMPTY_RESULT],
    options: farmEquipmentOptions,
    isMultiple: false,
  },
];

export const getTaskInfo = (
  obj: TaskReference,
  objRef: TaskReference
): {
  title: string;
  id: string;
  value: number;
  unit: string;
  refValue: number;
  refUnit: string;
}[] => [
  {
    id: "hasBedWidth",
    title: "Largeur de planche",
    value: obj.implements?.hasBedWidth.parameterValue || 0,
    unit: obj.implements?.hasBedWidth.hasParameterUnit,
    refValue: objRef.implements["hasBedWidth"].parameterValue,
    refUnit: objRef.implements["hasBedWidth"].hasParameterUnit,
  },
  {
    id: "hasFootPassWidth",
    title: "Largeur de passage",
    value:
      obj.implements?.hasFootPassWidth.parameterValue ||
      obj.implements?.hasWalkingSpaceWidth.parameterValue ||
      0,
    unit:
      obj.implements?.hasFootPassWidth.hasParameterUnit ||
      obj.implements?.hasWalkingSpaceWidth.hasParameterUnit,
    refValue:
      objRef.implements["hasFootPassWidth"]?.parameterValue ||
      objRef.implements["hasWalkingSpaceWidth"]?.parameterValue,
    refUnit:
      objRef.implements["hasFootPassWidth"].hasParameterUnit ||
      objRef.implements["hasWalkingSpaceWidth"].hasParameterUnit,
  },
  {
    id: "hasPlantingDensity",
    title: "Densité de plantation",
    value: obj.implements?.hasPlantingDensity.parameterValue || 0,
    unit: obj.implements?.hasPlantingDensity.hasParameterUnit,
    refValue: objRef.implements["hasPlantingDensity"].parameterValue,
    refUnit: objRef.implements["hasPlantingDensity"].hasParameterUnit,
  },
  {
    id: "hasSpacingByLine",
    title: "Espacement entre les lignes",
    value: obj.implements?.hasSpacingByLine.parameterValue || 0,
    unit: obj.implements?.hasSpacingByLine.hasParameterUnit,
    refValue: objRef.implements["hasSpacingByLine"].parameterValue,
    refUnit: objRef.implements["hasSpacingByLine"].hasParameterUnit,
  },
  {
    id: "hasSpacingInsideLine",
    title: "Espacement dans la ligne",
    value: obj.implements?.hasSpacingInsideLine.parameterValue || 0,
    unit: obj.implements?.hasSpacingInsideLine.hasParameterUnit,
    refValue: objRef.implements["hasSpacingInsideLine"].parameterValue,
    refUnit: objRef.implements["hasSpacingInsideLine"].hasParameterUnit,
  },
  {
    id: "hasWorkload",
    title: "Charge de travail",
    value: obj.implements?.hasWorkload.parameterValue || 0,
    unit: obj.implements?.hasWorkload.hasWorkloadUnit,
    refValue: objRef.implements["hasWorkload"].parameterValue,
    refUnit: objRef.implements["hasWorkload"].hasWorkloadUnit,
  },
];

export const getImplantationDetails = (obj: Itk): updateComponentProps[] => [
  // {
  //   title: "Mode d'implantation",
  //   modify: "implantationMode",
  //   content: obj.hasImplantationMode ? obj.hasImplantationMode : EMPTY_RESULT,
  //   options: implantationModeOptions,
  //   isMultiple: false,
  // },
  {
    title: "Mode de culture",
    modify: "technicalCulture",
    content: obj.technicalCulture
      ? (cultureModeOptions.filter(
          (el) => el.value === obj?.technicalCulture
        )[0]?.label as string)
      : EMPTY_RESULT,
    options: cultureModeOptions,
    isMultiple: false,
  },
];

export const getTaskDetails = (obj: Itk): taskDetailsProps[] => [
  {
    title: "Implantation",
    modalTitle: "Ajoût d'une tâche d'implantation",
    filter: (tasks: TaskReference[]) =>
      tasks.filter(
        (task) =>
          task.taskType.indexOf("Planting") > -1 ||
          task.taskType.indexOf("Plantation") > -1 ||
          task.taskType.indexOf("SeedingInRows") > -1 ||
          (task.hasOperationDistance?.refersToOperation &&
            task.hasOperationDistance.refersToOperation.type.indexOf(
              "Planting"
            ) > -1 &&
            task.hasOperationDistance?.distanceDuration.numericDuration < 0)
      ),
  },
  {
    title: "Conduite",
    modalTitle: "Ajoût d'une tâche de conduite",
    defaultValue: 1,
    filter: (tasks: TaskReference[]) =>
      tasks.filter(
        (task) =>
          task.taskType.indexOf("Harvest") === -1 &&
          task.hasOperationDistance?.refersToOperation &&
          ((task.hasOperationDistance.refersToOperation.type.indexOf(
            "Planting"
          ) > -1 &&
            task.hasOperationDistance?.distanceDuration.numericDuration >= 0) ||
            (task.hasOperationDistance.refersToOperation.type.indexOf(
              "Harvest"
            ) > -1 &&
              task.hasOperationDistance?.distanceDuration.numericDuration < 0))
      ),
  },
  {
    title: "Récolte",
    modalTitle: "Ajoût d'une tâche de récolte",
    filter: (tasks: TaskReference[]) =>
      tasks.filter(
        (task) =>
          task.taskType.indexOf("Harvest") > -1 ||
          task.taskType.indexOf("Storage") > -1 ||
          (task.hasOperationDistance?.refersToOperation &&
            task.hasOperationDistance.refersToOperation.type.indexOf(
              "Harvest"
            ) > -1 &&
            task.hasOperationDistance?.distanceDuration.numericDuration >= 0)
      ),
  },
];

export const EMPTY_RESULT = "Aucune valeur enregistrée";
