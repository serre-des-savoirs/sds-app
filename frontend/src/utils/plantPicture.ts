import defaultPlant from "../assets/default-plant.jpg";

const PLANT_PICTURES_ENDPOINT =
  "https://storage.sbg.cloud.ovh.net/v1/AUTH_083b74266e1047089266916cf47576fe/elzeard-plants";

export function plantPictureUrl(plantId: string) {
  return `${PLANT_PICTURES_ENDPOINT}/medium/c3pokb${plantId.replace(
    ":",
    ""
  )}.jpg`;
}

export const plantPictureFallback = defaultPlant;
