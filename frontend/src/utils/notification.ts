import { UseToastOptions } from "@chakra-ui/react";

export const TOAST_SUCCESS: UseToastOptions = {
  title: "Formulaire envoyé",
  description: "Votre formulaire a bien été envoyé",
  status: "success",
  duration: 900,
  isClosable: true,
};

export const TOAST_SUCCESS_LOGIN = (name: string): UseToastOptions => ({
  title: `Bienvenue ${name}`,
  description: "Vous êtes maintenant connecté",
  status: "success",
  duration: 2000,
  isClosable: true,
});

export const TOAST_SUCCESS_REGISTER = (name: string): UseToastOptions => ({
  title: `Bienvenue ${name}`,
  description: "Vous êtes maintenant inscrit",
  status: "success",
  duration: 2000,
  isClosable: true,
});

export const TOAST_ERROR_LOGIN: UseToastOptions = {
  title: "Erreur de connexion",
  description: "Identifiants ou mot de passe incorrect",
  status: "error",
  duration: 5000,
  isClosable: true,
};
