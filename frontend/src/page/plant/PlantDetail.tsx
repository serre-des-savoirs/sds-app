import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Box, Button, Flex, Heading, SimpleGrid, Text } from "@chakra-ui/react";
import { useEffect, useMemo, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { CreatePdf } from "../../component/CreatePdf";
import { Loading } from "../../component/Loading";
import { ITKCard, MyCard } from "../../component/card";
import { MyContainerDetail } from "../../component/display/MyContainerDetail";
import { MyFooter } from "../../component/layout/MyFooter";
import { useMyContext } from "../../context/MyContext";
import { usePlant } from "../../context/usePlant";
import { requestQueryPlant } from "../../graphql/itk";
import { Itk, Plant } from "../../utils";
import { exportPdf } from "../../utils/export/pdf";
import { useFetch } from "../../utils/hooks/useFetch";
import { uiDesign } from "../../utils/styles";
import NotFound from "../errors/NotFound";
import { PlantImage } from "../../component/card/PlantImage";

export function PlantDetail() {
  const { id } = useParams<{ id: string }>();
  const { itkList } = useMyContext();
  const { removePlant } = usePlant();
  const [isPermission, setIsPermission] = useState<boolean>(false);
  const navigate = useNavigate();
  const {
    values,
    loading,
    error,
  }: { values: any; loading: boolean; error: any } = useFetch(
    import.meta.env.VITE_URL_QUERY,
    {
      query: requestQueryPlant(id),
    }
  );

  const {
    values: user,
    loading: loadingUser,
    error: errorUser,
  }: {
    values: any;
    loading: boolean;
    error: any;
  } = useFetch(
    import.meta.env.VITE_URL_QUERY,
    {
      query: `
        query {
          getUser
          }`,
    },
    "POST",
    {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    }
  );

  useEffect(() => {
    if (loadingUser || !user?.data) return;
    if (errorUser) console.log(errorUser);
    if (user.data.getUser === "success") setIsPermission(true);
  }, [user]);

  const itksForPlant = useMemo(() => {
    if (!itkList || !values?.data) {
      return []
    }
    return itkList.filter((itk) => 
      itk.plantId === values.data.getPlant.id ||
      itk.plantId === values.data.getPlant.broaderKey
    );
  }, [values?.data, itkList])

  if (loading) return <Loading />;

  if (error) console.log(error);

  if (id === undefined || !values?.data) return <NotFound />;

  const currentPlant: Plant = {
    id: values.data.getPlant.id,
    broaderKey: values.data.getPlant.broaderKey,
    title: values.data.getPlant.name,
    typeVarietal: values.data.getPlant.typeVarietal,
    typeFamily: values.data.getPlant.typeFamily,
    wiki: values.data.getPlant.wiki,
    botanicalFamilies: values.data.getPlant.botanicalFamilies,
    usageFamilies: values.data.getPlant.usageFamilies,
    relatedCrop: values.data.getPlant?.relatedCrop,
    fao: values.data.getPlant?.fao,
    taxRef: values.data.getPlant?.taxRef,
    storages: {
      min: values.data.getPlant?.storage.minStorage,
      max: values.data.getPlant?.storage.maxStorage,
      unit: values.data.getPlant?.storage.unitTimeStorage,
    },
  };

  if (currentPlant.id !== id) {
    window.location.reload();
    return <Box />;
  }

  return (
    <>
      <MyContainerDetail>
        <Flex alignItems="center" justifyContent="flex-start" mb={10} gap={8}>
          <Heading size="xl" alignItems="inherit" display="center" gap={4}>
            <PlantImage plantId={currentPlant.id}/>
            {currentPlant.title}
          </Heading>
          <Flex gap={4}>
            {currentPlant.wiki ? (
              <ExternalLink href={currentPlant.wiki} label="Wiki" />
            ) : null}
            {currentPlant.fao ? (
              <ExternalLink href={currentPlant.fao} label="FAO" />
            ) : null}
            {currentPlant.relatedCrop ? (
              <ExternalLink href={currentPlant.relatedCrop} label="FCU" />
            ) : null}
            {currentPlant.taxRef ? (
              <ExternalLink href={currentPlant.taxRef} label="TaxRef" />
            ) : null}
          </Flex>
        </Flex>
        {currentPlant.botanicalFamilies.length !== 0 && (
          <Box>
            <Flex flexDirection={'row'}  alignItems="center" justifyContent="flex-start" gap={8}>
              <Text mt={5}>Famille Botanique:</Text>
              <SimpleGrid columns={2} spacing={10} mt={5}>
                {currentPlant.botanicalFamilies.map(({id, name }) => {
                  return (
                    <MyCard key={id}>
                      {
                        <Text fontWeight="bold" fontSize="l">
                          {name}
                        </Text>
                      }
                    </MyCard>
                  );
                })}
              </SimpleGrid>
            </Flex>
            <Flex flexDirection={'row'}  alignItems="center" justifyContent="flex-start" gap={8}>
              <Text mt={5}>Famille d'usage:</Text>
              <SimpleGrid columns={2} spacing={10} mt={5}>
                {currentPlant.usageFamilies.map(({ id, name }) => {
                  return (
                    <MyCard key={id}>
                      {
                        <Text fontWeight="bold" fontSize="l">
                          {name}
                        </Text>
                      }
                    </MyCard>
                  );
                })}
              </SimpleGrid>
            </Flex>
          </Box>
        )}
        <Box>
          <Text mt={5}>Itks associés:</Text>
          {itksForPlant && itksForPlant.length > 0
          ?
            <SimpleGrid columns={3} spacing={10} mt={5}>
              {itksForPlant.map((itk: Itk) => {
                return <ITKCard key={itk.id} itk={itk} />;
              })}
            </SimpleGrid>
          : <Text fontWeight="bold" fontSize="l">Aucun</Text>
          }
          </Box>
      </MyContainerDetail>
      <MyFooter>
        <CreatePdf documentContent={exportPdf(currentPlant, "plant")} />
        {isPermission ? (
          <Box>
            <Button
              onClick={() => {
                removePlant(currentPlant);
                navigate("/plant");
              }}
              colorScheme="red"
              variant="outline"
            >
              Supprimer
            </Button>
          </Box>
        ) : (
          <Box />
        )}
      </MyFooter>
    </>
  );
}

const ExternalLink = ({ href, label }: { href: string; label: string }) => (
  <Link to={href} target="_blank" rel="noopener noreferrer">
    <Box
      as="span"
      color={uiDesign.title.textColor}
      p={2}
      bg="white"
      borderRadius="md"
      boxShadow="md"
      _hover={{ bg: "teal.500", color: "white" }}
    >
      {label}
      <ExternalLinkIcon mx="2px" />
    </Box>
  </Link>
);
