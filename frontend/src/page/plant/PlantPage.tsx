import {
  Box,
  Button,
  Flex,
  Grid,
  GridItem,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import { zodResolver } from "@hookform/resolvers/zod";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useSearchParams } from "react-router-dom";
import { z } from "zod";
import { Loading } from "../../component/Loading";
import { PlantCard } from "../../component/card";
import { MyGrid } from "../../component/display/MyGrid";
import { PageLayout } from "../../component/layout";
import { SearchBar } from "../../component/search/SearchBar";
import { useMyContext } from "../../context/MyContext";
import { usePlant } from "../../context/usePlant";
import { Plant, PlantFilter, generatePlant, myFilterPlant } from "../../utils";
import { useFetch } from "../../utils/hooks/useFetch";
import { uiDesign } from "../../utils/styles";
import { FilterPlant } from "./PlantFilter";
import { PlantModal } from "./PlantModal";

const validationSchema = z.object({
  title: z.string().min(1, "trop court").max(32, "Trop long"),
  description: z.string().max(256, "Trop long").optional(),
});

export function PlantPage() {
  const { plantList, plantLoading } = useMyContext();
  const { addPlant } = usePlant();
  const [searchParams] = useSearchParams();
  const [searchPlant, setSearchPlant] = useState<string>(
    searchParams.get("search") || ""
  );
  const [isPermission, setIsPermission] = useState<boolean>(false);
  const [filterPlant, setMyFilterPlant] = useState<PlantFilter>({
    title: "",
    usageId: "",
    botaniqueId: "",
  });
  const {
    values: user,
    loading: loadingUser,
    error: errorUser,
  }: {
    values: any;
    loading: boolean;
    error: any;
  } = useFetch(
    import.meta.env.VITE_URL_QUERY,
    {
      query: `
        query {
          getUser
          }`,
    },
    "POST",
    {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    }
  );

  useEffect(() => {
    if (loadingUser || !user?.data) return;
    if (errorUser) console.log(errorUser);
    if (user.data.getUser === "success") setIsPermission(true);
  }, [user]);

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({ resolver: zodResolver(validationSchema) });
  const { isOpen, onOpen, onClose } = useDisclosure();
  const toast = useToast();

  const onSubmit = (el: Plant) => {
    addPlant(
      generatePlant({
        ...el,
      })
    );
    toast({
      title: "Formulaire envoyé.",
      description: "Nous avons bien reçu votre formulaire.",
      status: "success",
      duration: 900,
      isClosable: true,
    });
    reset();
    onClose();
  };

  return (
    <PageLayout>
      <Grid
        templateAreas={`"filterPlant SearchBar"
					"filterPlant cardPlant"`}
        gridTemplateColumns={"250px 1fr"}
        gridTemplateRows={"auto 1fr"}
        height="100%"
      >
        <GridItem gridArea="filterPlant">
          {isPermission ? (
            <Box>
              <Button
                colorScheme={uiDesign.buttonAdd.colorScheme}
                onClick={onOpen}
                w="100%"
                mb={6}
              >
                Ajout d'une plante
              </Button>
              <PlantModal
                register={register}
                errors={errors}
                onSubmit={onSubmit}
                handleSubmit={handleSubmit}
                isOpen={isOpen}
                onOpen={onOpen}
                onClose={onClose}
                reset={reset}
              />
            </Box>
          ) : null}
          <FilterPlant filter={setMyFilterPlant} valueFilter={filterPlant} />
        </GridItem>
        <GridItem gridArea="SearchBar" pl={5}>
          <Flex mb={5}>
            <SearchBar setSearch={setSearchPlant} search={searchPlant} />
          </Flex>
        </GridItem>
        <GridItem gridArea="cardPlant" pl={5}>
          {plantLoading && <Loading />}
          {!plantLoading && (
            <MyGrid columns={4}>
              {myFilterPlant(plantList, searchPlant, filterPlant).map(
                (plant) => (
                  <PlantCard
                    plant={plant}
                    key={`${plant.id}`}
                    path={`/plant/${plant.id}`}
                  />
                )
              )}
            </MyGrid>
          )}
        </GridItem>
      </Grid>
    </PageLayout>
  );
}
