import { Box, Text } from "@chakra-ui/react";
import {
  FieldValues,
  UseFormGetValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { MyModal } from "../../component/display/MyModal";
import { MyError, MyTextArea, MyTextInput } from "../../component/form";
import { Plant } from "../../utils";

interface Props {
  register: any;
  errors: any;
  onSubmit?: (el: any) => void;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  isOpen: boolean;
  onOpen: () => void;
  onClose: () => void;
  reset: () => void;
}

export function PlantModal({
  register,
  errors,
  handleSubmit,
  onSubmit,
  isOpen,
  onOpen,
  onClose,
  reset,
}: Props) {
  return (
    <MyModal
      title="Création d'une plante"
      handleSubmit={handleSubmit}
      onSubmit={onSubmit}
      isOpen={isOpen}
      onOpen={onOpen}
      onClose={onClose}
      reset={reset}
    >
      <Box>
        <Text>Nom: </Text>
        <MyTextInput
          name="title"
          register={register as UseFormGetValues<Plant>}
        />
        <MyError errors={errors["title"]} />
        <Text>Description: </Text>
        <MyTextArea
          name="description"
          register={register as UseFormGetValues<Plant>}
        />
        <MyError errors={errors["description"]} />
      </Box>
    </MyModal>
  );
}
