import { Box, FormControl, FormLabel, Select } from "@chakra-ui/react";
import { botaniqueFamilyOptions, usageFamilyOptions } from "../../utils";
import { PlantFilter } from "../../utils/types";

interface Props {
  filter: (filter: PlantFilter) => void;
  valueFilter: PlantFilter;
}

export function FilterPlant({ filter, valueFilter }: Props) {
  return (
    <Box>
      <FormControl>
        <FormLabel>Famille botanique</FormLabel>
        <Select
          placeholder="Choisir une option"
          onChange={(e) =>
            filter({ ...valueFilter, botaniqueId: e.target.value })
          }
        >
          {botaniqueFamilyOptions.toSorted((a, b) => a.label! < b.label! ? -1 : 1).map((el) => {
            return (
              <option key={el.value} value={el.value}>
                {el.label}
              </option>
            );
          })}
        </Select>
      </FormControl>
      <FormControl>
        <FormLabel pt={4}>Famille d'usage</FormLabel>
        <Select
          placeholder="Choisir une option"
          onChange={(e) => filter({ ...valueFilter, usageId: e.target.value })}
        >
          {usageFamilyOptions.toSorted((a, b) => a.label! < b.label! ? -1 : 1).map((el) => {
            return (
              <option key={el.value} value={el.value}>
                {el.label}
              </option>
            );
          })}
        </Select>
      </FormControl>
    </Box>
  );
}
