import { Box, Button, Flex, Heading, Text } from "@chakra-ui/react";
import { useParams } from "react-router-dom";
import { CreatePdf } from "../../component/CreatePdf";
import { MyContainerDetail } from "../../component/display/MyContainerDetail";
import { MainLayout } from "../../component/layout";
import { MyFooter } from "../../component/layout/MyFooter";
import { exportPdf } from "../../utils/export/pdf";
import NotFound from "../errors/NotFound";
import { TaskEdit } from "./TaskEdit";
import { useFetch } from "../../utils/hooks/useFetch";
import { requestQueryTask } from "../../graphql/itk";

//TODO: Rajouter les itk dans les taches
export function TaskDetail() {
  const { id } = useParams<{ id: string }>();

  const {
    values,
    loading,
    error,
  }: { values: any; loading: boolean; error: any } = useFetch(
    import.meta.env.VITE_URL_QUERY,
    {
      query: requestQueryTask(id),
    }
  );

  if (loading) return <div />;
  if (error) console.log(error);

  if (id === undefined || !values?.data) return <NotFound />;
  const currentTask = {
    id: values.data.getTask.id.split("#")[1],
    title: values.data.getTask.id.split("#")[1],
    type: values.data.getTask.type,
  };

  return (
    <MainLayout>
      <MyContainerDetail>
        <Flex alignItems="center" justifyContent="flex-start" mb={10}>
          <Heading size="xl" mb={-1.5}>
            {currentTask.title}
          </Heading>
        </Flex>
        <Box mt={5}>
          <Text ml={5} mt={1}>
            Type de plantation : {currentTask.type}
          </Text>
        </Box>
      </MyContainerDetail>
      <MyFooter>
        <CreatePdf documentContent={exportPdf(currentTask, "task")} />
        <TaskEdit currentTask={currentTask} />
        <Button
          // onClick={() => removeTaskDetail()}
          colorScheme="red"
          variant="outline"
        >
          Supprimer
        </Button>
      </MyFooter>
    </MainLayout>
  );
}
