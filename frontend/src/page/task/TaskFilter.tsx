import { FormControl, FormLabel, Select } from "@chakra-ui/react";
import { TaskFilter } from "../../utils";

interface Props {
  filter: (filter: TaskFilter) => void;
  valueFilter: TaskFilter;
}

export function FilterTask({ filter, valueFilter }: Props) {
  return (
    <FormControl>
      <FormLabel>Option</FormLabel>
      <Select
        placeholder="Choisir une option"
        onChange={(e) => filter({ ...valueFilter })}
      >
        <option value="">Tous</option>
      </Select>
    </FormControl>
  );
}
