import {
  Button,
  Flex,
  Grid,
  GridItem,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import { zodResolver } from "@hookform/resolvers/zod";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { TaskCard } from "../../component/card/TaskCard";
import { MyGrid } from "../../component/display";
import { MainLayout, PageLayout } from "../../component/layout";
import { SearchBar } from "../../component/search/SearchBar";
import { useMyContext } from "../../context/MyContext";
import { useTask } from "../../context/useTask";
import { TaskFilter, myFilter } from "../../utils";
import { TOAST_SUCCESS } from "../../utils/notification";
import { FilterTask } from "./TaskFilter";
import { TaskModal } from "./TaskModal";

const validationSchema = z.object({
  title: z
    .string()
    .min(1, "Trop court")
    .max(32, "Trop long (max 32 caractères))"),
  description: z.string().max(256, "Trop long (max 256 caractères))"),
  subCategory: z.string().refine((val) => {
    return val !== "" ? true : false;
  }, "Veuillez choisir une catégorie"),
  useInfo: z.boolean(),
});

type ValidationSchema = z.infer<typeof validationSchema>;

export function TaskPage() {
  const [searchTask, setSearchTask] = useState<string>("");
  const [myFilterTask, setMyFilterTask] = useState<TaskFilter>({});
  const toast = useToast();
  const { taskList } = useMyContext();
  const { addTask } = useTask();

  const {
    register,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
  } = useForm({ resolver: zodResolver(validationSchema) });

  const { isOpen, onOpen, onClose } = useDisclosure();

  const onSubmit = (el: ValidationSchema) => {
    reset();
    onClose();
    toast(TOAST_SUCCESS);
  };

  return (
    <MainLayout>
      <PageLayout>
        <Grid
          templateAreas={`"filterTask SearchBar"
                          "filterTask TaskList"`}
          gridTemplateColumns="250px 1fr"
          gridTemplateRows="auto 1fr"
          height={"100%"}
        >
          <GridItem gridArea="filterTask" pt={5}>
            <FilterTask filter={setMyFilterTask} valueFilter={myFilterTask} />
          </GridItem>
          <GridItem gridArea="SearchBar" pl={5}>
            <Flex justifyContent="space-between" alignItems="center">
              <SearchBar setSearch={setSearchTask} />
              <Button colorScheme="teal" onClick={onOpen} ml={5}>
                Ajouter une tâche
              </Button>
              <TaskModal
                setValue={setValue}
                isOpen={isOpen}
                onClose={onClose}
                onSubmit={onSubmit}
                register={register}
                errors={errors}
                onOpen={onOpen}
                handleSubmit={handleSubmit}
                reset={reset}
              />
            </Flex>
          </GridItem>
          <GridItem gridArea="TaskList" pl={5}>
            <MyGrid>
              {myFilter(taskList, searchTask, myFilterTask).map((el) => {
                return (
                  <TaskCard key={el.id} path={`/task/${el.id}`} task={el} />
                );
              })}
            </MyGrid>
          </GridItem>
        </Grid>
      </PageLayout>
    </MainLayout>
  );
}
