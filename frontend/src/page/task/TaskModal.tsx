import { Box, Checkbox, Flex, Text } from "@chakra-ui/react";
import { Select } from "chakra-react-select";
import {
  FieldValues,
  UseFormGetValues,
  UseFormHandleSubmit,
} from "react-hook-form";
import { MyModal } from "../../component/display";
import { MyError, MyTextArea, MyTextInput } from "../../component/form";
import { useMyContext } from "../../context/MyContext";
import { getTaskCategories } from "../../utils";

interface Props {
  register: any;
  setValue: any;
  errors: any;
  onSubmit?: (el: any) => void;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  isOpen: boolean;
  onOpen: () => void;
  onClose: () => void;
  reset: () => void;
}

export function TaskModal({
  register,
  setValue,
  errors,
  handleSubmit,
  onSubmit,
  isOpen,
  onOpen,
  onClose,
  reset,
}: Props) {
  const { taskList } = useMyContext();
  return (
    <MyModal
      title="Création d'une tâche"
      handleSubmit={handleSubmit}
      onSubmit={onSubmit}
      isOpen={isOpen}
      onOpen={onOpen}
      onClose={onClose}
      reset={reset}
    >
      <Box>
        <Text>Titre: </Text>
        <MyTextInput
          label="Titre"
          name="title"
          register={register as UseFormGetValues<any>}
        />
        <MyError errors={errors["title"]} />
        <Text>Description: </Text>
        <MyTextArea
          name="description"
          register={register as UseFormGetValues<any>}
        />
        <MyError errors={errors["description"]} />
        <Text>Sous-catégorie: </Text>
        <Select
          useBasicStyles
          name="subCategory"
          options={getTaskCategories(taskList)}
          onChange={(e: any) => {
            setValue("subCategory", e.value);
          }}
          selectedOptionStyle="check"
          closeMenuOnSelect={false}
        />
        <MyError errors={errors["subCategory"]} />
        <Flex>
          <Text>useInfo: </Text>
          <Checkbox name="useInfo" {...register("useInfo")} />
        </Flex>
      </Box>
    </MyModal>
  );
}
