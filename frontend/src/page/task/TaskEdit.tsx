import { Button, Text, useDisclosure, useToast } from "@chakra-ui/react";
import { zodResolver } from "@hookform/resolvers/zod";
import { UseFormGetValues, useForm } from "react-hook-form";
import { z } from "zod";
import { MyModal } from "../../component/display";
import { MyError, MyTextInput } from "../../component/form";
import { useMyContext } from "../../context/MyContext";
import { useTask } from "../../context/useTask";
import { Task } from "../../utils";

const validationSchema = z.object({
  title: z.string().min(1, "trop court").max(32, "Trop long"),
  description: z.string().max(256, "Trop long").optional(),
  useInfo: z.boolean(),
  subCategory: z.string().refine((val) => {
    return val !== "" ? true : false;
  }, "Veuillez choisir une catégorie"),
});

type ValidationSchema = z.infer<typeof validationSchema>;

interface Props {
  currentTask: Task;
}

export function TaskEdit({ currentTask }: Props) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { taskList } = useMyContext();
  const { updateTask } = useTask();
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(validationSchema),
    defaultValues: currentTask,
  });
  const toast = useToast();

  const onSubmit = (el: Task) => {
    el.id = currentTask.id;
    updateTask(el);
    reset();
    onClose();
    toast({
      title: "Tâche modifiée",
      description: "La tâche a bien été modifiée",
      status: "success",
      duration: 900,
      isClosable: true,
    });
  };

  return (
    <Button onClick={onOpen} colorScheme="green" variant="outline" mr={2}>
      Modifier
      <MyModal
        title="Modication de Tâche"
        isOpen={isOpen}
        onClose={onClose}
        onOpen={onOpen}
        handleSubmit={handleSubmit}
        onSubmit={onSubmit}
        reset={reset}
      >
        <Text pb={10}>Id: {currentTask.id}</Text>
        <Text>Titre:</Text>
        <MyTextInput
          label="Titre"
          name="title"
          register={register as UseFormGetValues<ValidationSchema>}
        />
        <MyError errors={errors.title} />
      </MyModal>
    </Button>
  );
}
