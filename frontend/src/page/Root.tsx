import { Outlet } from "react-router-dom";
import { MainLayout } from "../component/layout";

export function Root() {
  return (
    <MainLayout>
      <Outlet />
    </MainLayout>
  );
}
