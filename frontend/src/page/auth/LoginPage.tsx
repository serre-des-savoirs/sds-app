import {
  Button,
  Card,
  CardBody,
  Center,
  Divider,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Image,
  Stack,
  useToast,
} from "@chakra-ui/react";
import { zodResolver } from "@hookform/resolvers/zod";
import { UseFormGetValues, useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { z } from "zod";
import { MyTextInput } from "../../component/form";
import {
  TOAST_ERROR_LOGIN,
  TOAST_SUCCESS_LOGIN,
} from "../../utils/notification";
import { uiDesign } from "../../utils/styles";

const validationSchema = z.object({
  username: z
    .string()
    .min(1, "Merci de renseigner un nom d'utilisateur")
    .nonempty("Champ requis"),
  password: z
    .string()
    // .min(8, "Trop court")
    .nonempty("Mot de passe requis"),
  // .regex(
  //   /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/,
  //   "Le mot de passe doit contenir au moins 8 caractères, une majuscule et un chiffre"
  // ),
});

type ValidationSchema = z.infer<typeof validationSchema>;

export function LoginPage() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ValidationSchema>({ resolver: zodResolver(validationSchema) });

  const toast = useToast();
  const navigate = useNavigate();

  // TODO : avoid using JSON.stringify
  const onSubmit = (data: ValidationSchema) => {
    fetch(import.meta.env.VITE_URL_QUERY, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        query: `mutation {
          signIn(email: "${data.username}", password: "${data.password}")
        }`,
      }),
    })
      .then((res) => res.json())
      .then((el) => {
        if (el.data.signIn === "error") {
          toast(TOAST_ERROR_LOGIN);
          return;
        }
        localStorage.setItem("token", el.data.signIn);
        toast(TOAST_SUCCESS_LOGIN(data.username));
        navigate("/");
      })
      .catch((err) => console.log(err));
  };

  return (
    <Card mt={10}>
      <CardBody>
        <Center>
          <Image src="/logo-color.jpg" borderRadius="lg" />
        </Center>
        <Stack mt={6} spacing={3}>
          <Heading>Page de connexion</Heading>
          <Stack spacing={3}>
            <FormControl isInvalid={!!errors.username} id="username" isRequired>
              <FormLabel>Nom d'utilisateur</FormLabel>
              <MyTextInput
                register={register as UseFormGetValues<ValidationSchema>}
                name="username"
                label="Nom d'utilisateur"
              />
              <FormErrorMessage>{errors.username?.message}</FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={!!errors.password} id="password" isRequired>
              <FormLabel>Mot de passe</FormLabel>
              <MyTextInput
                register={register as UseFormGetValues<ValidationSchema>}
                name="password"
                label="Mot de passe"
                type="password"
              />
              <FormErrorMessage>{errors.password?.message}</FormErrorMessage>
            </FormControl>
          </Stack>
          <Button
            onClick={handleSubmit(onSubmit)}
            bgColor={uiDesign.buttonAdd.backgroundColor}
            color={uiDesign.title.textColor}
          >
            Se connecter
          </Button>
          <Divider />
          <Button
            onClick={() =>
              window.open("https://wkf.ms/48HNHA3", "_blank", "noreferrer")
            }
            bgColor={uiDesign.register.backgroundColor}
            color={uiDesign.title.textColor}
          >
            Créer un compte
          </Button>
        </Stack>
      </CardBody>
    </Card>
  );
}
