import {
  Button,
  Card,
  CardBody,
  Center,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Image,
  Stack,
  useToast,
} from "@chakra-ui/react";
import { zodResolver } from "@hookform/resolvers/zod";
import { UseFormGetValues, useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { z } from "zod";
import { MyTextInput } from "../../component/form";
import { TOAST_SUCCESS_REGISTER } from "../../utils/notification";

const validationSchema = z.object({
  username: z
    .string()
    .min(1, "Merci de renseigner un nom d'utilisateur")
    .nonempty("Champ requis"),
  password: z
    .string()
    .min(8, "Trop court")
    .nonempty("Mot de passe requis")
    .regex(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/,
      "Le mot de passe doit contenir au moins 8 caractères, une majuscule et un chiffre"
    ),
});

type ValidationSchema = z.infer<typeof validationSchema>;

export function RegisterPage() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ValidationSchema>({ resolver: zodResolver(validationSchema) });
  const toast = useToast();
  const navigate = useNavigate();

  const onSubmit = (data: ValidationSchema) => {
    toast(TOAST_SUCCESS_REGISTER(data.username));
    navigate("/login");
  };

  return (
    <Card mt={10}>
      <CardBody>
        <Center>
          <Image src="/logo-color.jpg" borderRadius="lg" />
        </Center>
        <Stack mt={6} spacing={3}>
          <Heading>Page d'inscription</Heading>
          <Stack spacing={3}>
            <FormControl isInvalid={!!errors.username} id="username" isRequired>
              <FormLabel>Username</FormLabel>
              <MyTextInput
                register={register as UseFormGetValues<ValidationSchema>}
                name="username"
                label="Username"
              />
              <FormErrorMessage>{errors.username?.message}</FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={!!errors.password} id="password" isRequired>
              <FormLabel>Password</FormLabel>
              <MyTextInput
                register={register as UseFormGetValues<ValidationSchema>}
                name="password"
                label="Password"
                type="password"
              />
              <FormErrorMessage>{errors.password?.message}</FormErrorMessage>
            </FormControl>
          </Stack>
          <Button onClick={handleSubmit(onSubmit)}>Submit</Button>
        </Stack>
      </CardBody>
    </Card>
  );
}
