import { Box, Button, Flex, Grid, GridItem, Heading } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { CreatePdf } from "../../component/CreatePdf";
import { Loading } from "../../component/Loading";
import { MySpacer } from "../../component/MySpacer";
import { MyBoxContent } from "../../component/card/MyBoxContent";
import { MyContainerDetail } from "../../component/display/MyContainerDetail";
import { MyFooter } from "../../component/layout/MyFooter";
import { UpateTitleModal } from "../../component/update/UpdateTitleModal";
import { AUTH_USER } from "../../graphql/auth";
import { requestQueryItk } from "../../graphql/itk";
import { Itk } from "../../utils";
import { deduplicateObjects, sortObjects } from "../../utils/cleanGraphData";
import { exportPdf } from "../../utils/export/pdf";
import { useFetch } from "../../utils/hooks/useFetch";
import { uiDesign } from "../../utils/styles";
import NotFound from "../errors/NotFound";
import { CulturalContextDetail } from "./detail/CulturalContextDetail";
import { GeneralDetail } from "./detail/GeneralDetail";
import { ImplantationDetail } from "./detail/ImplantationDetail";
import { TaskDetail } from "./detail/TaskDetail";
import { PlantImage } from "../../component/card/PlantImage";

export function ItkDetail() {
  const { id } = useParams<{ id: string }>();
  const [currentItk, setCurrentItk] = useState<Itk | undefined>(undefined);
  const [isUpdate, setIsUpdate] = useState<boolean>(false);
  const [isPermission, setIsPermission] = useState<boolean>(false);
  const navigate = useNavigate();

  const {
    values,
    loading,
    error,
  }: { values: any; loading: boolean; error: any } = useFetch(
    import.meta.env.VITE_URL_QUERY,
    {
      query: requestQueryItk(id),
    }
  );
  const token = localStorage.getItem("token");
  const {
    values: user,
    loading: loadingUser,
    error: errorUser,
  }: {
    values: any;
    loading: boolean;
    error: any;
  } = useFetch(
    import.meta.env.VITE_URL_QUERY,
    {
      query: AUTH_USER,
    },
    "POST",
    {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    }
  );
  useEffect(() => {
    if (loadingUser || !user?.data) return;
    if (errorUser) console.log(errorUser);
    if (user.data.getUser === "success") setIsPermission(true);
  }, [user]);

  useEffect(() => {
    if (!values?.data || !id) return;
    setCurrentItk({
      id: id,
      title: values.data.getItk.name,
      technicalCulture: values.data.getItk.cultureMode,
      plantId: values.data.getItk.plantId?.split("#")[1],
      source: values.data.getItk.source,
      typeSoil: values.data.getItk.typeSoil,
      tasks: sortObjects(
        deduplicateObjects(values.data.getItk.task),
        (a, b) => {
          return (
            a.hasOperationDistance.distanceDuration.numericDuration -
            b.hasOperationDistance.distanceDuration.numericDuration
          );
        }
      ),
      season: values.data.getItk.season,
      hasRelativePeriod: values.data.getItk.hasRelativePeriod,
      dependsOnCulturalContext: values.data.getItk.dependsOnCulturalContext,
      dependsOnSoilClimateContext:
        values.data.getItk.dependsOnSoilClimateContext,
      hasImplantationMode: values.data.getItk.implantationMode,
      hasYield: values.data.getItk.hasYield,
      lossMargin: values.data.getItk.lossMargin,
      hasGlobalWorload: values.data.getItk.hasGlobalWorload,
      hasFarmEquipment: values.data.getItk.hasFarmEquipment,
      hasIrrigationMode: values.data.getItk.hasIrrigationMode,
    });
  }, [values]);

  if (loading || !currentItk) return <Loading />;
  if (error) console.log(error);

  if (id === undefined || !values?.data) return <NotFound />;

  return (
    <>
      <MyContainerDetail>
        <Flex>
          <Heading size="xl" alignItems={"center"} display={"center"} gap={4}>
            <PlantImage plantId={currentItk.plantId} />
            {currentItk.title}
          </Heading>
          <MySpacer w={5} />
          {isUpdate ? (
            <UpateTitleModal
              currentObject={currentItk}
              onChangeObject={setCurrentItk as any}
            />
          ) : (
            <Box />
          )}
        </Flex>
        <Grid
          templateAreas={`"infoItk detailItk"
					"taskItk taskItk"`}
          gridTemplateColumns={"550px 1fr"}
          gap={6}
          pt={5}
        >
          <GridItem gridArea="infoItk">
            <GeneralDetail
              currentItk={currentItk}
              isUpdated={isUpdate}
              setCurrentItk={setCurrentItk}
            />
            <MySpacer h={5} />
            {/* <CompatibilityDetail
              currentItk={currentItk}
              isUpdated={isUpdate}
              setCurrentItk={setCurrentItk}
            /> */}
          </GridItem>
          <GridItem gridArea="detailItk">
            <ImplantationDetail
              currentItk={currentItk}
              isUpdated={isUpdate}
              setCurrentItk={setCurrentItk}
            />
            <MySpacer h={5} />
            <CulturalContextDetail
              currentItk={currentItk}
              isUpdated={isUpdate}
              setCurrentItk={setCurrentItk}
            />
          </GridItem>
          <GridItem gridArea="taskItk">
            <MyBoxContent>
              <Heading size="md">Tâches</Heading>
              {currentItk.tasks.length > 0 ? (
                <TaskDetail
                  currentItk={currentItk}
                  isUpdated={isUpdate}
                  setCurrentItk={setCurrentItk}
                />
              ) : (
                <Box />
              )}
            </MyBoxContent>
          </GridItem>
        </Grid>
      </MyContainerDetail>
      <MyFooter>
        {/* <CreatePdf documentContent={exportPdf(currentItk, "itk")} /> */}
        {isPermission ? (
          <Box>
            <Button
              colorScheme={uiDesign.buttonUpdate.colorScheme}
              variant="outline"
              mr={2}
              onClick={() => setIsUpdate((isUpdate) => !isUpdate)}
            >
              Modifier
            </Button>
            <Button
              colorScheme={uiDesign.buttonDeleteCancel.colorScheme}
              variant="outline"
              onClick={() => {
                navigate("/itk");
              }}
            >
              Supprimer
            </Button>
          </Box>
        ) : (
          <Box />
        )}
      </MyFooter>
    </>
  );
}
