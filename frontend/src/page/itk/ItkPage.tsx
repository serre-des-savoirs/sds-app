import {
  Box,
  Button,
  Flex,
  Grid,
  GridItem,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import { zodResolver } from "@hookform/resolvers/zod";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { Loading } from "../../component/Loading";
import { ITKCard } from "../../component/card";
import { MyGrid } from "../../component/display/MyGrid";
import { PageLayout } from "../../component/layout";
import { SearchBar } from "../../component/search/SearchBar";
import { useMyContext } from "../../context/MyContext";
import { useItk } from "../../context/useItk";
import { AUTH_USER } from "../../graphql/auth";
import { Itk, ItkFilter, generateItk, myFilterItk } from "../../utils";
import { useFetch } from "../../utils/hooks/useFetch";
import { uiDesign } from "../../utils/styles";
import { FilterItk } from "./ItkFilter";
import { ItkModal } from "./ItkModal";

const validationSchema = z.object({
  title: z.string().min(1, "Trop court").max(32, "Trop long"),
  description: z.string().max(256, "Trop long").optional(),
  technicalCulture: z.string().refine((el) => {
    return el === "" ? false : true;
  }, "Valeur non valide"),
  plantId: z.string().refine((el) => {
    return el === "" ? false : true;
  }, "Valeur non valide"),
  tasks: z
    .array(
      z.object({
        taskId: z.string(),
        info: z.string().optional(),
      })
    )
    .optional(),
});

export function ItkPage() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { itkList, itkLoading } = useMyContext();
  const { addItk } = useItk();
  const [searchItk, setSearchItk] = useState<string>("");
  const [isPermission, setIsPermission] = useState<boolean>(false);
  const [filterItk, setMyFilterItk] = useState<ItkFilter>({
    technicalCulture: "",
    plantId: "",
    hasImplantationMode: "",
    regions: "",
  });
  const {
    values: user,
    loading: loadingUser,
    error: errorUser,
  }: {
    values: any;
    loading: boolean;
    error: any;
  } = useFetch(
    import.meta.env.VITE_URL_QUERY,
    {
      query: AUTH_USER,
    },
    "POST",
    {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    }
  );

  useEffect(() => {
    if (loadingUser || !user?.data) return;
    if (errorUser) console.log(errorUser);
    if (user.data.getUser === "success") setIsPermission(true);
  }, [user]);
  const {
    register,
    setValue,
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({ resolver: zodResolver(validationSchema) });
  const toast = useToast();

  const onSubmit = (el: Itk) => {
    addItk(generateItk(el));
    toast({
      title: "Formulaire envoyé",
      description: "Nous avons bien reçu votre formulaire.",
      status: "success",
      duration: 900,
      isClosable: true,
    });
    reset();
    onClose();
  };

  return (
    <PageLayout>
      <Grid
        templateAreas={`"filterItk SearchBar"
								"filterItk cardItk"`}
        gridTemplateColumns={"250px 1fr"}
        gridTemplateRows={"auto 1fr"}
        height="100%"
      >
        <GridItem area={"SearchBar"} pl="5">
          <Flex justify="space-between">
            <SearchBar setSearch={setSearchItk} />
          </Flex>
        </GridItem>
        <GridItem gridArea="filterItk">
          {isPermission ? (
            <Box>
              <Button
                colorScheme={uiDesign.buttonAdd.colorScheme}
                onClick={onOpen}
                w="100%"
                mb={6}
              >
                Ajout d'ITK
              </Button>
              <ItkModal
                isOpen={isOpen}
                onClose={onClose}
                onSubmit={onSubmit}
                register={register}
                control={control}
                errors={errors}
                onOpen={onOpen}
                handleSubmit={handleSubmit}
                reset={reset}
                setValue={setValue}
              />
            </Box>
          ) : null}
          <FilterItk filter={setMyFilterItk} valueFilter={filterItk} />
        </GridItem>
        <GridItem gridArea="cardItk" pl="5">
          {itkLoading && <Loading />}
          {!itkLoading && (
            <MyGrid columns={3}>
              {myFilterItk(itkList, searchItk, filterItk).map((itk, index) => {
                return (
                  <ITKCard key={`${index}`} itk={itk} />
                );
              })}
            </MyGrid>
          )}
        </GridItem>
      </Grid>
    </PageLayout>
  );
}
