import { ExternalLinkIcon } from "@chakra-ui/icons";
import { addDays, format, parse } from 'date-fns'
import { Badge, Box, Flex, Heading, Text } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { MySpacer } from "../../../component/MySpacer";
import { MyBasicCard } from "../../../component/card/MyBasicCard";
import { MyBoxContent } from "../../../component/card/MyBoxContent";
import { UpdateSelectModal } from "../../../component/update/UpdateSelectModal";
import { useMyContext } from "../../../context/MyContext";
import { QUERY_GET_ALL_PLANTS } from "../../../graphql/itk";
import { EMPTY_RESULT, Itk } from "../../../utils";
import {
  calculateYearDifference,
  convertMeasure,
  convertWeekTimeToDate,
  convertWeekTimeToFormattedString,
} from "../../../utils/convert";
import { useFetch } from "../../../utils/hooks/useFetch";

interface Props {
  currentItk: Itk;
  setCurrentItk: (itk: Itk) => void;
  isUpdated: boolean;
}

export function GeneralDetail({ currentItk, setCurrentItk, isUpdated }: Props) {
  const yearDifference = calculateYearDifference(
    currentItk.hasRelativePeriod?.hasBeginning as string,
    currentItk.hasRelativePeriod?.hasEnding as string
  );

  const renderImplantationPeriod = () => {
    const implantationTask = currentItk.tasks.find(
      (el) =>
        Number(el.hasOperationDistance.distanceDuration.numericDuration) === 0
    );
    const implantationBeginning =
      implantationTask?.hasRelativePeriod?.hasBeginning || currentItk.hasRelativePeriod?.hasBeginning;
    const yearDifference = calculateYearDifference(
      currentItk.hasRelativePeriod?.hasBeginning as string,
      implantationBeginning as string
    );
    if (implantationBeginning) {
      return (
        <Flex>
          <Text mr={2} as="b">
            {convertWeekTimeToFormattedString(implantationBeginning as string)}
          </Text>
          {yearDifference !== 0 && (
            <Badge ml={2}>année +{yearDifference}</Badge>
          )}
        </Flex>
      );
    } else {
      return <Text as="b">{EMPTY_RESULT}</Text>;
    }
  };

  const renderRecoltePeriod = () => {
    const harvestTask = currentItk.tasks.find(
      (el) => el.taskName === "Récolte"
    );
    let harvestBeginning: string | null = null;
    let yearDifference: number | null = null;
    if (harvestTask?.hasRelativePeriod?.hasBeginning) {
      harvestBeginning = convertWeekTimeToFormattedString(harvestTask?.hasRelativePeriod?.hasBeginning)
      yearDifference = calculateYearDifference(
        currentItk.hasRelativePeriod?.hasBeginning as string,
        harvestBeginning
      );
    } else if (harvestTask?.hasOperationDistance?.distanceDuration && harvestTask?.hasOperationDistance?.refersToOperation) {
      const duration = harvestTask.hasOperationDistance.distanceDuration;
      const durationInDays = duration.unitType.indexOf('Week') > -1 ? duration.numericDuration * 7 : duration.numericDuration
      const implantationBeginning = convertWeekTimeToDate(currentItk.hasRelativePeriod?.hasBeginning as string)!
      const harvestDate = addDays(implantationBeginning, durationInDays)
      harvestBeginning = format(harvestDate, 'dd/MM')
      yearDifference = harvestDate.getFullYear() - implantationBeginning.getFullYear()
    }

    if (harvestBeginning && yearDifference !== null) {
      return (
        <Flex>
          <Text mr={2} as="b">
            {harvestBeginning}
          </Text>
          {yearDifference !== 0 && (
            <Badge ml={2}>année +{yearDifference}</Badge>
          )}
        </Flex>
      );
    } else {
      return <Text as="b">{EMPTY_RESULT}</Text>;
    }
  };

  const {
    values,
    error,
  }: {
    values: any;
    error: any;
  } = useFetch(import.meta.env.VITE_URL_QUERY, {
    query: QUERY_GET_ALL_PLANTS,
  });

  if (error) {
    console.log(error);
  }

  const plantOptions = values?.data?.getAllPlant?.map(
    (el: { id: string; plantName: string }) => ({
      value: el.id,
      label: el.plantName,
    })
  );

  const renderUpdatePlant = () => {
    if (!values || !values.data.getAllPlant) return <Box />;
    return (
      <Box>
        {isUpdated ? (
          <UpdateSelectModal
            currentObject={currentItk}
            setObject={setCurrentItk}
            modify="plantId"
            option={plantOptions}
          />
        ) : (
          <Box w={25} h={25} />
        )}
      </Box>
    );
  };

  return (
    <MyBoxContent>
      <Heading size="md">Général</Heading>
      {currentItk.source &&
        <MyBasicCard>
          <Flex gap={4}>
            <Text>Source:</Text>
            <Text style={{ fontWeight: 600 }}>{currentItk.source}</Text>
          </Flex>
        </MyBasicCard>
      }
      
      <MyBasicCard>
        <Flex>
          <Text>Plante associée: </Text>
          <AssociatedPlantLinks currentItk={currentItk} />
          {renderUpdatePlant()}
        </Flex>
      </MyBasicCard>

      <MyBasicCard>
        <Flex alignItems="center" justifyContent="flex-start" gap={4}>
          <Text>Rendement estimé: </Text>
          <Text as="b">
            {currentItk.hasYield?.parameterValue} {convertMeasure(currentItk.hasYield?.hasParameterUnit)}
          </Text>
        </Flex>
      </MyBasicCard>
      <MyBasicCard>
        <Flex alignItems="center" justifyContent="flex-start" gap={4}>
          <Text>Temps de travail théorique: </Text>
          {currentItk.hasGlobalWorload?.parameterValue ? (
            <Flex>
              <Text as="b">
                {Number(currentItk.hasGlobalWorload?.parameterValue)}
              </Text>
              <Text ml={2}>
                {convertMeasure(currentItk.hasGlobalWorload?.hasParameterUnit)}
              </Text>
            </Flex>
          ) : (
            <Text as="b">{EMPTY_RESULT}</Text>
          )}
        </Flex>
      </MyBasicCard>
      <MyBasicCard>
        <Flex alignItems="center" justifyContent="flex-start">
          <Text>Période de l'ITK: </Text>
          <MySpacer w={5} />
          {currentItk.hasRelativePeriod?.hasBeginning ? (
            <Flex>
              <Text mr={2} as="b">
                {convertWeekTimeToFormattedString(currentItk.hasRelativePeriod?.hasBeginning)} -{" "}
                {convertWeekTimeToFormattedString(
                  currentItk.hasRelativePeriod?.hasEnding as string
                )}
              </Text>
              {yearDifference !== 0 && (
                <Badge ml={2}>année +{yearDifference}</Badge>
              )}
            </Flex>
          ) : (
            <Text as="b">{EMPTY_RESULT}</Text>
          )}
        </Flex>
        {currentItk.tasks?.length !== 0 && (
          <Box>
            <Flex alignItems="center" justifyContent="flex-start">
              <Text>Date début d'implantation: </Text>
              <MySpacer w={5} />
              {renderImplantationPeriod()}
            </Flex>
            <Flex alignItems="center" justifyContent="flex-start">
              <Text>Date début de récolte: </Text>
              <MySpacer w={5} />
              {renderRecoltePeriod()}
            </Flex>
          </Box>
        )}
      </MyBasicCard>
    </MyBoxContent>
  );
}

function AssociatedPlantLinks({ currentItk }: { currentItk: Itk }) {
  const { plantList } = useMyContext();
  return (
    <>
      {plantList.map((el, index) => {
        if (el.id === currentItk.plantId) {
          return (
            <Link to={`/plant/${el.id}`} key={`${index}`}>
              <Text as="b" ml={2}>
                {el.title}
              </Text>
              <ExternalLinkIcon mx="2px" />
            </Link>
          );
        }
        return null;
      })}
    </>
  );
}
