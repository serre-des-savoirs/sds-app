import { Box, Flex, Text, useDisclosure } from "@chakra-ui/react";
import { zodResolver } from "@hookform/resolvers/zod";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { GrFormAdd } from "react-icons/gr";
import { z } from "zod";
import { MyModal } from "../../../component/display";
import { EMPTY_TASKREF, Itk, TaskReference, getTaskInfo } from "../../../utils";
import { convertMeasure } from "../../../utils/convert";
import { InsertInfoTask } from "./InsertInfoTask";

const validationSchema = z.object({
  title: z
    .string()
    .nonempty("Le nom de la tâche ne peut pas être vide")
    .max(50, "Le nom de la tâche est trop long")
    .min(3, "Le nom de la tâche est trop court"),
});

type ValidationSchema = z.infer<typeof validationSchema>;

interface Props {
  currentItk: Itk;
  setCurrentItk: (itk: Itk) => void;
  title: string;
  min?: number;
  max?: number;
  defaultValue?: number;
}

export function AddTaskModal({
  title,
  currentItk,
  defaultValue = 0,
}: Props) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(validationSchema),
  });
  const [linkTask, setLinkTask] = useState<boolean>(false);
  const [newTask, setNewTask] = useState<TaskReference>(EMPTY_TASKREF);

  const onSubmit = (el: ValidationSchema): void => {
    setLinkTask(false);
    reset();
    onClose();
  };
  return (
    <Box>
      <GrFormAdd size={25} onClick={onOpen} className="update-field" />
      <MyModal
        title={title}
        isOpen={isOpen}
        onClose={onClose}
        onOpen={onOpen}
        handleSubmit={handleSubmit}
        onSubmit={onSubmit}
        reset={reset}
      >
        <Box>
          <InsertInfoTask
            currentItk={currentItk}
            setNewTask={setNewTask}
            setLinkTask={setLinkTask}
            newTask={newTask}
            linkTask={linkTask}
            defaultValue={defaultValue}
            register={register}
            errors={errors}
          />
          <Box p={2} border="1px solid black" borderRadius="5px" mt={10}>
            <Flex mb={2} align="center">
              <Text>Nom de la tâche: </Text>
              <Text as="b" ml={2}>
                {newTask.taskName}
              </Text>
            </Flex>
            <Flex align="center">
              <Text>distance a l'implantation: </Text>
              <Text as="b" ml={2}>
                {Number(
                  newTask.hasOperationDistance.distanceDuration.numericDuration
                ) > 0
                  ? `+ ${newTask.hasOperationDistance.distanceDuration.numericDuration} Semaines`
                  : `${newTask.hasOperationDistance.distanceDuration.numericDuration} Semaines`}
              </Text>
            </Flex>
            {linkTask ? (
              <Flex>
                <Text>Tâches de référence: </Text>
                <Text as="b" ml={2}>
                  {newTask.hasOperationDistance.refersToOperation.id.split("#")[1]}
                </Text>
              </Flex>
            ) : null}
            {/* TODO : Search for the task in the current itk and display the info */}
            {getTaskInfo(currentItk.tasks[0], newTask).map((info, index) => (
              <Flex key={`${index}`}>
                <Text>{info.title}: </Text>
                <Text as="b" ml={2}>
                  {linkTask
                    ? newTask.hasOperationDistance.refersToOperation && (
                        <Text>
                          {info.value} {convertMeasure(info.unit)}
                        </Text>
                      )
                    : info.refValue}
                </Text>
              </Flex>
            ))}
          </Box>
        </Box>
      </MyModal>
    </Box>
  );
}
