import { Box, Flex, Grid, GridItem, Input, Text } from "@chakra-ui/react";
import { MySpacer } from "../../../component/MySpacer";
import { MyNumberInput } from "../../../component/form/MyNumberInput";
import { Select } from "chakra-react-select";
import { TaskReference, hasWorkloadUnitOptions } from "../../../utils";

interface Props {
  currentTask: TaskReference;
  isUpdate: boolean;
  setValue: any;
  errors: any;
  register: any;
}

export function UpdateFields({
  currentTask,
  isUpdate,
  setValue,
  errors,
  register,
}: Props) {
  const handleDefaultValueWorkload = hasWorkloadUnitOptions.find((option) =>
    currentTask.implements.hasWorkload.hasWorkloadUnit
      ? option.value ===
        currentTask.implements.hasWorkload.hasWorkloadUnit.split("#")[1]
      : option.value
  );

  return (
    <Box>
      <Grid
        templateRows="repeat(1, 1fr)"
        templateColumns="repeat(2, 1fr)"
        gridTemplateColumns="150px 1fr"
        gap={4}
      >
        <GridItem rowSpan={1} colSpan={1}>
          <Text mt={4}>Nom de la tâche: </Text>
          {isUpdate && (
            <Box>
              <Text mt={8}>distance a l'implantation: </Text>
              <Text mt={8}>Charge de travail: </Text>
            </Box>
          )}
        </GridItem>
        <GridItem rowSpan={2} colSpan={1}>
          {isUpdate ? (
            <Box p={2}>
              <Input
                placeholder={currentTask.taskName}
                defaultValue={currentTask.taskName}
                {...register("title")}
              />
            </Box>
          ) : (
            <Box p={4}>
              <Text as="b">{currentTask.taskName}</Text>
            </Box>
          )}
          {isUpdate && (
            <Box>
              <Box p={2}>
                <MyNumberInput
                  defaultValue={
                    currentTask.hasOperationDistance.distanceDuration
                      .numericDuration
                  }
                  step={1}
                  onChange={(value) => setValue("duration", value)}
                />
              </Box>
              <Flex alignItems="center" gap={2}>
                <Box p={2}>
                  <MyNumberInput
                    defaultValue={
                      currentTask.implements.hasWorkload.parameterValue
                    }
                    step={1}
                    onChange={(value) => setValue("workload", value)}
                  />
                </Box>
                <Box p={2}>
                  <Select
                    options={hasWorkloadUnitOptions}
                    defaultValue={handleDefaultValueWorkload}
                    onChange={(el) => setValue("unit", el?.value)}
                    useBasicStyles
                  />
                </Box>
              </Flex>
              <Text color="red" ml={2}>
                {errors.duration?.message as string}
              </Text>
            </Box>
          )}
        </GridItem>
      </Grid>
    </Box>
  );
}
