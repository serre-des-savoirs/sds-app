import { Box, Flex, Heading } from "@chakra-ui/react";
import { MyBoxContent } from "../../../component/card/MyBoxContent";
import { MyCardWithBoxContent } from "../../../component/card/MyCardWithBoxContent";
import { UpdateSelectModal } from "../../../component/update/UpdateSelectModal";
import { Itk, getCulturalContext } from "../../../utils";

interface Props {
  currentItk: Itk;
  isUpdated: boolean;
  setCurrentItk: (itk: Itk) => void;
}

export function CulturalContextDetail({
  currentItk,
  isUpdated,
  setCurrentItk,
}: Props) {
  return (
    <MyBoxContent>
      <Heading size="md">Contexte cultural</Heading>
      <Flex flexWrap="wrap" gap={2}>
        {getCulturalContext(currentItk).map((culturalContext, index) => (
          <Box
            key={`${index}`}
            flex="1 0 auto"
            maxWidth={isUpdated ? "none" : "100%"}
          >
            <MyCardWithBoxContent
              title={culturalContext.title}
              content={culturalContext.content as string[]}
            >
              {isUpdated ? (
                <UpdateSelectModal
                  currentObject={currentItk}
                  setObject={setCurrentItk}
                  modify={culturalContext.modify}
                  option={culturalContext.options}
                  isMulti={culturalContext.isMultiple}
                />
              ) : (
                <Box w={25} h={25} />
              )}
            </MyCardWithBoxContent>
          </Box>
        ))}
      </Flex>
    </MyBoxContent>
  );
}
