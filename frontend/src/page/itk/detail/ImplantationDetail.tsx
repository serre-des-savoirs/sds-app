import { Box, Flex, Heading, Text, Wrap, WrapItem } from "@chakra-ui/react";
import { MyBasicCard } from "../../../component/card/MyBasicCard";
import { MyBoxContent } from "../../../component/card/MyBoxContent";
import { UpdateSelectModal } from "../../../component/update/UpdateSelectModal";
import { EMPTY_RESULT, Itk, getImplantationDetails } from "../../../utils";

interface Props {
  currentItk: Itk;
  setCurrentItk: (itk: Itk) => void;
  isUpdated: boolean;
}

export function ImplantationDetail({
  currentItk,
  setCurrentItk,
  isUpdated,
}: Props) {
  return (
    <MyBoxContent>
      <Heading size="md">Implantation</Heading>
      <Wrap spacing={2}>
        {getImplantationDetails(currentItk).map((implantationDetail, index) => (
          <WrapItem key={`${index}`}>
            <MyBasicCard>
              <Flex>
                <Text mr={4}>
                  {implantationDetail.title}:{" "}
                  <Text as="b">
                    {implantationDetail.content || EMPTY_RESULT}
                  </Text>
                </Text>
                {isUpdated ? (
                  <UpdateSelectModal
                    currentObject={currentItk}
                    setObject={setCurrentItk}
                    modify={implantationDetail.modify}
                    option={implantationDetail.options}
                  />
                ) : (
                  <Box w={25} h={25} />
                )}
              </Flex>
            </MyBasicCard>
          </WrapItem>
        ))}
      </Wrap>
    </MyBoxContent>
  );
}
