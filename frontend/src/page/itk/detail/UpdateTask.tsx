import { Box, Flex, Text, useDisclosure } from "@chakra-ui/react";
import { zodResolver } from "@hookform/resolvers/zod";
import { useMemo } from "react";
import { useForm } from "react-hook-form";
import { GrSearchAdvanced } from "react-icons/gr";
import { z } from "zod";
import { MyModal } from "../../../component/display";
import { AUTH_USER } from "../../../graphql/auth";
import { MUTATION_TASK } from "../../../graphql/itk";
import { Itk, TaskReference, getTaskInfo } from "../../../utils";
import { convertMeasure } from "../../../utils/convert";
import { useFetch } from "../../../utils/hooks/useFetch";
import { UpdateFields } from "./UpdateFields";

const validationSchema = z.object({
  title: z
    .string()
    .min(3, "Le nom doit faire au moins 3 caractères")
    .max(50, "Le nom doit faire au plus 50 caractères")
    .optional(),
  select: z.array(z.string()).optional(),
  duration: z.number().optional(),
  workload: z.number().optional(),
  unit: z.string().optional(),
});

type ValidationSchema = z.infer<typeof validationSchema>;

interface Props {
  currentItk: Itk;
  currentTask: TaskReference;
  setCurrentItk: (itk: Itk) => void;
  isUpdate: boolean;
}

export function UpdateTask({
  currentItk,
  currentTask,
  setCurrentItk,
  isUpdate,
}: Props) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    handleSubmit,
    register,
    reset,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(validationSchema),
  });
  const implantationTask = currentItk.tasks.filter(
    (task) =>
      task.taskType.indexOf('Plantation') > -1 || task.taskType.indexOf('Planting') > -1 || task.taskType.indexOf('SeedingInRows') > -1 || task.taskType.indexOf('Broadcast') > -1
  )[0];

  // TODO : Should be in a UserContext hook
  const token = localStorage.getItem("token");
  const {
    values: user,
    loading: loadingUser,
    error: errorUser,
  }: {
    values: any;
    loading: boolean;
    error: any;
  } = useFetch(
    import.meta.env.VITE_URL_QUERY,
    {
      query: AUTH_USER,
    },
    "POST",
    {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    }
  );

  const isPermission = useMemo(() => {
    return user?.data?.getUser === "success";
  }, [user]);

  const onSubmit = (el: ValidationSchema): void => {
    fetch(import.meta.env.VITE_URL_QUERY, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        query: MUTATION_TASK(
          currentTask.taskId.search("#")
            ? currentTask.taskId
            : currentTask.taskId.split("#")[1],
          el.title ? el.title : currentTask.taskName,
          el.duration
            ? el.duration
            : currentTask.hasOperationDistance.distanceDuration.numericDuration,
          el.workload
            ? el.workload
            : currentTask.implements.hasWorkload.parameterValue,
          el.unit
            ? el.unit
            : currentTask.implements.hasWorkload.hasWorkloadUnit.split("#")[1]
        ),
      }),
    })
      .then((res) =>
        setCurrentItk({
          ...currentItk,
          tasks: currentItk.tasks.map((task) => {
            if (task.taskId === currentTask.taskId) {
              return {
                ...task,
                taskName: el.title ? el.title : task.taskName,
                hasOperationDistance: {
                  ...task.hasOperationDistance,
                  distanceDuration: {
                    ...task.hasOperationDistance.distanceDuration,
                    numericDuration: el.duration
                      ? el.duration
                      : task.hasOperationDistance.distanceDuration
                          .numericDuration,
                  },
                },
                implements: {
                  ...task.implements,
                  hasWorkload: {
                    ...task.implements.hasWorkload,
                    parameterValue: el.workload
                      ? el.workload
                      : task.implements.hasWorkload.parameterValue,
                    hasWorkloadUnit: el.unit
                      ? `c3pocm#${el.unit}`
                      : task.implements.hasWorkload.hasWorkloadUnit,
                  },
                },
              };
            }
            return task;
          }),
        })
      )
      .catch((err) => console.log(err));
    reset();
    onClose();
  };

  return (
    <Box>
      <GrSearchAdvanced size={25} onClick={onOpen} className="update-field" />
      <MyModal
        title={isPermission ? "Modifier la tâche" : "Visualiser la tâche"}
        isOpen={isOpen}
        onClose={onClose}
        onOpen={onOpen}
        handleSubmit={isPermission ? handleSubmit : undefined}
        onSubmit={onSubmit}
        reset={reset}
      >
        {/* <Flex gap={20}>
          <Text>Id de la tâche: </Text>
          <Text as="b">{currentTask.taskId.split("#")[1]}</Text>
        </Flex> */}
        <UpdateFields
          currentTask={currentTask}
          isUpdate={isUpdate}
          setValue={setValue}
          errors={errors}
          register={register}
        />
        <Box p={2} border="1px solid black" borderRadius="5px" overflow="auto">
          {/* {currentTask.hasOperationDistance.refersToOperation && (
            <Flex alignItems="center">
              <Text mr={2}>Tâche de référence: </Text>
              <Text as="b">
                {
                  currentTask.hasOperationDistance.refersToOperation.id.split(
                    "#"
                  )[1]
                }
              </Text>
            </Flex>
          )} */}
          {getTaskInfo(currentTask, implantationTask)
            .filter(
              (prop) =>
                currentTask.taskType.split("#")[1] === "Planting" ||
                prop.id === "hasWorkload"
            )
            .map((task, index) => (
              <Flex key={`${index}`} alignItems="center">
                <Text>{task.title}:</Text>
                <Text as="b" ml={2}>
                  {task.value ? task.value : (task.refValue || '-')}{" "}
                  {convertMeasure(task.unit ? task.unit : task.refUnit)}
                </Text>
              </Flex>
            ))}
        </Box>
      </MyModal>
    </Box>
  );
}
