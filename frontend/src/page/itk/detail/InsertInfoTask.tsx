import { Box, Flex, Input, Select, Switch, Text } from "@chakra-ui/react";
import { MyNumberInput } from "../../../component/form/MyNumberInput";
import { useMyContext } from "../../../context/MyContext";
import { Itk, TaskReference } from "../../../utils";

interface Props {
  currentItk: Itk;
  setNewTask: (task: TaskReference) => void;
  setLinkTask: (linkTask: boolean) => void;
  newTask: TaskReference;
  linkTask: boolean;
  defaultValue: number;
  register: any;
  errors: any;
}

export function InsertInfoTask({
  currentItk,
  setNewTask,
  setLinkTask,
  newTask,
  linkTask,
  defaultValue,
  register,
  errors,
}: Props) {
  const { taskList } = useMyContext();

  return (
    <Box>
      <Flex mb={2} align="center">
        <Text>ID de la tâche: </Text>
        <Text as="b" ml={10}>
          {currentItk.tasks.length + 1}
        </Text>
      </Flex>
      <Flex mb={2} align="center">
        <Text width={175}>Nom de la tâche: </Text>
        <Input
          placeholder="Nom de la tâche"
          {...register("title", { required: true })}
          onChange={(e) => setNewTask({ ...newTask, taskName: e.target.value })}
        />
      </Flex>
      {errors.title && (
        <Text color="red" ml={2}>
          {errors.title?.message as string}
        </Text>
      )}
      <Flex align="center" pt={2} pb={2}>
        <Text width="100%">Liaison à une tâche principale: </Text>
        <Switch size="lg" onChange={() => setLinkTask(!linkTask)} />
      </Flex>
      {!linkTask ? (
        <Select placeholder="Selectionnez une tâche">
          {taskList.map((task, index) => (
            <option key={`${index}`} value={task.id}>
              {task.title}
            </option>
          ))}
        </Select>
      ) : (
        <Select
          placeholder="Selectionnez une tâche"
          onChange={(e) =>
            setNewTask({
              ...newTask,
              hasOperationDistance: {
                ...newTask.hasOperationDistance,
                refersToOperation: {
                  id: e.target.value,
                  type: ''
                }
              },
            })
          }
        >
          {currentItk.tasks
            .filter(
              (task) =>
                !task.hasOperationDistance.distanceDuration.numericDuration
            )
            .map((task, index) => (
              <option key={`${index}`} value={task.taskId}>
                {task.taskName}
              </option>
            ))}
        </Select>
      )}
      <Flex mb={2} align="center" pt={2}>
        <Text width="25%">distance a l'implantation: </Text>
        <MyNumberInput
          defaultValue={defaultValue}
          onChange={(e) =>
            setNewTask({
              ...newTask,
              hasOperationDistance: {
                ...newTask.hasOperationDistance,
                distanceDuration: {
                  ...newTask.hasOperationDistance.distanceDuration,
                  numericDuration: e,
                },
              },
            })
          }
        />
        <Text ml={2}>Semaines</Text>
      </Flex>
    </Box>
  );
}
