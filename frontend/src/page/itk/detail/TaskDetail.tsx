import { Box, Divider, Flex, Spacer, Text } from "@chakra-ui/react";
import { MyBasicCard } from "../../../component/card";
import { Itk, getTaskDetails, taskDetailsProps } from "../../../utils";
import { AddTaskModal } from "./AddTaskModal";
import { UpdateTask } from "./UpdateTask";

interface Props {
  currentItk: Itk;
  isUpdated: boolean;
  setCurrentItk: (itk: Itk) => void;
}

export function TaskDetail({ currentItk, isUpdated, setCurrentItk }: Props) {
  const renderTaskTitle = (task: taskDetailsProps) => {
    return (
      <Flex align="center">
        <Text size="xl" as="b">
          {task.title}
        </Text>
        <Spacer />
        {isUpdated ? (
          <AddTaskModal
            currentItk={currentItk}
            setCurrentItk={setCurrentItk}
            title={task.title}
            defaultValue={task.defaultValue}
          />
        ) : (
          <Box w={25} h={25} />
        )}
      </Flex>
    );
  };

  const renderDisplayTask = (taskDetail: taskDetailsProps) => {
    return (
      <Text mt={2}>
        {taskDetail.filter(currentItk.tasks)
          .sort(
            (a, b) =>
              a.hasOperationDistance.distanceDuration.numericDuration -
              b.hasOperationDistance.distanceDuration.numericDuration
          )
          .map((task, index) => {
            const duration =
              task.hasOperationDistance.distanceDuration.numericDuration;
            return (
              <Flex key={`${index}`}>
                <Box w="40px">
                  <Text>{duration && duration > 0 ? `+${duration}` : (duration || 0)}s</Text>
                </Box>
                <Text>{task.taskName}</Text>
                <Spacer />
                <UpdateTask
                  currentItk={currentItk}
                  currentTask={task}
                  setCurrentItk={setCurrentItk}
                  isUpdate={isUpdated}
                />
              </Flex>
            );
          })}
      </Text>
    );
  };

  return (
    <Flex gap={5}>
      {getTaskDetails(currentItk).map((task, index) => (
        <Box flex={1} key={`${index}`}>
          <MyBasicCard display="flex" flexDirection="column" height="100%">
            <Box flex={1}>
              {renderTaskTitle(task)}
              <Divider />
              {renderDisplayTask(task)}
            </Box>
          </MyBasicCard>
        </Box>
      ))}
    </Flex>
  );
}
