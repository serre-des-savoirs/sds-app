import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Box, Flex, Heading, Text } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { MyBasicCard } from "../../../component/card/MyBasicCard";
import { MyBoxContent } from "../../../component/card/MyBoxContent";
import { UpdateSelectModal } from "../../../component/update/UpdateSelectModal";
import { useMyContext } from "../../../context/MyContext";
import { QUERY_GET_ALL_PLANTS } from "../../../graphql/itk";
import { Itk } from "../../../utils";
import { useFetch } from "../../../utils/hooks/useFetch";

interface Props {
  currentItk: Itk;
  setCurrentItk: (itk: Itk) => void;
  isUpdated: boolean;
}

export function CompatibilityDetail({
  currentItk,
  setCurrentItk,
  isUpdated,
}: Props) {
  const {
    values,
    error,
  }: {
    values: any;
    error: any;
  } = useFetch(import.meta.env.VITE_URL_QUERY, {
    query: QUERY_GET_ALL_PLANTS,
  });

  if (error) {
    console.log(error);
  }

  const plantOptions = values?.data?.getAllPlant?.map(
    (el: { id: string; plantName: string }) => ({
      value: el.id,
      label: el.plantName,
    })
  );

  const renderUpdatePlant = () => {
    if (!values || !values.data.getAllPlant) return <Box />;
    return (
      <Box>
        {isUpdated ? (
          <UpdateSelectModal
            currentObject={currentItk}
            setObject={setCurrentItk}
            modify="plantId"
            option={plantOptions}
          />
        ) : (
          <Box w={25} h={25} />
        )}
      </Box>
    );
  };

  // Todo remove renderUpdatePlant function, make it a component

  return (
    <MyBoxContent>
      <Heading size="md">Compatibilité</Heading>
      <MyBasicCard>
        <Flex>
          <Text>Plante associée: </Text>
          <AssociatedPlantLinks currentItk={currentItk} />
          {renderUpdatePlant()}
        </Flex>
      </MyBasicCard>
    </MyBoxContent>
  );
}

function AssociatedPlantLinks({ currentItk }: { currentItk: Itk }) {
  const { plantList } = useMyContext();
  return (
    <>
      {plantList.map((el, index) => {
        if (el.id === currentItk.plantId) {
          return (
            <Link to={`/plant/${el.id}`} key={`${index}`}>
              <Text as="b" ml={2}>
                {el.title}
              </Text>
              <ExternalLinkIcon mx="2px" />
            </Link>
          );
        }
        return null;
      })}
    </>
  );
}
