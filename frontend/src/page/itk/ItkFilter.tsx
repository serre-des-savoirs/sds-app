import {
  FormControl,
  FormLabel,
  Select as SimpleSelect,
} from "@chakra-ui/react";
import { Select, SingleValue } from "chakra-react-select";
import { useMyContext } from "../../context/MyContext";
import {
  ItkFilter,
  Option,
  cultureModeOptions,
  getPlantOptions,
  getRegionOptions,
  implantationModeOptions,
} from "../../utils";

interface Props {
  filter: (filter: ItkFilter) => void;
  valueFilter: ItkFilter;
}

export function FilterItk({ filter, valueFilter }: Props) {
  const { plantList, itkList } = useMyContext();

  return (
    <FormControl>
      <FormLabel>Plante</FormLabel>
      <Select
        useBasicStyles
        onChange={(e: SingleValue<Option>) =>
          filter({ ...valueFilter, plantId: e?.value || "" })
        }
        options={[
          { value: "", label: "Tous" },
          ...getPlantOptions(
            plantList.filter((el) =>
              itkList.map((itk) => itk.plantId).includes(el.id)
            )
          ),
        ]}
      />
      <FormLabel pt={4}>Mode de culture</FormLabel>
      <SimpleSelect
        placeholder="Tous"
        onChange={(e) =>
          filter({ ...valueFilter, technicalCulture: e.target.value })
        }
      >
        {cultureModeOptions.map((el) => (
          <option key={el.value} value={el.value}>
            {el.label}
          </option>
        ))}
      </SimpleSelect>
      {/* <FormLabel pt={4}>Mode d'implantation</FormLabel>
      <SimpleSelect
        placeholder="Tous"
        onChange={(e) =>
          filter({ ...valueFilter, hasImplantationMode: e.target.value })
        }
      >
        {implantationModeOptions.map((el) => (
          <option key={el.value} value={el.value}>
            {el.label}
          </option>
        ))}
      </SimpleSelect> */}
      <FormLabel pt={4}>Région</FormLabel>
      <SimpleSelect
        onChange={(e) => filter({ ...valueFilter, regions: e.target.value })}
      >
        {getRegionOptions(itkList).map((el) => (
          <option key={el.value} value={el.value}>
            {el.label}
          </option>
        ))}
      </SimpleSelect>
    </FormControl>
  );
}
