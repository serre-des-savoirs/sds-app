import { Box, Button, Flex, Text } from "@chakra-ui/react";
import { Select } from "chakra-react-select";
import {
  Controller,
  FieldValues,
  UseFormGetValues,
  UseFormHandleSubmit,
  useFieldArray,
} from "react-hook-form";
import { MyModal } from "../../component/display/MyModal";
import {
  MyError,
  MySelect,
  MyTextArea,
  MyTextInput,
} from "../../component/form";
import { useMyContext } from "../../context/MyContext";
import {
  Itk,
  cultureModeOptions,
  getPlantOptions,
  getTaskOptions,
} from "../../utils";
import { uiDesign } from "../../utils/styles";

interface Props {
  register: any;
  control: any;
  errors: any;
  onSubmit?: (el: any) => void;
  handleSubmit: UseFormHandleSubmit<FieldValues>;
  isOpen: boolean;
  onOpen: () => void;
  onClose: () => void;
  reset: () => void;
  setValue: any;
}

export function ItkModal({
  register,
  control,
  errors,
  handleSubmit,
  onSubmit,
  isOpen,
  onOpen,
  onClose,
  reset,
  setValue,
}: Props) {
  const { plantList, taskList } = useMyContext();
  const { remove, append, fields } = useFieldArray({
    control: control,
    name: "tasks",
  });

  return (
    <MyModal
      title="Création d'un ITK"
      handleSubmit={handleSubmit}
      onSubmit={onSubmit}
      isOpen={isOpen}
      onOpen={onOpen}
      onClose={onClose}
      reset={reset}
    >
      <Box>
        <Text>Titre: </Text>
        <MyTextInput
          label="Titre"
          name="title"
          register={register as UseFormGetValues<Itk>}
        />
        <MyError errors={errors["title"]} />
        <Text>Description: </Text>
        <MyTextArea
          name="description"
          register={register as UseFormGetValues<Itk>}
        />
        <MyError errors={errors["description"]} />
        <Text>Mode de culture: </Text>
        <MySelect
          placeholder="Choisir une option"
          name="technicalCulture"
          register={register}
          options={cultureModeOptions}
        />
        <MyError errors={errors["technicalCulture"]} />
        <Text>Plante:</Text>
        <Select
          placeholder="Choisir une option"
          name="plantId"
          options={getPlantOptions(plantList)}
          onChange={(e) => {
            setValue("plantId", e?.value);
          }}
        />
        <MyError errors={errors["plantId"]} />
        <Text>
          Tâche:
          <Button
            ml={1}
            size={"md"}
            colorScheme={uiDesign.icon.colorScheme}
            color={uiDesign.icon.textColor}
            onClick={() => append({ taskId: "" })}
          >
            +
          </Button>
        </Text>
        {fields.map((el, index) => (
          <Flex key={el.id}>
            <Controller
              control={control}
              name={`tasks.${index}.taskId`}
              render={({ field }) => (
                <Box flexGrow="1">
                  <Select
                    useBasicStyles
                    placeholder="Choisir une option"
                    onChange={(e: any) => field.onChange(e?.value)}
                    options={getTaskOptions(taskList)}
                  />
                  <MyError errors={errors["tasks.taskId"]} />
                </Box>
              )}
            />
            <Button onClick={() => remove(index)}>X</Button>
          </Flex>
        ))}
      </Box>
    </MyModal>
  );
}
