import { Box, Flex, Heading, Image, Text, VStack } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { MyFooter } from "../../component/layout/MyFooter";
import { NavBar } from "../../component/search/NavBar";

export function HomePage() {
  return (
    <>
      <Flex
        direction="column"
        align="center"
        minW="100%"
        flexGrow={1}
        bg="gray.100"
        p={8}
      >
        <VStack spacing={2} w="100%" maxW="1000px">
          <NavBar />
          <Box bg="white" p={8} borderRadius="xl" boxShadow="xl">
            <Flex gap="16" paddingRight="100px">
              <Image src="/logo-color.jpg" boxSize="150px" />
              <Box maxW="600px">
                <Heading size="2xl" mt={4} fontWeight="bold" color="gray.800">
                  Le premier guide de cultures agroécologiques
                </Heading>
                <Text mt={4} fontSize="xl" color="gray.600" textAlign="justify">
                  Trouvez des informations détaillées sur les plantes et leurs
                  itinéraires de cultures, les types variétaux recommandés et
                  les meilleures pratiques pour le maraîchage. Exploitez
                  l'intelligence collective en partageant et en apprenant des
                  autres agriculteurs et acteurs du secteur. Utilisez les
                  connaissances acquises pour prendre des décisions éclairées et
                  mettre en œuvre des pratiques durables sur votre territoire.
                </Text>
                <Text mt={4} fontSize="lg" color="gray.600" textAlign="justify">
                  Que vous soyez agriculteur, passionné de jardinage ou
                  simplement curieux, notre guide vous fournira les
                  connaissances nécessaires pour cultiver des aliments sains et
                  contribuer à la préservation de la planète.
                </Text>
              </Box>
            </Flex>
          </Box>
        </VStack>
      </Flex>
      <MyFooter>
        <Flex
          direction="column"
          width="100%"
          textAlign="center"
          alignItems="center"
        >
          <Text color="gray.600" fontSize="md">
            Développé avec passion par{" "}
            <Link to="https://elzeard.co/">Elzeard</Link> avec{" "}
            <Link to="https://maraichage.wixsite.com/mesclun">
              le collectif Mesclun
            </Link>
            .
          </Text>
          <Text color="gray.600" fontSize="md">
            Merci à nos financeurs de permettre le développement de ce service.
          </Text>
          <Flex alignItems="center" gap={8}>
            <InvestorCard
              src="images/logo-bourgogne.png"
              name="Région Bourgogne-Franche-Comté"
            />
            <InvestorCard
              src="images/logo-carasso.png"
              name="Fondation Carrasso"
            />
            <InvestorCard
              src="images/logo-aquitaine.png"
              name="Région Nouvelle-Aquitaine"
            />
            <InvestorCard
              src="images/logo-bpi.png"
              name="BPI France"
            />
          </Flex>
        </Flex>
      </MyFooter>
    </>
  );
}

interface InvestorCardProps {
  src: string;
  name: string;
}

const InvestorCard = ({ src, name }: InvestorCardProps) => {
  return (
    <Flex direction="column" alignItems="center">
      <Image src={src} height="100px" />
      <Text>{name}</Text>
    </Flex>
  );
};
