import { Flex } from "@chakra-ui/react";
import { ReactNode } from "react";
import { Header } from "./Header";

interface Props {
  children: ReactNode;
}

export function MainLayout({ children }: Props) {
  return (
    <Flex direction="column" alignItems="center" height="100vh" m={"0 auto"}>
      <Header />
      {children}
    </Flex>
  );
}
