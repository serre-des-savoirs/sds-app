import { Container, ContainerProps, Flex } from "@chakra-ui/react";
import { ReactNode } from "react";
import { uiDesign } from "../../utils/styles";

interface Props {
  children: ReactNode;
}

export function MyFooter({ children, ...props }: Props & ContainerProps) {
  return (
    <Container
      maxW="100%"
      mx="auto"
      p={2}
      borderWidth="1px"
      borderStyle="solid"
      borderColor={uiDesign.footer.borderColor}
      {...props}
    >
      <Flex alignItems="center" justifyContent="flex-end">
        {children}
      </Flex>
    </Container>
  );
}
