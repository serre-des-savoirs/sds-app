import { Box } from "@chakra-ui/react";
import { ReactNode } from "react";
import { uiDesign } from "../../utils/styles";

interface Props {
  children: ReactNode;
}

export function PageLayout({ children }: Props) {
  return (
    <Box
      width="100%"
      height="0"
      flexGrow="1"
      mt="2rem"
      p={5}
      borderWidth="1px"
      borderStyle="solid"
      borderColor={uiDesign.body.borderColor}
    >
      {children}
    </Box>
  );
}
