import {
  Avatar,
  AvatarBadge,
  Box,
  Button,
  Flex,
  Heading,
  Image,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Spacer,
} from "@chakra-ui/react";
import { NavLink, useNavigate } from "react-router-dom";
import { uiDesign } from "../../utils/styles";

export function Header() {
  const navigate = useNavigate();
  const token = localStorage.getItem("token") || "";

  const signOut = (token: string) => {
    fetch(import.meta.env.VITE_URL_QUERY, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        query: `
          query {
            signOut
          }
        `,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.errors) {
          throw new Error(res.errors[0].message);
        }
        localStorage.removeItem("token");
        navigate("/login");
      })
      .catch((err) => console.error(err));
  };

  return (
    <Flex boxShadow="md" py={4} px={6} alignItems="center" width="100%" gap={8}>
      {/* <CheckToken /> */}
      <Flex
        onClick={() => navigate("/")}
        gap={2}
        alignItems="center"
        className="clickable"
      >
        <Image src="/logo-color.jpg" boxSize="40px" />
        <Heading size="lg" color={uiDesign.title.textColor}>
          Serre des Savoirs
        </Heading>
      </Flex>
      <Flex gap="20px">
        {/* <NavLink to="/task" className="navigation-item">
            <Box color={uiDesign.title.textColor}>Tâches</Box>
          </NavLink> */}
        <NavLink to="/plant" className="navigation-item">
          <Box color={uiDesign.title.textColor}>Plantes</Box>
        </NavLink>
        <NavLink to="/itk" className="navigation-item">
          <Box color={uiDesign.title.textColor}>ITKs</Box>
        </NavLink>

      </Flex>

      <Spacer />
      <Menu>
        <MenuButton as={Button} rounded="full" variant="link" cursor="pointer">
          <Avatar>
            <AvatarBadge
              boxSize="1.25em"
              bg={localStorage.getItem("token") ? "green.500" : "red.500"}
            />
          </Avatar>
        </MenuButton>
        <MenuList>
          <MenuItem onClick={() => navigate("/login")}>Se connecter</MenuItem>
          <MenuItem onClick={() => signOut(token)}>Se déconnecter</MenuItem>
        </MenuList>
      </Menu>
    </Flex>
  );
}
