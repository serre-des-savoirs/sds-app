import { Box, Text, useDisclosure } from "@chakra-ui/react";
import { zodResolver } from "@hookform/resolvers/zod";
import { Select } from "chakra-react-select";
import { useForm } from "react-hook-form";
import { BiPencil } from "react-icons/bi";
import { z } from "zod";
import { useItk } from "../../context/useItk";
import {
  MUTATION_CULTURE_MODE,
  MUTATION_FARM_EQUIPMENT,
  MUTATION_IMPLANTATION_MODE,
  MUTATION_IRRIGATION_MODE,
  MUTATION_ITK_REGION,
  MUTATION_TYPE_SOIL,
  MUTATION_UPDATE_CLIMATE,
  MUTATION_UPDATE_PLANT,
  MUTATION_UPDATE_SEASON,
} from "../../graphql/itk";
import {
  Itk,
  Option,
  climateOptions,
  farmEquipmentOptions,
  implantationModeOptions,
  irrigationModeOptions,
  seasonOptions,
  typeSoilOptions,
} from "../../utils";
import { MyModal } from "../display";

const validationSchema = z.object({
  select: z.array(z.string()),
});

type ValidationSchema = z.infer<typeof validationSchema>;

interface Props {
  currentObject: Itk;
  setObject: (itk: Itk) => void;
  modify:
    | "region"
    | "plantId"
    | "climate"
    | "season"
    | "technicalCulture"
    | "implantationMode"
    | "typeSoil"
    | "irrigationMode"
    | "farmEquipment";
  option: Option[];
  isMulti?: boolean;
}

export function UpdateSelectModal({
  currentObject,
  setObject,
  modify,
  option,
  isMulti,
}: Props) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { updateItk } = useItk();

  const { handleSubmit, reset, setValue } = useForm({
    resolver: zodResolver(validationSchema),
  });

  const getSeason = (seasons: string[]): string[] => {
    return seasonOptions
      .filter((el) => seasons.includes(el.value))
      .map((el) => el.label) as string[];
  };

  const handleUpdate = async (el: ValidationSchema): Promise<void> => {
    switch (modify) {
      case "region":
        setObject({
          ...currentObject,
          dependsOnSoilClimateContext: {
            ...currentObject.dependsOnSoilClimateContext,
            belongsToRegion: el.select,
          },
        });
        await fetch(import.meta.env.VITE_URL_QUERY, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${localStorage.getItem("token") || null}`,
          },
          body: JSON.stringify({
            query: MUTATION_ITK_REGION(currentObject.id, el.select),
          }),
        });
        updateItk({
          ...currentObject,
          dependsOnSoilClimateContext: {
            ...currentObject.dependsOnSoilClimateContext,
            belongsToRegion: el.select,
          },
        });
        return;
      case "plantId":
        setObject({
          ...currentObject,
          plantId: el.select[0],
        });
        await fetch(import.meta.env.VITE_URL_QUERY, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${localStorage.getItem("token") || null}`,
          },
          body: JSON.stringify({
            query: MUTATION_UPDATE_PLANT(currentObject.id, el.select[0]),
          }),
        });
        updateItk({
          ...currentObject,
          plantId: el.select[0],
        });
        return;
      case "climate":
        setObject({
          ...currentObject,
          dependsOnSoilClimateContext: {
            ...currentObject.dependsOnSoilClimateContext,
            hasClimateContext: climateOptions.filter(
              (e) => e.value === el.select[0]
            )[0].label,
          },
        });
        await fetch(import.meta.env.VITE_URL_QUERY, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${localStorage.getItem("token") || null}`,
          },
          body: JSON.stringify({
            query: MUTATION_UPDATE_CLIMATE(currentObject.id, el.select[0]),
          }),
        });
        updateItk({
          ...currentObject,
          dependsOnSoilClimateContext: {
            ...currentObject.dependsOnSoilClimateContext,
            hasClimateContext: climateOptions.filter(
              (e) => e.value === el.select[0]
            )[0].label,
          },
        });
        return;
      case "season": {
        const seasons = getSeason(el.select);
        setObject({
          ...currentObject,
          season: seasons,
        });
        await fetch(import.meta.env.VITE_URL_QUERY, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${localStorage.getItem("token") || null}`,
          },
          body: JSON.stringify({
            query: MUTATION_UPDATE_SEASON(currentObject.id, el.select),
          }),
        });
        updateItk({
          ...currentObject,
          season: seasons,
        });
        return;
      }
      case "technicalCulture":
        setObject({
          ...currentObject,
          technicalCulture: el.select[0],
        });
        await fetch(import.meta.env.VITE_URL_QUERY, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${localStorage.getItem("token") || null}`,
          },
          body: JSON.stringify({
            query: MUTATION_CULTURE_MODE(currentObject.id, el.select[0]),
          }),
        });
        updateItk({
          ...currentObject,
          technicalCulture: el.select[0],
        });
        return;
      case "implantationMode":
        setObject({
          ...currentObject,
          hasImplantationMode: implantationModeOptions.filter(
            (e) => e.value === el.select[0]
          )[0].label,
        });
        await fetch(import.meta.env.VITE_URL_QUERY, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${localStorage.getItem("token") || null}`,
          },
          body: JSON.stringify({
            query: MUTATION_IMPLANTATION_MODE(currentObject.id, el.select[0]),
          }),
        });
        updateItk({
          ...currentObject,
          hasImplantationMode: el.select[0],
        });
        return;
      // TODO : update typeSoil with nodes not exists
      case "typeSoil": {
        const res_typeSoil = await fetch(import.meta.env.VITE_URL_QUERY, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${localStorage.getItem("token") || null}`,
          },
          body: JSON.stringify({
            query: MUTATION_TYPE_SOIL(currentObject.id, el.select[0]),
          }),
        });
        const response = await res_typeSoil.json();
        if (response.data.updateTypeSoil !== "error") {
          setObject({
            ...currentObject,
            typeSoil: typeSoilOptions.filter((e) => e.value === el.select[0])[0]
              .label,
          });
        }
        return;
      }
      // TODO : update irrigationMode with nodes not exists
      case "irrigationMode": {
        const res_irrigationMode = await fetch(import.meta.env.VITE_URL_QUERY, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${localStorage.getItem("token") || null}`,
          },
          body: JSON.stringify({
            query: MUTATION_IRRIGATION_MODE(currentObject.id, el.select[0]),
          }),
        });
        const response_irrigationMode = await res_irrigationMode.json();
        if (response_irrigationMode.data.updateIrrigationMode !== "error") {
          setObject({
            ...currentObject,
            hasIrrigationMode: irrigationModeOptions.filter(
              (e) => e.value === el.select[0]
            )[0].label,
          });
        }
        return;
      }
      // TODO : update farmEquipment with nodes not exists
      case "farmEquipment": {
        const res_farmEquipment = await fetch(import.meta.env.VITE_URL_QUERY, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${localStorage.getItem("token") || null}`,
          },
          body: JSON.stringify({
            query: MUTATION_FARM_EQUIPMENT(currentObject.id, el.select[0]),
          }),
        });
        const response_farmEquipment = await res_farmEquipment.json();
        if (response_farmEquipment.data.updateFarmEquipment !== "error") {
          setObject({
            ...currentObject,
            hasFarmEquipment: farmEquipmentOptions.filter(
              (e) => e.value === el.select[0]
            )[0].label,
          });
        }
        return;
      }
      default:
        return;
    }
  };

  const onSubmit = async (el: ValidationSchema) => {
    await handleUpdate(el);
    onClose();
  };

  return (
    <Box>
      <BiPencil size={25} onClick={onOpen} className="update-field" />
      <MyModal
        title="Modifier un champ"
        isOpen={isOpen}
        onClose={onClose}
        onOpen={onOpen}
        handleSubmit={handleSubmit}
        onSubmit={onSubmit}
        reset={reset}
      >
        <Text pb={10}>Id: {currentObject.id}</Text>
        <Text>Selectionner une nouvelle valeur:</Text>
        <Select
          useBasicStyles
          options={option}
          placeholder="Selectionner une valeur"
          closeMenuOnSelect={false}
          isMulti={isMulti}
          onChange={(e) =>
            setValue(
              "select",
              Array.isArray(e)
                ? e.map((el) => el.value)
                : [(e as { value: string; label: string }).value]
            )
          }
        />
      </MyModal>
    </Box>
  );
}
