import { Box, Text, useDisclosure } from "@chakra-ui/react";
import { zodResolver } from "@hookform/resolvers/zod";
import { UseFormGetValues, useForm } from "react-hook-form";
import { BiPencil } from "react-icons/bi";
import { z } from "zod";
import { MyModal } from "../display/MyModal";
import { MyError } from "../form/MyError";
import { MyTextInput } from "../form/MyTextInput";
import { QUERY_UPDATE_ITK_TITLE } from "../../graphql/itk";
import "../../index.css";

const validationSchema = z.object({
  title: z.string().min(1, "trop court").max(32, "Trop long"),
});

type ValidationSchema = z.infer<typeof validationSchema>;

interface Props {
  currentObject: any;
  onChangeObject: <T>(object: T) => void;
}

export function UpateTitleModal({ currentObject, onChangeObject }: Props) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(validationSchema),
    defaultValues: currentObject,
  });

  const onSubmit = async (el: ValidationSchema) => {
    await fetch(import.meta.env.VITE_URL_QUERY, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        query: QUERY_UPDATE_ITK_TITLE(currentObject.id, el.title, "fr"),
      }),
    });
    onChangeObject({ ...currentObject, title: el.title });
    onClose();
  };

  return (
    <Box>
      <BiPencil size={25} onClick={onOpen} className="update-field" />
      <MyModal
        title="Modifier le titre"
        isOpen={isOpen}
        onClose={onClose}
        onOpen={onOpen}
        handleSubmit={handleSubmit}
        onSubmit={onSubmit}
        reset={reset}
      >
        <Text pb={10}>Id: {currentObject.id}</Text>
        <Text>Titre :</Text>
        <MyTextInput
          label="Title"
          register={register as UseFormGetValues<ValidationSchema>}
          name="title"
        />
        <MyError errors={errors["title"]} />
      </MyModal>
    </Box>
  );
}
