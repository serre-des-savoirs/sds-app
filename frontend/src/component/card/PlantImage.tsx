import { Image } from "@chakra-ui/react";
import { plantPictureFallback, plantPictureUrl } from "../../utils/plantPicture";


interface PlantImageProps {
  plantId: string;
  style?: Record<string, any>
}

export function PlantImage({ plantId, style }: PlantImageProps) {
  return (
    <Image
      src={plantPictureUrl(plantId)}
      onError={(e) => e.currentTarget.src=plantPictureFallback}
      style={{
        width: 60,
        height: 60,
        borderRadius: 50,
        overflow: 'hidden',
        alignSelf: 'center',
        ...style
      }}
    />
  )
}