import { Box, Flex, Text, Wrap } from "@chakra-ui/react";
import { ReactNode } from "react";
import { MySpacer } from "../MySpacer";
import { MyBasicCard } from "./MyBasicCard";
import { uiDesign } from "../../utils/styles";

interface Props {
  children?: ReactNode;
  title: string;
  content: string[];
}

export function MyCardWithBoxContent({ title, content, children }: Props) {
  return (
    <MyBasicCard>
      <Flex>
        {title}
        <MySpacer w={4} />
        {children}
      </Flex>
      <Wrap spacing={4}>
        {content.map((item, index) => (
          <Box
            key={index}
            borderRadius="md"
            m={2}
            p={2}
            bgColor={uiDesign.boxCardContent.backgroundColor}
          >
            <Text>{item}</Text>
          </Box>
        ))}
      </Wrap>
    </MyBasicCard>
  );
}
