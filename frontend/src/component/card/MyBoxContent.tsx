import { Box } from "@chakra-ui/react";
import { ReactNode } from "react";

interface Props {
  children: ReactNode;
}

export function MyBoxContent({ children }: Props) {
  return (
    <Box borderWidth="1px" borderRadius="lg" bg="white" p={5}>
      {children}
    </Box>
  );
}
