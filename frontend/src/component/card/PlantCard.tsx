import { Flex, Text } from "@chakra-ui/react";
import { Plant } from "../../utils/types";
import { MyCard } from "./MyCard";
import { PlantImage } from "./PlantImage";

interface Props {
  plant: Plant;
  path: string;
}

export function PlantCard({ plant, path }: Props) {
  return (
    <MyCard path={path}>
      <Flex flexDirection={"row"} gap={4} alignItems={"center"}>
        <PlantImage plantId={plant.id}/>
        <Text fontWeight="bold" fontSize="xl">
          {plant.title}
        </Text>
      </Flex>
    </MyCard>
  );
}
