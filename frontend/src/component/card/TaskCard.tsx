import { Text } from "@chakra-ui/react";
import { Task } from "../../utils";
import { MyCard } from "./MyCard";

interface Props {
  task: Task;
  path: string;
}

export function TaskCard({ task, path }: Props) {
  return (
    <MyCard path={path}>
      <Text fontWeight="bold" fontSize="xl">
        {task.title}
      </Text>
    </MyCard>
  );
}
