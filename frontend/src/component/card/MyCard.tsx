import { Box } from "@chakra-ui/react";
import { ReactNode } from "react";
import { useNavigate } from "react-router-dom";
import { uiDesign } from "../../utils/styles";

interface Props {
  children: ReactNode;
  path?: string;
}

export function MyCard({ children, path }: Props) {
  const navigate = useNavigate();
  return (
    <Box
      bg={uiDesign.card.backgroundColor}
      width="100%"
      borderRadius="md"
      boxShadow="md"
      p={4}
      onClick={() => (path ? navigate(path) : null)}
      transition="background-color 0.2s ease"
      _hover={{
        backgroundColor: "gray.100",
        boxShadow: "md",
      }}
      cursor="pointer"
    >
      {children}
    </Box>
  );
}
