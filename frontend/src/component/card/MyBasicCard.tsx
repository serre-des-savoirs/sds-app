import { Box, ResponsiveValue } from "@chakra-ui/react";
import { ReactNode } from "react";
import { uiDesign } from "../../utils/styles";

interface Props {
  children: ReactNode;
  display?: string;
  flexDirection?: ResponsiveValue<any>;
  height?: string | number;
}

export function MyBasicCard({
  children,
  display,
  flexDirection,
  height,
}: Props) {
  return (
    <Box
      bg={uiDesign.basicCard.backgroundColor}
      borderRadius="md"
      boxShadow="md"
      p={4}
      mt={2}
      m={2}
      display={display}
      flexDirection={flexDirection}
      height={height}
    >
      {children}
    </Box>
  );
}
