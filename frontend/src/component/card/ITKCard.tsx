import {
  Badge,
  Box,
  Flex,
  Spacer,
  Text,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import { AiFillQuestionCircle } from "react-icons/ai";
import { CiCloudSun } from "react-icons/ci";
import { GiGreenhouse, GiHabitatDome } from "react-icons/gi";
import { Itk } from "../../utils/types";
import { MyCard } from "./MyCard";
import {
  EMPTY_RESULT,
  cultureModeOptions,
  implantationModeOptions,
  regionOptions,
} from "../../utils";
import { ellipse } from "../../utils/ellipse";
import { PlantImage } from "./PlantImage";

interface Props {
  itk: Itk;
}

type Icon = {
  [key: string]: JSX.Element;
};

const ICONS_BY_TECH_CULTURE: Icon = {
  "Sous abri": <GiGreenhouse size={30} />,
  "Plein champ": <CiCloudSun size={30} />,
  // "serre chauffé": <GiHabitatDome size={30} />,
};

export function ITKCard({ itk }: Props) {
  const getIcon = ICONS_BY_TECH_CULTURE[
    cultureModeOptions.find((el) => el.value === itk.technicalCulture)
      ?.label as string
  ] || <AiFillQuestionCircle size={30} />;
 
  const itkDetailsLink = `/itk/${itk.id.slice(Math.max(itk.id.indexOf('/')+1))}`
  
  return (
    <MyCard path={itkDetailsLink}>
      <Flex alignSelf={"center"} gap={4}>
        <PlantImage
          plantId={itk.plantId}
          style={{
            width: 30,
            height: 30,
          }}
        />
        <Box w="40px" h="40px" alignContent={"center"}>
          {getIcon}
        </Box>
        <Spacer />
        <Text alignContent={"center"} fontSize="sm" color="gray.500">
          {implantationModeOptions.find(
            (el) => el.value === itk.hasImplantationMode
          )?.label || EMPTY_RESULT}
        </Text>
      </Flex>
      <Text fontWeight="bold" fontSize="xl">
        {itk.title}
      </Text>
      <Wrap spacing={4} mt={6}>
        {ellipse(
          3,
          regionOptions
            .filter((el) =>
              itk.dependsOnSoilClimateContext?.belongsToRegion?.includes(
                el.value
              )
            )
            .map((el) => el.label) as string[]
        ).map((region) => (
          <WrapItem key={region}>
            <Badge>{region}</Badge>
          </WrapItem>
        ))}
      </Wrap>
    </MyCard>
  );
}
