import { Box, Flex, Heading, Text } from "@chakra-ui/react";
import { MyBasicCard } from "./MyBasicCard";

interface Props {
  title: string;
  value: string;
  unit: string;
}

export function MyCardSmallInformation({ title, value, unit }: Props) {
  return (
    <MyBasicCard>
      <Text size="md">{title}</Text>
      <Box>
        <Flex>
          <Heading size="lg">{value}</Heading>
          <Text ml={1}>{value ? unit : ""}</Text>
        </Flex>
      </Box>
    </MyBasicCard>
  );
}
