import { Box } from "@chakra-ui/react";

interface Props {
  h?: number;
  w?: number;
}

export function MySpacer({ h, w }: Props) {
  return <Box h={h} w={w} />;
}
