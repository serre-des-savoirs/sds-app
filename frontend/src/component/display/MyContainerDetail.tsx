import { Container } from "@chakra-ui/react";
import { ReactNode } from "react";
import { uiDesign } from "../../utils/styles";

interface Props {
  children: ReactNode;
}

export function MyContainerDetail({ children }: Props) {
  return (
    <Container
      minW="800px"
      maxW="100%"
      mx="auto"
      height="83vh"
      mt="2rem"
      p={4}
      borderWidth="1px"
      borderStyle="solid"
      borderColor={uiDesign.body.borderColor}
      overflow={"auto"}
    >
      {children}
    </Container>
  );
}
