import {
  Button,
  Flex,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Spacer,
} from "@chakra-ui/react";
import { ReactNode } from "react";
import {
  FieldValues,
  SubmitHandler,
  UseFormHandleSubmit,
} from "react-hook-form";
import { uiDesign } from "../../utils/styles";

interface Props {
  children: ReactNode;
  title?: string;
  onSubmit?: (el: any) => void;
  handleSubmit?: UseFormHandleSubmit<FieldValues>;
  isOpen: boolean;
  onOpen: () => void;
  onClose: () => void;
  reset: () => void;
}

export function MyModal({
  children,
  title,
  handleSubmit,
  onSubmit,
  isOpen,
  onOpen,
  onClose,
  reset,
}: Props) {
  const modalOverlay = () => (
    <ModalOverlay
      bg="blackAlpha.300"
      backdropFilter="blur(10px) hue-rotate(90deg)"
    />
  );
  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} size="3xl">
        {modalOverlay()}
        <form
          onSubmit={
            handleSubmit && handleSubmit(onSubmit as SubmitHandler<FieldValues>)
          }
        >
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>{title}</ModalHeader>
            <ModalCloseButton />
            <ModalBody>{children}</ModalBody>
            <ModalFooter>
              <Flex>
                <Button
                  colorScheme={uiDesign.buttonDeleteCancel.colorScheme}
                  mr={3}
                  onClick={() => (onClose(), reset())}
                >
                  Fermer
                </Button>
                {handleSubmit && (
                  <Button
                    colorScheme={uiDesign.buttonAdd.colorScheme}
                    mr={3}
                    type="submit"
                  >
                    Envoyer
                  </Button>
                )}
                <Spacer />
              </Flex>
            </ModalFooter>
          </ModalContent>
        </form>
      </Modal>
    </>
  );
}
