import { Box, Container, Flex, Heading, Stack, Text } from "@chakra-ui/react";
import { ReactNode } from "react";

interface Props {
  description: string;
  title: string;
  icon: ReactNode;
  flexDirection?: "row" | "row-reverse";
}

export function MyContainer({
  description,
  title,
  icon,
  flexDirection,
}: Props) {
  return (
    <Stack mt="2rem" pt={8}>
      <Box p={5}>
        <Flex flexDirection={flexDirection}>
          {icon}
          <Container minW={"80%"}>
            <Heading>{title}</Heading>
            <Text>{description}</Text>
          </Container>
        </Flex>
      </Box>
    </Stack>
  );
}
