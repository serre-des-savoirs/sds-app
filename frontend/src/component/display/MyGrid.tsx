import { Box, Flex, SimpleGrid, Text } from "@chakra-ui/react";
import { Children, ReactNode } from "react";

interface Props {
  children: ReactNode;
  columns?: number;
}

export function MyGrid({ children, columns = 1 }: Props) {
  const count = Children.count(children);

  const lengthDisplay = count
    ? `${count} résultat${count > 1 ? "s" : ""}`
    : "Aucun résultat";

  return (
    <Flex direction="column" height="100%">
      <Text>{lengthDisplay}</Text>

      <Box height={0} flexGrow={1} overflow={"auto"}>
        <SimpleGrid className="hide-scroll" columns={columns} p={4} spacing={3}>
          {children}
        </SimpleGrid>
      </Box>
    </Flex>
  );
}
