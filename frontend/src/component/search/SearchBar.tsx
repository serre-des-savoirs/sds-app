import { Input } from "@chakra-ui/react";

interface Props {
  setSearch: (itk: string) => void;
  search?: string;
}

export function SearchBar({ setSearch, search }: Props) {
  return (
    <Input
      placeholder="Recherche"
      variant="flushed"
      mb={4}
      onChange={(e) => setSearch(e.target.value)}
      value={search}
    />
  );
}
