import { SearchIcon } from "@chakra-ui/icons";
import { Box, Input, InputGroup, InputLeftElement } from "@chakra-ui/react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export function NavBar() {
  const [search, setSearch] = useState("");
  const navigate = useNavigate();

  const handleSearch = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter") {
      navigate({ pathname: "/plant", search: `?search=${search}` });
    }
  };

  return (
    <Box w="100%">
      <InputGroup size="lg">
        <InputLeftElement color="gray.500" fontSize="1.2em">
          <SearchIcon />
        </InputLeftElement>
        <Input
          type="text"
          placeholder="Recherche d'une plante"
          borderColor="transparent"
          borderRadius="xl"
          color="black"
          bg="white"
          _hover={{ borderColor: "gray.300" }}
          _placeholder={{ color: "gray.600" }}
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          onKeyDown={(e) => handleSearch(e)}
        />
      </InputGroup>
    </Box>
  );
}
