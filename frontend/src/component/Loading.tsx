import { Box, Center } from "@chakra-ui/react";
import { useEffect, useState } from "react";

export function Loading() {
  const [dots, setDots] = useState<string>("... ");

  useEffect(() => {
    const interval = setInterval(() => {
      setDots((dots) => {
        return `${dots[3]}${dots[0]}${dots[1]}${dots[2]}`;
      });
    }, 500);
    return () => clearInterval(interval);
  }, []);
  return (
    <Center height="100%">
      <Box width="200px" textAlign="left">{`Chargement${dots}`}</Box>
    </Center>
  );
}
