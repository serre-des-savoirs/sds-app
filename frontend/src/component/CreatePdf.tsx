import { Button } from "@chakra-ui/react";
import pdfMake from "pdfmake/build/pdfmake";
import { TDocumentDefinitions } from "pdfmake/interfaces";
import { uiDesign } from "../utils/styles";

const fonts = {
  Roboto: {
    normal:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Regular.ttf",
    bold: "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Medium.ttf",
    italics:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Italic.ttf",
    bolditalics:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-MediumItalic.ttf",
  },
};
interface Props {
  documentContent: TDocumentDefinitions;
}

export function CreatePdf({ documentContent }: Props) {
  return (
    <Button
      colorScheme={uiDesign.export.colorScheme}
      variant={"outline"}
      mr={2}
      onClick={() =>
        pdfMake.createPdf(documentContent, undefined, fonts).open()
      }
    >
      Export pdf
    </Button>
  );
}
