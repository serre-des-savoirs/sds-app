import { Select } from "@chakra-ui/react";
import { FieldValues, UseFormRegister } from "react-hook-form";
import { Option } from "../../utils/types";

interface MySelectProps {
  register: UseFormRegister<FieldValues>;
  name: string;
  options: Option[];
  placeholder?: string;
}

export function MySelect({
  register,
  name,
  options,
  placeholder,
}: MySelectProps) {
  return (
    <Select {...register(name, { required: true })} placeholder={placeholder}>
      {options.map((option) => (
        <option key={option.value} value={option.value}>
          {option.label}
        </option>
      ))}
    </Select>
  );
}
