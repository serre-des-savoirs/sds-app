import { Input } from "@chakra-ui/react";
import { FieldValues, UseFormRegister } from "react-hook-form";

interface MyDateProps {
  register: UseFormRegister<FieldValues>;
}

export function MyDate({ register }: MyDateProps) {
  return <Input type="date" {...register("date", { required: true })} />;
}
