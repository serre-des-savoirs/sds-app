import React from "react";
import {
  RangeSlider,
  RangeSliderTrack,
  RangeSliderFilledTrack,
  RangeSliderThumb,
} from "@chakra-ui/react";

const MyRangeSlider = ({
  register,
  errors,
  name,
  label,
  min,
  max,
  step,
}: any) => {
  const [value, setValue] = React.useState([0, 0]);
  return (
    <>
      <RangeSlider
        aria-label={label}
        colorScheme="teal"
        defaultValue={[30, 70]}
        min={min}
        max={max}
        step={step}
        {...register(name, { required: true, valueAsNumber: true })}
        aria-invalid={errors[name] ? true : false}
        onChange={(e) => {
          setValue(e);
        }}
      >
        <RangeSliderTrack>
          <RangeSliderFilledTrack />
        </RangeSliderTrack>
        <RangeSliderThumb index={0} />
        <RangeSliderThumb index={1} />
      </RangeSlider>
      {errors[name] && <p className="errorField">{errors[name].message}</p>}
    </>
  );
};

export default MyRangeSlider;
