import { Input } from "@chakra-ui/react";
import { FieldValues, UseFormRegister } from "react-hook-form";

interface MyTextProps {
  register: UseFormRegister<FieldValues>;
  name: string;
  label?: string;
  type?: string;
}

export function MyTextInput({
  register,
  name,
  label,
  type = "text",
}: MyTextProps) {
  return <Input type={type} {...register(name)} placeholder={label} />;
}
