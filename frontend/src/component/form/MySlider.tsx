import React from "react";
import {
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  SliderMark,
} from "@chakra-ui/react";

const MySlider = ({ register, errors, name, min, max, step, label }: any) => {
  const [value, setValue] = React.useState(0);

  return (
    <>
      <Slider
        aria-label={label}
        colorScheme="teal"
        min={min}
        max={max}
        defaultValue={50}
        step={step}
        value={value}
        {...register(name, { required: true, valueAsNumber: true })}
        aria-invalid={errors[name] ? true : false}
        onChange={(e) => {
          setValue(e);
        }}
      >
        <SliderTrack>
          <SliderFilledTrack />
        </SliderTrack>
        <SliderThumb />
      </Slider>
      {errors[name] && <p className="errorField">{errors[name].message}</p>}
    </>
  );
};

export default MySlider;
