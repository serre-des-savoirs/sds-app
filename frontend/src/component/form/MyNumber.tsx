import {
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from "@chakra-ui/react";
import { FieldValues, UseFormRegister } from "react-hook-form";
import { useState } from "react";

interface MyNumberProps {
  register: any;
  min?: number;
  max?: number;
  step?: number;
}

export function MyNumber({
  register,
  max = 100,
  min = 0,
  step = 1,
}: MyNumberProps) {
  const [value, setValue] = useState(0);

  return (
    <NumberInput
      min={min}
      max={max}
      step={step}
      value={value}
      clampValueOnBlur={false}
      {...register("number", { required: true, valueAsNumber: true })}
      onChange={(e) => {
        setValue(Number(e));
      }}
    >
      <NumberInputField />
      <NumberInputStepper>
        <NumberIncrementStepper />
        <NumberDecrementStepper />
      </NumberInputStepper>
    </NumberInput>
  );
}
