import { FieldError, FieldErrorsImpl, Merge } from "react-hook-form";
import { Text } from "@chakra-ui/react";

interface MyErrorProps {
  errors: FieldError | Merge<FieldError, FieldErrorsImpl> | undefined;
}

export function MyError({ errors }: MyErrorProps) {
  return <>{errors && <Text color="red">{errors?.message as string}</Text>}</>;
}
