import { Checkbox } from "@chakra-ui/react";
import { FieldValues, UseFormRegister } from "react-hook-form";

import { Option } from "../../utils/types";

interface MyCheckBoxProps {
  register: UseFormRegister<FieldValues>;
  options: Option[];
}

export function MyCheckBox({ register, options }: MyCheckBoxProps) {
  return (
    <>
      {options.map((option) => (
        <Checkbox
          key={option.value.split(" ")[1]}
          value={option.value}
          {...register("checkBox")}
        >
          {option.value}
        </Checkbox>
      ))}
    </>
  );
}
