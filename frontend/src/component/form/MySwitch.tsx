import { Switch } from "@chakra-ui/react";
import { FieldValues, UseFormRegister } from "react-hook-form";

interface MySwitchProps {
  register: UseFormRegister<FieldValues>;
  name: string;
}

export function MySwitch({ register, name }: MySwitchProps) {
  return <Switch {...register(name)} />;
}
