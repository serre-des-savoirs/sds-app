import { PinInput, PinInputField } from "@chakra-ui/react";
import { FieldValues, UseFormRegister } from "react-hook-form";

interface MyPinInputProps {
  register: UseFormRegister<FieldValues>;
  index: number;
}

export function MyPinInput({ register, index }: MyPinInputProps) {
  return (
    <PinInput size={"lg"} otp>
      {Array.from(Array(index).keys()).map((i) => (
        <PinInputField
          key={i}
          {...register(`pin[${i}]`, { required: true, valueAsNumber: true })}
        />
      ))}
    </PinInput>
  );
}
