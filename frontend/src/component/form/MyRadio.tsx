import { Radio, RadioGroup } from "@chakra-ui/react";
import { FieldValues, UseFormRegister } from "react-hook-form";
import { Option } from "../../utils/types";

interface MyRadioProps {
  register: UseFormRegister<FieldValues>;
  name: string;
  options: Option[];
}

const MyRadio = ({ register, name, options }: MyRadioProps) => {
  return (
    <RadioGroup>
      {options.map((option: Option) => (
        <Radio
          key={option.value}
          value={option.value}
          {...register(name, { required: true })}
        >
          {option.label}
        </Radio>
      ))}
    </RadioGroup>
  );
};

export default MyRadio;
