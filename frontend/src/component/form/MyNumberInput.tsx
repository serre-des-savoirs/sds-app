import {
  Box,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";

interface Props {
  defaultValue?: number;
  value?: number;
  min?: number;
  max?: number;
  step?: number;
  onChange?: (value: any) => void;
}

export function MyNumberInput({
  defaultValue,
  value,
  min,
  max,
  step,
  onChange,
}: Props) {
  const [inputValue, setInputValue] = useState(defaultValue || 0);

  useEffect(() => {
    if (value) {
      setInputValue(value);
    }
  }, [value]);

  return (
    <Box>
      <NumberInput
        value={inputValue}
        min={min}
        max={max}
        step={step}
        onChange={(value) => {
          setInputValue(Number(value));
          if (onChange) {
            onChange(Number(value));
          }
        }}
      >
        <NumberInputField />
        <NumberInputStepper>
          <NumberIncrementStepper />
          <NumberDecrementStepper />
        </NumberInputStepper>
      </NumberInput>
    </Box>
  );
}
