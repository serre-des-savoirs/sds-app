import { Textarea } from "@chakra-ui/react";
import { FieldValues, UseFormRegister } from "react-hook-form";

interface MyTextAreaProps {
  register: UseFormRegister<FieldValues>;
  name: string;
}

export function MyTextArea({ register, name }: MyTextAreaProps) {
  return <Textarea {...register(name, { required: true })} />;
}
