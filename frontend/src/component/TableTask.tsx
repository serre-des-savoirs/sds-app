import { Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { Itk } from "../utils";
import { deduplicateObjects, sortObjects } from "../utils/cleanGraphData";

interface Props {
  currentObject: Itk;
  title?: string;
}

export function TableTask({ currentObject, title }: Props) {
  const navigate = useNavigate();

  return (
    <Table variant={"simple"}>
      <Thead>
        <Tr>
          <Th>{!title ? "titre" : title}</Th>
          <Th>Temps</Th>
        </Tr>
      </Thead>
      <Tbody>
        {sortObjects(deduplicateObjects(currentObject.tasks), (a, b) => {
          return (
            a.hasOperationDistance.distanceDuration.numericDuration -
            b.hasOperationDistance.distanceDuration.numericDuration
          );
        }).map((el, index) => (
          <Tr
            key={`${el.taskId}-${index}`}
            onClick={() => {
              navigate(`/task/${el.taskId.split("#")[1]}`);
            }}
            cursor={"pointer"}
            _hover={{ bg: "gray.200" }}
          >
            <Td>{el.taskName}</Td>
            <Td>
              {el.hasOperationDistance.distanceDuration.numericDuration || 0}
            </Td>
          </Tr>
        ))}
      </Tbody>
    </Table>
  );
}
