import { Box } from "@chakra-ui/react";
import { ReactNode } from "react";
import { Navigate } from "react-router-dom";

interface Props {
  children?: ReactNode;
}

export function CheckToken({ children }: Props) {
  const token = localStorage.getItem("token");

  if (!token && window.location.pathname !== "/login") {
    return <Navigate to="/login" />;
  }

  return <Box>{children}</Box>;
}
