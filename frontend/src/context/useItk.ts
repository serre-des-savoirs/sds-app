import { useEffect, useState } from "react";
import { ITK_QUERY } from "../graphql/itk";
import { useFetch } from "../utils/hooks/useFetch";
import { removeStorage, setStorage } from "../utils/storage";
import { Itk } from "../utils/types";

interface itkRequest {
  id: string;
  name: string;
  cultureMode: string;
  implantationMode: string;
  plantId: string;
  dependsOnSoilClimateContext: {
    belongsToRegion: string[];
  };
}

export function useItk() {
  const [itkList, setItkList] = useState<Itk[]>([]);

  const { values: valuesItk, loading }: any = useFetch(
    import.meta.env.VITE_URL_QUERY,
    {
      query: ITK_QUERY,
    }
  );
  useEffect(() => {
    if (valuesItk?.data?.itks) {
      setItkList(
        valuesItk.data.itks
          .map((obj: itkRequest) => {
            return {
              id: obj.id.split("#")[1],
              title: obj.name,
              technicalCulture: obj.cultureMode,
              plantId: obj.plantId.split("#")[1],
              tasks: [],
              dependsOnSoilClimateContext: {
                belongsToRegion:
                  obj.dependsOnSoilClimateContext?.belongsToRegion || [],
                hasClimateContext: "",
              },
              hasImplantationMode: obj.implantationMode,
            };
          })
          .sort((a: Itk, b: Itk) => a.title.trim().localeCompare(b.title))
      );
    }
  }, [valuesItk]);

  const addItk = (el: Itk) => {
    setStorage("itkList", [...itkList, el]);
    setItkList([...itkList, el]);
  };

  const updateItk = (el: Itk) => {
    setItkList(itkList.map((itk) => (itk.id === el.id ? el : itk)));
  };

  const removeItk = (el: Itk | undefined) => {
    removeStorage("itkList");
    setStorage(
      "itkList",
      itkList.filter((itk) => itk.id !== el?.id)
    );
    setItkList(itkList.filter((itk) => itk.id !== el?.id));
  };

  const getItk = (id: string | undefined) => {
    return itkList.find((itk) => itk.id === id);
  };

  return { itkList, addItk, updateItk, removeItk, getItk, itkLoading: loading };
}
