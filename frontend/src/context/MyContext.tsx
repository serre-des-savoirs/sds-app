import { ReactNode, createContext, useContext } from "react";
import { Itk, Plant, Task } from "../utils/types";
import { useItk } from "./useItk";
import { usePlant } from "./usePlant";
import { useTask } from "./useTask";

interface Props {
  children: ReactNode;
}

export type contextType = {
  itkList: Itk[];
  plantList: Plant[];
  taskList: Task[];
  itkLoading: boolean;
  plantLoading: boolean;
};

export const myContext = createContext<contextType>({} as contextType);

// TODO : refactor the context
const ItkProvider = ({ children }: Props) => {
  const { itkList, itkLoading } = useItk();
  const { plantList, plantLoading } = usePlant();
  const { taskList, taskLoading } = useTask();

  return (
    <myContext.Provider
      value={{
        plantList,
        itkList,
        taskList,
        itkLoading,
        plantLoading,
      }}
    >
      {children}
    </myContext.Provider>
  );
};

export const useMyContext = () => {
  return useContext(myContext);
};

export default ItkProvider;
