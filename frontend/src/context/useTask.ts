import { useEffect, useState } from "react";
import { TASK_QUERY } from "../graphql/task";
import { useFetch } from "../utils/hooks/useFetch";
import { removeStorage, setStorage } from "../utils/storage";
import { Task } from "../utils/types";

interface taskRequest {
  id: string;
  type: string;
}

export function useTask() {
  const [taskList, setTaskList] = useState<Task[]>([]);

  const { values: valuesTask, loading }: any = useFetch(
    import.meta.env.VITE_URL_QUERY,
    {
      query: TASK_QUERY,
    }
  );
  useEffect(() => {
    if (valuesTask?.data?.taskTypes) {
      setTaskList(
        valuesTask.data.taskTypes.map((obj: taskRequest) => {
          return {
            id: obj.id.split("#")[1],
            title: obj.id.split("#")[1],
            type: obj.type.split("#")[1],
          };
        })
      );
    }
  }, [valuesTask]);

  const addTask = (el: Task) => {
    setStorage("taskList", [...taskList, el]);
    setTaskList([...taskList, el]);
  };

  const updateTask = (el: Task) => {
    removeStorage("taskList");
    setStorage(
      "taskList",
      taskList.map((task) => (task.id === el.id ? el : task))
    );
    setTaskList(taskList.map((task) => (task.id === el.id ? el : task)));
  };

  const removeTask = (el: Task | undefined) => {
    removeStorage("taskList");
    setStorage(
      "taskList",
      taskList.filter((task) => task.id !== el?.id)
    );
    setTaskList(taskList.filter((task) => task.id !== el?.id));
  };

  const getTask = (id: string | undefined) => {
    return taskList.find((task) => task.id === id);
  };

  return {
    taskList,
    addTask,
    updateTask,
    removeTask,
    getTask,
    taskLoading: loading,
  };
}
