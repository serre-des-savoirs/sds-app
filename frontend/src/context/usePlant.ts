import { useEffect, useState } from "react";
import { PLANT_QUERY } from "../graphql/plant";
import { useFetch } from "../utils/hooks/useFetch";
import { removeStorage, setStorage } from "../utils/storage";
import { Plant } from "../utils/types";

interface plantRequest {
  id: string;
  name: string;
  botanicalFamilies: { id: string; name: string }[];
  usageFamilies: { id: string; name: string }[];
}

export function usePlant() {
  const [plantList, setPlantList] = useState<Plant[]>([]);

  const { values: valuesPlant, loading }: any = useFetch(
    import.meta.env.VITE_URL_QUERY,
    {
      query: PLANT_QUERY,
    }
  );
  useEffect(() => {
    if (valuesPlant?.data?.plants) {
      setPlantList(
        valuesPlant.data.plants
          .map((obj: plantRequest) => {
            return {
              id: obj.id.split("#")[1],
              title: obj.name,
              botanicalFamilies: obj.botanicalFamilies,
              usageFamilies: obj.usageFamilies,
            };
          })
          .sort((a: Plant, b: Plant) => a.title.localeCompare(b.title))
      );
    }
  }, [valuesPlant]);

  const getPlant = (id: string | undefined) =>
    plantList.find((plant) => plant.id === id);

  const addPlant = (el: Plant) => {
    setStorage("plantList", [...plantList, el]);
    setPlantList([...plantList, el]);
  };

  const updatePlant = (el: Plant) => {
    removeStorage("plantList");
    setStorage(
      "plantList",
      plantList.map((plant) => (plant.id === el.id ? el : plant))
    );
    setPlantList(plantList.map((plant) => (plant.id === el.id ? el : plant)));
  };

  const removePlant = (el: Plant | undefined) => {
    removeStorage("plantList");
    setStorage(
      "plantList",
      plantList.filter((plant) => plant.id !== el?.id)
    );
    setPlantList(plantList.filter((plant) => plant.id !== el?.id));
  };

  return {
    plantList,
    getPlant,
    addPlant,
    updatePlant,
    removePlant,
    plantLoading: loading,
  };
}
