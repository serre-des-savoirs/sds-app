import { RouterProvider } from "react-router-dom";
import ItkProvider from "./context/MyContext";
import { router } from "./router";

function App() {
  return (
    <ItkProvider>
      <RouterProvider router={router} />
    </ItkProvider>
  );
}

export default App;
