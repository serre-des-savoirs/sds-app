export const PLANT_QUERY = `
query { 
    plants { 
        id
        name
        botanicalFamilies {
            id
            name
        }
        usageFamilies {
            id
            name
        }
    }
}`;
