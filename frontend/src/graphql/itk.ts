export const requestQueryItk = (id: string | undefined) => `query{
getItk(id: "${id}") {
    id
    name
    cultureMode
    implantationMode
    plantId
    source
    task {
      taskId
      taskType
      taskName
      implements {
        hasBedWidth {
          parameterValue
          hasParameterUnit
        }
        hasFootPassWidth {
          parameterValue
          hasParameterUnit
        }
        hasPlantingDensity {
          parameterValue
          hasParameterUnit
        }
        hasSpacingByLine {
          parameterValue
          hasParameterUnit
        }
        hasSpacingInsideLine {
          parameterValue
          hasParameterUnit
        }
        hasWalkingSpaceWidth {
          parameterValue
          hasParameterUnit
        }
        hasWorkload {
          parameterValue
          hasWorkloadUnit
        }
      }
      hasOperationDistance {
        distanceDuration {
          numericDuration
          unitType
        }
        refersToOperation {
          id
          type
        }
      }
      hasRelativePeriod {
        hasBeginning
      }
    }
    season
    hasRelativePeriod {
      hasBeginning
      hasEnding
    }
    dependsOnCulturalContext {
      cultivatesIn
      hasCultivation
    }
    dependsOnSoilClimateContext {
      belongsToRegion
      hasClimateContext
    }
    hasYield {
      averageYield
      maxYield
      minYield
      parameterValue
      hasParameterUnit
    }
    lossMargin
    hasGlobalWorload {
      parameterValue
      hasParameterUnit
    }
    typeSoil
    hasIrrigationMode
    hasFarmEquipment
  }
}`;

export const requestQueryPlant = (id: string | undefined) => `query {
	getPlant(id: "${id}") {
		id
    broaderKey
		name
		typeVarietal
		typeFamily
		wiki
    botanicalFamilies {
      id
      name
    }
    usageFamilies {
      id
      name
    }
		relatedCrop
		fao
    taxRef
		storage {
			minStorage
			maxStorage
			unitTimeStorage
			  }
		 }
	}`;

export const requestQueryTask = (
  id: string | undefined
) => `{getTask(id: "${id}") {
	id
	type
}
}`;

export const ITK_QUERY = `
query {
	itks {
		id
		name
		cultureMode
    implantationMode
		plantId
    dependsOnSoilClimateContext {
      belongsToRegion
    }
	}
}
`;

export const QUERY_UPDATE_ITK_TITLE = (
  id: string,
  newTitle: string,
  lang: string
) => `
mutation UpdateTitleItk {
	updateTitle(id: "${id}", newTitle: "${newTitle}", lang: "${lang}")
}`;

export const MUTATION_UPDATE_PLANT = (id: string, plantId: string) => `
mutation {
  updatePlant(id: "${id}" newPlant: "${plantId}")
  }`;

export const MUTATION_UPDATE_CLIMATE = (id: string, climate: string) => `
mutation updateClimate {
  updateClimate(id: "${id}" newClimate: "${climate}")
}`;

export const MUTATION_UPDATE_SEASON = (id: string, season: string[]) => `
mutation {
  updateSeason(id: "${id}" newSeason: ["${season.join('","')}"])
}`;

export const MUTATION_CULTURE_MODE = (id: string, cultureMode: string) => `
mutation updateCultureMode {
  updateCultureMode(id : "${id}" newCultureMode: "${cultureMode}")
}
`;

export const MUTATION_ITK_REGION = (id: string, region: string[]) => `
mutation {
  updateRegion(id: "${id}" newRegion: ["${region.join('","')}"])
}
`;

export const MUTATION_IMPLANTATION_MODE = (
  id: string,
  implantationMode: string
) => `
mutation updateImplatationMode {
  updateImplantationMode(id : "${id}" newImplantationMode : "${implantationMode}")
}
`;

export const MUTATION_TYPE_SOIL = (id: string, typeSoil: string) => `
mutation updateTypeSoil {
  updateTypeSoil(id : "${id}" newTypeSoil: "${typeSoil}")
}
`;

export const MUTATION_IRRIGATION_MODE = (
  id: string,
  irrigationMode: string
) => `
mutation updateIrrigationMode {
  updateIrrigationMode(id : "${id}" newIrrigationMode: "${irrigationMode}")
}
`;

export const MUTATION_FARM_EQUIPMENT = (id: string, farmEquipment: string) => `
mutation updateFarmEquipment {
  updateFarmEquipment(id : "${id}" newFarmEquipment: "${farmEquipment}")
}
`;

export const MUTATION_TASK = (
  id: string,
  title: string,
  duration: number,
  workloadValue: number,
  workloadUnit: string
) => `
mutation {
  updateTask(id: "${id}" newTitle: "${title}" duration:${duration} workloadValue:${workloadValue} workloadUnit:"${workloadUnit}")
}
`;

export const QUERY_GET_ALL_PLANTS = `
{
  getAllPlant{
    id
    plantName
  }
}
`;
