import { createBrowserRouter } from "react-router-dom";
import { Root } from "./page/Root";
import { LoginPage } from "./page/auth/LoginPage";
import { RegisterPage } from "./page/auth/RegisterPage";
import NotFound from "./page/errors/NotFound";
import { HomePage } from "./page/home/HomePage";
import { ItkDetail } from "./page/itk/ItkDetail";
import { ItkPage } from "./page/itk/ItkPage";
import { PlantDetail } from "./page/plant/PlantDetail";
import { PlantPage } from "./page/plant/PlantPage";
import { TaskDetail } from "./page/task/TaskDetail";
import { TaskPage } from "./page/task/TaskPage";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    children: [
      { path: "", element: <HomePage /> },
      { path: "/itk", element: <ItkPage /> },
      { path: "/itk/:id", element: <ItkDetail /> },
      { path: "/plant", element: <PlantPage /> },
      { path: "/plant/:id", element: <PlantDetail /> },
      { path: "/task", element: <TaskPage /> },
      { path: "/task/:id", element: <TaskDetail /> },
      { path: "/login", element: <LoginPage /> },
      { path: "/register", element: <RegisterPage /> },
      { path: "*", element: <NotFound /> },
    ],
  },
]);
