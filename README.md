# Serre des savoirs

Ce monorepo peut lancer plusieurs scripts indépendamment ou en simultané.

Pour initialiser le projet au root du repo. Il est important de penser qu'avant tout déploiement du projet, il est important d'utiliser la commande suivante.

> npm run init

Pour build :

> npm run build

pour run le project :

> npm run start

## Frontend

Dans le dossier frontend on va retrouver toute la partie front-end du projet

Pour le lancer il est possible d'utiliser la commande suivante :

> npm run dev

## Backend

dans le dossier backend on va retrouver toutes les connexions avec la base de données, les requêtes graphql ou même le système d'authentification.

Pour lancer le projet il est possible d'utiliser la commande :

> npm run start

## Docker-compose

Il existe une configuration docker qui permet de lancer le back-end et le front-end en simultané

> docker-compose up -d